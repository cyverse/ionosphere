# syntax=docker/dockerfile:1
FROM node:18-alpine

ARG CI_COMMIT_SHA=unknown
ENV ION_VERSION=$CI_COMMIT_SHA

WORKDIR /src

COPY package.json package-lock.json /src/
RUN npm ci

COPY . .
RUN npm run build

EXPOSE 3010 3011

CMD ["npm", "start"]

LABEL org.label-schema.vcs-url="https://gitlab.com/cyverse/ionosphere"
