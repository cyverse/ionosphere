/**
 * Contains controllers for express routes associated with creating
 * openstack credentials through the credential wizard.
 */

const openstackService = require("../services/openstack.service");

/**
 * Get openstack projects for current user.
 * @param {*} req request
 * @param {*} res response
 */
const getProjects = async (req, res) => {
  try {
    return await openstackService.getProjects(req, res);
  } catch (error) {
    return res.status(500).send("Error fetching Jetstream2 projects.");
  }
};

/**
 * Create openstack credentials for projects specified.
 * @param {*} req request
 * @param {*} res response
 */
const createCredentials = async (req, res) => {
  try {
    return await openstackService.createCredentials(req, res);
  } catch (error) {
    return res.status(500).send("Error creating Jetstream2 credentials.");
  }
};

module.exports = {
  getProjects,
  createCredentials,
};
