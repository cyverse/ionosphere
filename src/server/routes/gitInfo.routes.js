const express = require("express");
const { readFileSync } = require("node:fs");
const router = express.Router();

/**
 * read version.txt. see scripts/version.sh for its generation
 * @returns {{date: string, branch: string, hash: string}}
 */
function readVersionFile() {
  let result = {
    branch: "unknown",
    hash: "unknown",
    date: "unknown",
  };
  let content;
  try {
    content = readFileSync("./version.txt");
  } catch (e) {
    console.error(e);
    return result; // just return unknown if fail to read file
  }
  const lines = content.toString().split("\n");
  lines.forEach(function (line, i) {
    if (!line || line.length === 0) {
      return;
    }
    if (i === 0) {
      result.branch = line;
    } else if (i === 1) {
      result.hash = line;
    } else if (i === 2) {
      result.date = line;
    }
  });
  return result;
}

// read version file just once on startup
const ionVersion = readVersionFile();

router.get("/", (req, res) => {
  return res.json(ionVersion);
});

module.exports = router;
