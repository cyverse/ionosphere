require("dotenv").config({ path: __dirname + "/./../../.env.local" });
const express = require("express");
const cors = require("cors");
const crypto = require("node:crypto");

const SentryNodeJS = require("@sentry/node");
const SentryNextJS = require("@sentry/nextjs");
const next = require("next");
const ws = require("ws");
const stan = require("node-nats-streaming");
// const { logger, requestLogger, errorLogger } = require('./api/lib/logging')
const auth = require("./auth");
const Cacao = require("./apiClient");

const cookieParser = require("cookie-parser");

const isDevelopment = process.env.NODE_ENV !== "production";
const app = next({ dev: isDevelopment });

module.exports = app;

// get routers
const navRouter = require("./routes/nav.routes");
const openstackRouter = require("./routes/openstack.routes");
const gitInfoRouter = require("./routes/gitInfo.routes");

// Configure Sentry error tracking -- should be done as early as possible
let sentryRelease = undefined;
let sentryEnv = undefined;
if (process.env.SENTRY_DSN) {
  const domain = new URL(process.env.API_BASE_URL).hostname;
  sentryRelease = process.env.ION_VERSION;
  sentryEnv = `${process.env.NODE_ENV}_${domain}`;
  console.log(
    `Initializing Sentry, release: ${sentryRelease}, env: ${sentryEnv}`
  );

  SentryNodeJS.init({
    dsn: process.env.SENTRY_DSN,
    release: sentryRelease,
    environment: sentryEnv,
  });
  SentryNextJS.init({
    dsn: process.env.SENTRY_DSN,
    release: sentryRelease,
    environment: sentryEnv,
  });
} else {
  console.log("Sentry is disabled");
}

// Configure web socket server
const wsPort = process.env.SERVER_PORT * 1 + 1;
console.log("Initializing websocket server on port", wsPort);
const sockets = {};
const wsServer = new ws.Server({ port: wsPort });
wsServer.on("connection", (ws, req) => {
  const username = req.url.substring(req.url.lastIndexOf("/") + 1); //TODO consider using express-ws package for routing
  const connId = req.headers["sec-websocket-key"];
  //console.log(`Client connected: username=${username} id=${connId}`) // ip=${req.socket.remoteAddress}

  if (!username) {
    console.error("Failed to parse username from url:", req.url);
    return;
  }

  //TODO add authentication, see "upgrade" event
  if (!(username in sockets)) sockets[username] = {};
  sockets[username][connId] = ws;

  // ws.on('message', (message) => {
  //     console.log('Socket received:', message)
  // })

  ws.on("close", (code) => {
    //console.log(`Client closed: username=${username} id=${connId} code=${code}`)
    delete sockets[username][connId];
  });

  //TODO add disconnect detection using ping

  ws.send(
    JSON.stringify({
      type: "connected",
      data: {
        key: req.headers["sec-websocket-key"],
      },
    })
  );
});

// Configure the NATS connection
let sc;
if (process.env.NATS_URL) {
  const subscribedEventTypes = [
    //FIXME move to config or somewhere else
    "WorkspaceCreated",
    "WorkspaceCreateFailed",
    "WorkspaceUpdated",
    "WorkspaceUpdateFailed",
    "WorkspaceDeleted",
    "WorkspaceDeleteFailed",
    "DeploymentCreated",
    "DeploymentCreateFailed",
    "DeploymentDeleted",
    "DeploymentDeleteFailed",
    "DeploymentRunCreated",
    "DeploymentRunCreateFailed",
    "DeploymentRunStatusUpdated",
  ];

  sc = stan.connect(
    "cacao-cluster",
    process.env.NATS_CLIENT_ID || "ionosphere",
    process.env.NATS_URL
  ); //FIXME move cluster name to config file
  sc.on("connect", () => {
    console.log(`Connected to NATS ${process.env.NATS_URL}`);

    //const opts = sc.subscriptionOptions().setStartWithLastReceived()
    const subscription = sc.subscribe("cyverse.events"); //, opts)
    subscription.on("message", (msg) => {
      try {
        //console.log('Received message [' + msg.getSequence() + '] ' + msg.getData())
        const message = JSON.parse(msg.getData());
        const data = JSON.parse(
          Buffer.from(message.data_base64, "base64").toString("utf-8")
        );
        console.log(
          `Message: actor=${data.actor} type=${
            message.type
          } data=${JSON.stringify(data)}`
        );
        const username = data.actor;
        const eventName = message.type.substring(
          message.type.lastIndexOf(".") + 1
        );

        // Filter and broadcast to appropriate client over websocket
        if (
          username &&
          username in sockets &&
          message.type &&
          subscribedEventTypes.includes(eventName)
        ) {
          for (const connId in sockets[username]) {
            console.log("Send client:", username, connId, eventName);
            const ws = sockets[username][connId];
            ws.send(
              JSON.stringify({
                type: eventName,
                transactionId: message.transactionid,
                data,
              })
            );
          }
        }
      } catch (e) {
        console.error(`fail to handle message, ${e}`);
      }
    });

    //TODO add connection closure/timeout handling, https://www.npmjs.com/package/node-nats-streaming
  });
} else {
  console.log("NATS is disabled");
}

// Configure authentication
auth.init({
  onLogin: async (token, profile) => {
    const now = new Date();
    const cloudevent = {
      specversion: "1.0",
      type: "org.cyverse.events.UserLogin",
      source: "ionosphere",
      id: "cloudevent-" + crypto.randomBytes(10).toString("hex"), // this generates something that looks like xid
      time: now.toISOString(),
      datacontenttype: "application/json",
      data_base64: Buffer.from(
        JSON.stringify({ token: token, profile: profile })
      ).toString("base64"),
    };
    sc.publish("cyverse.events", JSON.stringify(cloudevent), (err, guid) => {
      if (err) {
        console.log("Publish failed: " + err);
      } else {
        console.log("Publish: UserLogin", guid, profile);
      }
    });
  },
});

app
  .prepare()
  .then(() => {
    const server = express();

    // Setup logging
    // server.use(errorLogger)
    // server.use(requestLogger)

    // Setup Sentry error handling
    if (process.env.SENTRY_DSN)
      server.use(SentryNodeJS.Handlers.requestHandler());

    // Support CORS requests
    server.use(cors());
    server.use(cookieParser());

    // Configure Express behind SSL proxy: https://expressjs.com/en/guide/behind-proxies.html
    // Also set "proxy_set_header X-Forwarded-Proto https;" in NGINX config
    server.set("trust proxy", true);

    auth.middleware(server);

    // Setup API client for use by getServerSideProps()
    server.use((req, _, next) => {
      let token;
      if (process.env.API_SIMPLE_TOKEN)
        // development with mock API
        token = process.env.API_SIMPLE_TOKEN;
      else if (process.env.DEV_AUTH_TOKEN)
        // development with OAuth token
        token = "Bearer " + process.env.DEV_AUTH_TOKEN;
      else token = req.user ? "Bearer " + req.user.accessToken : null; // set by Keycloak/Globus in auth.middleware() or undefined for mock API

      if (token) console.log("token:", token); //FIXME remove after MVP

      req.api = new Cacao({
        baseUrl: process.env.API_BASE_URL,
        token: token,
        debug: true,
      });

      req.isDevelopment = isDevelopment;
      if (process.env.SENTRY_DSN) {
        // pass SENTRY_DSN to be used in page rendering (see _app.js).
        req.sentryDSN = process.env.SENTRY_DSN;
        req.sentryRelease = sentryRelease;
        req.sentryEnv = sentryEnv;
      }
      next();
    });

    // configure routers
    server.use("/gitinfo", gitInfoRouter);
    server.use("/openstack", openstackRouter);
    server.use("/", navRouter);

    // Catch errors
    if (process.env.SENTRY_DSN)
      server.use(SentryNodeJS.Handlers.errorHandler());

    server.listen(process.env.SERVER_PORT, (err) => {
      if (err) throw err;
      if (isDevelopment)
        console.log("!!!!!!!!! RUNNING IN DEV MODE !!!!!!!!!!");
      console.log(`Ready on port ${process.env.SERVER_PORT}`);
    });
  })
  .catch((exception) => {
    //logger.error(exception.stack)
    console.error(exception.stack);
    process.exit(1);
  });
