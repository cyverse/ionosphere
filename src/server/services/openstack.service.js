/**
 * Contains services for express controllers associated with creating
 * openstack credentials through the credential wizard.
 */

const isDevelopment = process.env.NODE_ENV !== "production";

/**
 * Get openstack projects for current user.
 * @param {*} req request
 * @param {*} res response
 */
const getProjects = async (req, res) => {
  let providerId = req.query.selectedCloudId;
  let openstackToken;

  // get unscoped openstack token from appropriate location
  if (isDevelopment) {
    openstackToken = process.env.UNSCOPED_TOKEN;
  } else {
    openstackToken = req.cookies.openstackToken;
  }

  // get projects
  let projectList = await req.api.openstackProjects(providerId, {
    openstacktoken: openstackToken,
  });

  // return project list in response
  res.setHeader("Content-Type", "application/json");
  return res.end(JSON.stringify({ projectList: projectList }));
};

/**
 * Create openstack credentials for projects specified.
 * @param {*} req request
 * @param {*} res response
 */
const createCredentials = async (req, res) => {
  let providerId = req.query.selectedCloudId;
  let credentialsToCreate = req.query.credentialsToCreate;

  let openstackToken;

  // get unscoped openstack token from appropriate location
  if (isDevelopment) {
    openstackToken = process.env.UNSCOPED_TOKEN;
  } else {
    openstackToken = req.cookies.openstackToken;
  }

  // make credential create request to openstack
  let promiseResults = await Promise.allSettled(
    credentialsToCreate.map((credential) =>
      req.api.createOpenstackCredential(providerId, credential, {
        openstacktoken: openstackToken,
      })
    )
  );

  // send back results of credential creation
  res.setHeader("Content-Type", "application/json");
  res.end(
    JSON.stringify({
      promiseResults: promiseResults,
    })
  );
};

module.exports = {
  getProjects,
  createCredentials,
};
