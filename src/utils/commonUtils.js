/**
 * Generic utility functions
 * @param {*} timestamp
 * @returns
 */

export function datetimeString(timestamp) {
  return new Date(timestamp).toLocaleString([], { timeZoneName: "short" });
}

export function decodeBase64ArrayString(arrayStringBase64) {
  let arrayBase64 = [];

  if (arrayStringBase64.length) {
    arrayBase64 = arrayStringBase64.split(",");
  }

  let decodedKeys = [];
  arrayBase64.forEach((sshKey) => decodedKeys.push(atob(sshKey)));
  return decodedKeys;
}

export function sortBy(field) {
  return (a, b) => {
    return a[field] - b[field];
  };
}

export function sortByName(a, b) {
  return a.name.localeCompare(b.name);
}

export function sortByDateDesc(a, b) {
  return new Date(b.created_at) - new Date(a.created_at);
}

export function timeDiffString(start, end) {
  const diff = Math.abs(new Date(end) - new Date(start));
  const secDiff = Math.floor(diff / 1000);
  const minDiff = Math.floor(secDiff / 60);
  const hourDiff = Math.floor(minDiff / 60);
  const dayDiff = Math.floor(hourDiff / 24);

  if (dayDiff > 0) return `${dayDiff} day` + (dayDiff !== 1 ? "s" : "");
  if (hourDiff > 0) return `${hourDiff} hour` + (hourDiff !== 1 ? "s" : "");
  if (minDiff > 0) return `${minDiff} min`;
  if (secDiff > 0) return `${secDiff} sec`;

  return `${diff} ms`;
}

/**
 * Capitalize first letter of a string.
 * @param {string} string
 * @returns {string} Capitalized string.
 */
export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function stringToColor(string) {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
}
