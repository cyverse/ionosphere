/**
 * Utility functions that are specific to CACAO (relating to deployments, clouds, templates etc.)
 */

import { isAlphanumeric, isLowercase, isAlpha } from "validator";

function sortByName(a, b) {
  return a.name.localeCompare(b.name);
}

function sortByDateDesc(a, b) {
  return new Date(b.created_at) - new Date(a.created_at);
}

/**
 * Check if a cloud provider is JS2 provider
 * @param cloud
 * @returns {boolean}
 */
export function isJS2Cloud(cloud) {
  if (!cloud) {
    return false;
  }
  if (cloud["type"] !== "openstack") {
    return false;
  }
  if (typeof cloud["url"] !== "string") {
    return false;
  }
  return cloud.metadata.OS_AUTH_URL.startsWith(
    "https://js2.jetstream-cloud.org"
  );
}

export function ValidateDeploymentName(name) {
  if (!isAlpha(name.charAt(0))) {
    return "Name must begin with a letter.";
  } else if (
    !isAlphanumeric(name, ["en-US"], { ignore: "-" }) ||
    !isLowercase(name)
  ) {
    return "Name must be all lowercase and contain only letters, numbers, and hyphens (-).";
  } else {
    return null;
  }
}

// return true if template is a prerequisite template
export function isPrerequisiteTemplate(template) {
  // TODO use a better way (not hard-code name) to check if template is a prerequisite.
  return (
    template.name !== "openstack-prerequisite" &&
    template.name !== "aws-prerequisite"
  );
}

export function filterFlavorsByMinValues(all_flavors, min_disk, min_ram) {
  let filtered_result = [];

  if (!all_flavors) {
    return filtered_result;
  }

  // Loop through all flavors and add to filtered_result if it meets the conditions
  for (let flavor of all_flavors) {
    const meetsDiskRequirement =
      min_disk !== undefined ? flavor.disk >= min_disk : true;
    const meetsRamRequirement =
      min_ram !== undefined ? flavor.ram >= min_ram : true;

    if (meetsDiskRequirement && meetsRamRequirement) {
      filtered_result.push(flavor);
    }
  }

  return filtered_result;
}

export function sortDeployments(deployments, sort) {
  const order = {
    active: 0,
    "pre-flight": 1,
    errored: 2,
    "": 3,
  };

  if (sort === "name") return deployments.sort(sortByName);
  if (sort === "status")
    return deployments.sort(
      (a, b) => order[a.current_run.status] - order[b.current_run.status]
    );

  return deployments.sort(sortByDateDesc);
}

export function filterTemplateForSelection(
  t,
  selectedCloudType,
  templateTypesMap
) {
  if (
    selectedCloudType &&
    templateTypesMap &&
    templateTypesMap[selectedCloudType]
  ) {
    // filter out templates that does not match the provider/cloud type
    const matchedTemplateTypeName = templateTypesMap[selectedCloudType].find(
      (templateTypeName) => templateTypeName === t.metadata.template_type
    );
    if (!matchedTemplateTypeName) {
      return false;
    }
  }
  return isPrerequisiteTemplate(t);
}

/**
 * Returns a Map of provider type to list of template types. This can be used to lookup list of template types for a provider type.
 * e.g.
 * {
 *     "openstack": ["openstack_terraform"],
 *     "aws": ["aws_terraform"]
 * }
 * @param {[Object]} templateTypes
 * @returns {{}}
 */
export function providerTypeTemplateTypesMap(templateTypes) {
  let m = {};
  for (let i = 0; i < templateTypes.length; i++) {
    templateTypes[i].provider_types.forEach((pt) => {
      if (m[pt]) {
        m[pt].push(templateTypes[i].name);
      } else {
        m[pt] = [templateTypes[i].name];
      }
    });
  }
  return m;
}

/**
 * Returns a Map of template type to provider types. This can be used to get list of cloud types that can be used with a given template type.
 * e.g.
 * {
 *     "openstack_terraform": ["openstack"],
 *     "aws_terraform": ["aws"]
 * }
 * @param {[Object]} templateTypes
 * @returns {{}}
 */
export function templateTypeProviderTypesMap(templateTypes) {
  const map = {};

  templateTypes.forEach((templateType) => {
    map[templateType.name] = templateType.provider_types;
  });

  return map;
}

/**
 * Filters credentials for a given cloud. For AWS, it returns all credentials of type "aws".
 * For Openstack, it returns credentials that have a tag with the cloud id.
 * @param {*} credentials - List of credentials
 * @param {*} cloud - Cloud object
 * @returns {Array} - List of credentials for the given cloud
 */
export const filterCloudCredentials = (credentials, cloud) => {
  return credentials.filter((cred) => {
    if (cloud.type === "aws") {
      return cred.type === "aws";
    } else if (cloud.type === "openstack") {
      return (
        cred.type === cloud.type &&
        (cred.tags_kv.cacao_provider === cloud.id ||
          cred.tags.includes(cloud.id))
      );
    }
  });
};

/**
 * Filters credentials for a given Openstack projectId.
 * @param {*} credentials - List of credentials
 * @param {*} projectId - Openstack project ID
 * @returns {Array} - List of credentials for the given project ID
 */
export const filterOpenstackProjectCredentials = (credentials, projectId) => {
  return credentials.filter(
    (cred) => cred.tags_kv?.cacao_openstack_project_id === projectId
  );
};

/**
 * Helper function to check if the user has a credential for a given cloud.
 * @param {*} cloud - Cloud object
 * @param {*} credentials - List of credentials
 * @returns {boolean} - True if the user has a credential for the cloud, false otherwise.
 */
const hasCredentialForCloud = (cloud, credentials) => {
  return filterCloudCredentials(credentials, cloud).length > 0;
};

/**
 * Filters clouds that can be used to launch a deployment. A cloud is eligible if the user
 * has a credential for that cloud, and it is an allowed cloud type for the template
 * being launched.
 * @param {*} clouds - List of clouds
 * @param {*} cloudTypes - List of allowed cloud types for the template
 * @param {*} credentials - List of credentials
 * @returns {Array} - List of eligible clouds
 */
export const filterEligibleClouds = (clouds, cloudTypes, credentials) => {
  let filteredClouds = [];
  if (clouds) {
    filteredClouds = clouds.filter((cloud) =>
      hasCredentialForCloud(cloud, credentials)
    );

    if (cloudTypes) {
      const cloudTypeSet = new Set(cloudTypes);
      filteredClouds = filteredClouds.filter((cloud) =>
        cloudTypeSet.has(cloud.type)
      );
    }
  }
  return filteredClouds;
};

export const isCredentialForCloud = (credential, cloud) => {
  if (cloud.type === "aws") {
    return credential.type === "aws";
  } else if (cloud.type === "openstack") {
    return (
      credential.type === cloud.type &&
      (credential.tags_kv?.cacao_provider === cloud.id ||
        credential.tags?.includes(cloud.id))
    );
  }
};
