import React, { createContext, useContext, useState, useMemo } from "react";

const UserConfigContext = createContext();
UserConfigContext.displayName = "UserConfig";

// Hook to use the context
function useUserConfig() {
  const context = useContext(UserConfigContext);
  if (!context) {
    throw new Error(`useUserConfig must be used within a UserConfigProvider`);
  }
  return context;
}

function UserConfigProvider(props) {
  const [userConfig, setUserConfig] = useState(props.userConfig);

  // Memoize the context value to prevent unnecessary re-renders
  const value = useMemo(() => [userConfig, setUserConfig], [userConfig]);

  return <UserConfigContext.Provider value={value} {...props} />;
}

export { UserConfigProvider, useUserConfig };
