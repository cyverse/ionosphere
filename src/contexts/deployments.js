import React, { createContext, useState, useContext } from "react";
import { useError } from "./error";
import { useAPI } from "./api";

// Create context for deployments page
const DeploymentsContext = createContext();

// Custom hook to use the context
export const useDeployments = () => useContext(DeploymentsContext);

export const DeploymentsProvider = (props) => {
  // Initialize state with an empty array
  const [deployments, setDeployments] = useState(props.deployments || []);
  const [templates, setTemplates] = useState(props.templates || []);
  const [busy, setBusy] = useState(false);
  const [, setError] = useError();
  const api = useAPI();

  // Function to fetch deployments from the API
  const fetchDeployments = async () => {
    setBusy(true);
    try {
      const response = await api.deployments({ "full-run": true });
      setDeployments(response);
    } catch (error) {
      console.error("Failed to fetch deployments:", error);
      setError("Failed to fetch deployments.");
    }
    setBusy(false);
  };

  async function fetchDeployment(id) {
    try {
      const deployment = await api.deployment(id);

      // Find the index of the deployment with the given ID
      const index = deployments.findIndex((d) => d.id === id);

      // If the deployment exists, replace it
      if (index !== -1) {
        setDeployments(
          deployments.map((d, i) => {
            if (i === index) return deployment;
            else return d;
          })
        );
      } else {
        // If the deployment does not exist, prepend it to the list
        setDeployments([deployment, ...deployments]);
      }
    } catch (error) {
      console.error("Failed to fetch deployment:", error);
      setError("Failed to fetch deployment.");
    }
  }

  const deleteDeployment = async (id) => {
    setBusy(true);
    try {
      const res = await api.deleteDeployment(id);
      if (!res) setError("There was an error deleting your deployment.");
      else {
        const deployment = deployments.find((d) => d.id === id);
        deployment.pending_status = "deleting";
        setBusy(false);
      }
    } catch (error) {
      setError(api.errorMessage(error));
    }
  };

  return (
    <DeploymentsContext.Provider
      value={{
        deployments,
        templates,
        busy,
        setBusy,
        fetchDeployment,
        fetchDeployments,
        deleteDeployment,
      }}
    >
      {props.children}
    </DeploymentsContext.Provider>
  );
};
