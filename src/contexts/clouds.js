import React from "react";
import { isJS2Cloud } from "@/utils";

const CloudsContext = React.createContext();
CloudsContext.displayName = "Clouds";

function useClouds() {
  const context = React.useContext(CloudsContext);
  if (!context) {
    throw new Error(`useClouds must be used within a CloudsProvider`);
  }
  return context;
}

function CloudsProvider(props) {
  const [clouds, setClouds] = React.useState(props.clouds);

  const value = React.useMemo(() => {
    // Map through clouds and add hasProjects property to each
    const cloudsHasProjects = clouds?.map((cloud) => ({
      ...cloud,
      hasProjects:
        isJS2Cloud(cloud) && cloud.name === "Jetstream 2" ? true : false,
    }));

    return [cloudsHasProjects, setClouds];
  }, [clouds]);

  return <CloudsContext.Provider value={value} {...props} />;
}

export { CloudsProvider, useClouds };
