import React from "react";
import {
  createContext,
  useState,
  useRef,
  useEffect,
  useCallback,
  useMemo,
} from "react";
import Sockette from "sockette";
import { useConfig } from "./config";

const EventsContext = createContext();
EventsContext.displayName = "Events";

function useEvents() {
  const context = React.useContext(EventsContext);
  if (!context) {
    throw new Error(`useEvents must be used within a EventsProvider`);
  }
  return context;
}

function EventsProvider(props) {
  const config = useConfig();
  const [event, setEvent] = useState();
  const value = useMemo(() => [event], [event]);

  const onMessage = useCallback((event) => {
    const data = event.data;
    //console.log("event:", data);

    let msg;
    try {
      msg = JSON.parse(data);
    } catch (e) {
      console.error("Could not parse event mesage data", event);
      return;
    }

    setEvent(msg);
  }, []);

  // Saving websocket connection in ref will allow websocket connection be cached between component unmounts.
  const wsConn = useRef(null);
  useEffect(() => {
    if (!wsConn.current && config.WS_BASE_URL) {
      console.log("Initializing websocket connection", config.WS_BASE_URL);
      const ws = new Sockette(config.WS_BASE_URL + "/" + props.username, {
        //maxAttempts: constants.WEBSOCKET_MAX_CONNECTION_ATTEMPTS, //FIXME
        onopen: (e) => {
          wsConn.current = ws;
        },
        onmessage: onMessage,
      });
    }
  }, []);

  return <EventsContext.Provider value={value} {...props} />;
}

export { EventsProvider, useEvents };
