import { createContext, useContext, useState, useMemo } from "react";

const CredentialsContext = createContext();
CredentialsContext.displayName = "Credentials";

function useCredentials() {
  const context = useContext(CredentialsContext);
  if (!context) {
    throw new Error(`useCredentials must be used within a CredentialsProvider`);
  }
  return context;
}

function CredentialsProvider(props) {
  const [credentials, setCredentials] = useState(props.credentials);
  const value = useMemo(() => [credentials, setCredentials], [credentials]);
  return <CredentialsContext.Provider value={value} {...props} />;
}

export { CredentialsProvider, useCredentials };
