import React from "react";

const AllocationsContext = React.createContext();
AllocationsContext.displayName = "Js2Allocations";

function useJs2Allocations() {
  const context = React.useContext(AllocationsContext);
  if (!context) {
    throw new Error(
      `useJs2Allocations must be used within a Js2AllocationsProvider`
    );
  }
  return context;
}

function Js2AllocationsProvider(props) {
  const [js2Allocations, setJs2Allocations] = React.useState(
    props.js2Allocations
  );
  const value = React.useMemo(
    () => [js2Allocations, setJs2Allocations],
    [js2Allocations]
  );
  return <AllocationsContext.Provider value={value} {...props} />;
}

export { Js2AllocationsProvider, useJs2Allocations };
