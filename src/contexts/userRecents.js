import React, { createContext, useContext, useState, useMemo } from "react";

import { useAPI, useUser } from "@/contexts";

const UserRecentsContext = createContext();
UserRecentsContext.displayName = "UserRecents";

// Hook to use the context
function useUserRecents() {
  const context = useContext(UserRecentsContext);
  if (!context) {
    throw new Error(`useUserRecents must be used within a UserRecentsProvider`);
  }
  return context;
}

function UserRecentsProvider(props) {
  const api = useAPI();
  const [user] = useUser();

  const [userRecents, setUserRecents] = useState(props.userRecents);

  const updateUserRecents = async (propertyName, newValue) => {
    try {
      await api.setUserRecent(user.username, propertyName, newValue);
      const newRecents = await api.userRecents(user.username);
      setUserRecents(newRecents);
    } catch (error) {
      console.error(error);
    }
  };

  // Memoize the context value to prevent unnecessary re-renders
  const value = useMemo(
    () => [userRecents, updateUserRecents],
    [userRecents, updateUserRecents]
  );

  return <UserRecentsContext.Provider value={value} {...props} />;
}

export { UserRecentsProvider, useUserRecents };
