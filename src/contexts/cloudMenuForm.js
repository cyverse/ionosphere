import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useReducer,
} from "react";

import {
  useClouds,
  useCredentials,
  useJs2Allocations,
  useUserRecents,
} from "@/contexts";

import { useFilteredProjects, useFilteredCredentials } from "@/hooks";
import { filterEligibleClouds } from "@/utils/cacaoUtils";

const CloudMenuFormContext = createContext();

const initialState = {
  selectedCloud: null,
  selectedProject: null,
  selectedCredentialId: null,
};

function reducer(state, action) {
  switch (action.type) {
    case "SET_CLOUD":
      return { ...state, cloud: action.payload };
    case "SET_PROJECT":
      return { ...state, project: action.payload };
    case "SET_CREDENTIAL_ID":
      return { ...state, credentialId: action.payload };
    default:
      return state;
  }
}

export const setCloud = (payload) => ({
  type: "SET_CLOUD",
  payload,
});

export const setProject = (payload) => ({
  type: "SET_PROJECT",
  payload,
});

export const setCredentialId = (payload) => ({
  type: "SET_CREDENTIAL_ID",
  payload,
});

export const CloudMenuFormProvider = ({ children, templateCloudTypes }) => {
  // Get clouds and js2Allocations from context
  const [clouds] = useClouds();
  const [js2Allocations] = useJs2Allocations();
  const [credentials] = useCredentials();
  const [userRecents] = useUserRecents();
  const [cloudMenu, dispatch] = useReducer(reducer, initialState);

  // Filter clouds based on user's credentials
  const cloudOptions = useMemo(() => {
    return filterEligibleClouds(
      clouds,
      templateCloudTypes || null,
      credentials
    );
  }, [clouds, credentials, templateCloudTypes]);

  // Filter projects based on selected cloud
  const [projectOptions, loadingProjects] = useFilteredProjects(
    cloudMenu.cloud?.id,
    cloudMenu.cloud?.hasProjects,
    js2Allocations
  );

  // Filter credentials based on selected cloud and project
  const [credentialOptions, loadingCredentials] = useFilteredCredentials(
    credentials,
    cloudMenu.cloud,
    cloudMenu.project?.id
  );

  // When cloud options are loaded, set the cloud to the user's recent selection or first in list
  useEffect(() => {
    if (cloudOptions && !cloudMenu.cloud) {
      const selectedCloud =
        cloudOptions.find((option) => option.id === userRecents["cloud"]) ||
        cloudOptions[0];
      dispatch(setCredentialId(""));
      dispatch(setProject(null));
      dispatch(setCloud(selectedCloud));
    }
  }, [cloudOptions]);

  // When project options are loaded, set the project to the user's recent selection for this cloud or first in list
  useEffect(() => {
    if (projectOptions && !cloudMenu.project) {
      const selectedProject =
        projectOptions.find(
          (option) =>
            option.name === userRecents[`project-${cloudMenu.cloud?.id}`]
        ) || projectOptions[0];
      dispatch(setCredentialId(""));
      dispatch(setProject(selectedProject));
    } else if (!projectOptions) {
      dispatch(setCredentialId(""));
      setProject(null);
    }
  }, [projectOptions]);

  // When credential options are loaded, set the credential to the user's recent selection for this cloud or first in list
  useEffect(() => {
    if (credentialOptions && !cloudMenu.credentialId) {
      const selectedCredentialId =
        credentialOptions.find(
          (option) =>
            option.id === userRecents[`credential-${cloudMenu.cloud?.id}`]
        )?.id || credentialOptions[0]?.id;
      dispatch(setCredentialId(selectedCredentialId));
    }
  }, [credentialOptions]);

  return (
    <CloudMenuFormContext.Provider
      value={{
        cloudMenu,
        cloudMenuDispatch: dispatch,
        cloudOptions,
        projectOptions,
        loadingProjects,
        credentialOptions,
        loadingCredentials,
      }}
    >
      {children}
    </CloudMenuFormContext.Provider>
  );
};

export const useCloudMenuForm = () => useContext(CloudMenuFormContext);
