import React, {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";

import {
  useClouds,
  useCredentials,
  useJs2Allocations,
  useUserRecents,
} from "@/contexts";
import { useFilteredCredentials, useFilteredProjects } from "@/hooks";
import { filterEligibleClouds } from "@/utils/cacaoUtils";

// Create the context
const CloudMenuContext = createContext();

// Provider for Deployments page cloud menu
export const CloudMenuProvider = ({ children }) => {
  const [clouds] = useClouds();
  const [js2Allocations] = useJs2Allocations();
  const [credentials] = useCredentials();
  const [userRecents] = useUserRecents();

  // Filter clouds that user has credentials for
  const cloudOptions = useMemo(() => {
    return filterEligibleClouds(clouds, null, credentials);
  }, [clouds, credentials]);

  // Get cloud from user recents or default to first cloud
  const initialCloud =
    cloudOptions.find((c) => c.id === userRecents.cloud) || cloudOptions[0];

  // Cloud menu state
  const [selectedCloud, setSelectedCloud] = useState(initialCloud);
  const [selectedProject, setSelectedProject] = useState();
  const [selectedCredential, setSelectedCredential] = useState();

  // Filter projects based on selected cloud
  const [projectOptions, loadingProjects] = useFilteredProjects(
    selectedCloud?.id,
    selectedCloud?.hasProjects,
    js2Allocations
  );

  // Filter credentials based on selected cloud and project
  const [credentialOptions, loadingCredentials] = useFilteredCredentials(
    credentials,
    selectedCloud,
    selectedProject?.id
  );

  // If recent cloud is updated, update selectedCloud
  useEffect(() => {
    const cloud =
      cloudOptions.find((c) => c.id === userRecents.cloud) || cloudOptions[0];
    setSelectedCloud(cloud);
  }, [cloudOptions, userRecents.cloud]);

  // Once project options are loaded, set the selected project based
  // on the user recents or default to the first project
  useEffect(() => {
    const project =
      projectOptions.find(
        (p) => p.name === userRecents[`project-${selectedCloud?.id}`]
      ) || projectOptions[0];
    setSelectedProject(project);
  }, [projectOptions, userRecents[`project-${selectedCloud?.id}`]]);

  // Once credential options are loaded, set the selected credential based on the user recents or default to the first credential
  useEffect(() => {
    const credential =
      credentialOptions.find(
        (c) => c.id === userRecents[`credential-${selectedCloud?.id}`]
      ) || credentialOptions[0];
    setSelectedCredential(credential);
  }, [credentialOptions, userRecents[`credential-${selectedCloud?.id}`]]);

  // Context value
  const contextValue = {
    cloudOptions,
    projectOptions,
    credentialOptions,
    selectedCloud,
    selectedProject,
    selectedCredential,
    setSelectedCloud,
    setSelectedProject,
    setSelectedCredential,
    loadingCredentials,
    loadingProjects,
  };

  return (
    <CloudMenuContext.Provider value={contextValue}>
      {children}
    </CloudMenuContext.Provider>
  );
};

// Custom hook to use the context
export const useCloudMenu = () => useContext(CloudMenuContext);
