import { useState, useEffect } from "react";
import { useAPI } from "@/contexts";

/**
 * The useFilteredProjects hook returns a list of projects that the user can
 * launch deployments for in the selected cloud. We do this by fetching the projects
 * for the selected cloud and if the cloud is JS2, filtering out projects that the user
 * does not have an allocation for.
 *
 * @param {*} cloudId - The ID of the selected cloud
 * @param {*} js2Allocations - Users js2 allocations
 * @returns {[Array, boolean]} - Returns an array containing the list of projects and a boolean indicating if the projects are loading.
 */
export const useFilteredProjects = (
  cloudId,
  cloudHasProjects,
  js2Allocations
) => {
  const [projectOptions, setProjectOptions] = useState([]);
  const [loadingProjects, setLoadingProjects] = useState(false);
  const api = useAPI();

  const updateProjectOptions = async (
    cloudId,
    cloudHasProjects,
    js2Allocations
  ) => {
    setLoadingProjects(true);
    if (cloudHasProjects === false) {
      setProjectOptions([]);
      setLoadingProjects(false);
      return;
    }
    try {
      const cloudProjects = await api.providerProjects(cloudId);
      const filtered = cloudProjects.filter((project) =>
        js2Allocations.has(project.name)
      );
      setProjectOptions(filtered);
    } catch (error) {
      setProjectOptions([]);
    } finally {
      setLoadingProjects(false);
    }
  };

  useEffect(() => {
    if (cloudId && js2Allocations) {
      updateProjectOptions(
        cloudId,
        cloudHasProjects,
        new Set(js2Allocations.map((allocation) => allocation.title))
      );
    }
  }, [cloudId, js2Allocations]);

  return [projectOptions, loadingProjects];
};
