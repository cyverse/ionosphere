import { useState, useEffect } from "react";
import {
  filterCloudCredentials,
  filterOpenstackProjectCredentials,
} from "@/utils";

/**
 * Returns a list of credentials valid for the selected cloud and project (project optional).
 * @param {*} credentials - List of credentials
 * @param {Object} cloud - Selected cloud
 * @param {} projectId - Selected project ID
 * @returns {[Array, boolean]} - Returns an array containing the list of credentials and a boolean indicating if the credentials are loading.
 */
export const useFilteredCredentials = (credentials, cloud, projectId) => {
  const [credentialOptions, setCredentialOptions] = useState([]);
  const [loadingCredentials, setLoadingCredentials] = useState(false);

  const updateCredentialOptions = async (cloud, projectId) => {
    setLoadingCredentials(true);
    const filteredCredentials = filterCloudCredentials(credentials, cloud);

    if (projectId) {
      filteredCredentials = filterOpenstackProjectCredentials(
        filteredCredentials,
        projectId
      );
    }

    setCredentialOptions(filteredCredentials);
    setLoadingCredentials(false);
  };

  useEffect(() => {
    if (cloud) {
      updateCredentialOptions(cloud, projectId);
    }
  }, [cloud, projectId]);
  return [credentialOptions, loadingCredentials];
};
