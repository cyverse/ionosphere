import { useEffect, useState } from "react";
import { useAPI, useClouds, useCredentials } from "@/contexts";
import { sortBy } from "@/utils";
import { isCredentialForCloud } from "@/utils/cacaoUtils";

/**
 * Custom React hook to fetch and manage a list of flavors from an API based on
 * the given cloud, credential, and region.
 *
 * This hook handles fetching, state management of the flavors, loading status, and any errors.
 *
 * @param {string} cloudId - The ID of the cloud provider,
 * @param {string} regionId - The ID of the region.
 * @returns {Object} An object containing the fetched flavors, a loading state indicator, and any encountered errors.
 */
export const useFlavors = (cloudId, credentialId, regionId) => {
  const api = useAPI();

  const [flavors, setFlavors] = useState([]);
  const [loadingFlavors, setLoadingFlavors] = useState(false);
  const [error, setError] = useState();
  const [credentials] = useCredentials();
  const [clouds] = useClouds();

  /**
   * Fetches flavors and provides loading state.
   */
  const fetchFlavors = async () => {
    setLoadingFlavors(true);

    try {
      // pass credential to filter flavors by allocation
      const fetchedFlavors = await api.providerFlavors(cloudId, {
        credential: credentialId,
        region: regionId,
      });
      flavors = flavors.sort(sortBy("id"));
      setFlavors(fetchedFlavors);
    } catch (e) {
      setError(e);
    } finally {
      setLoadingFlavors(false);
    }
  };

  useEffect(() => {
    if (!!credentialId && !!regionId) {
      const cloud = clouds.find((c) => c.id === cloudId);
      const credential = credentials.find((c) => c.id === credentialId);
      if (isCredentialForCloud(credential, cloud)) {
        setTimeout(fetchFlavors, 0);
      }
    }
  }, [credentialId, regionId]);

  return {
    flavors,
    loadingFlavors,
    flavorError: error,
  };
};
