import { useEffect, useState } from "react";
import { useAPI, useClouds, useCredentials } from "@/contexts";
import { isCredentialForCloud } from "@/utils/cacaoUtils";

/**
 * Custom React hook to fetch and manage images from an API, based on the provided cloud, credential, and region IDs.
 * The hook handles the fetching process, maintains the state of the images, loading status, and any errors.
 *
 * @param {string} cloudId - The ID of the cloud provider.
 * @param {string} credentialId - The ID of the credential.
 * @param {string} regionId - The ID of the region.
 * @returns {Object} An object containing the fetched images, a loading status, and any errors encountered.
 */
export const useImages = (cloudId, credentialId, regionId) => {
  const api = useAPI();
  const [credentials] = useCredentials();
  const [clouds] = useClouds();

  const [images, setImages] = useState([]);
  const [loadingImages, setLoadingImages] = useState(false);
  const [error, setError] = useState();

  /**
   * Fetches images and provides loading state.
   */
  const fetchImages = async () => {
    setLoadingImages(true);

    try {
      // pass credential to filter images by allocation
      const fetchedImages = await api.providerImages(cloudId, {
        credential: credentialId,
        region: regionId,
      });
      setImages(fetchedImages);
    } catch (e) {
      setError(e);
    } finally {
      setLoadingImages(false);
    }
  };

  useEffect(() => {
    if (!!credentialId && !!regionId && !!cloudId) {
      const cloud = clouds.find((c) => c.id === cloudId);
      const credential = credentials.find((c) => c.id === credentialId);
      if (isCredentialForCloud(credential, cloud)) {
        setTimeout(fetchImages, 0);
      }
    }
  }, [cloudId, credentialId, regionId]);

  return {
    images,
    loadingImages,
    imagesError: error,
  };
};
