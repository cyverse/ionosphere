import {
  Alert,
  AlertTitle,
  Box,
  Divider,
  MenuItem,
  Button,
  Typography,
  TextField,
  Link,
  Stack,
  Paper,
} from "@mui/material";

import { red } from "@mui/material/colors";

import { useClouds } from "../../contexts";

const AddSshKeyForm = ({ onChange, hasConflict }) => {
  return (
    <Stack spacing={4}>
      <Stack spacing={2}>
        <Typography>
          If you do not already have an SSH Key, you'll need to{" "}
          <Link>generate one</Link> first.
        </Typography>
        <Alert severity="info">
          <AlertTitle>Where can I find my SSH Key?</AlertTitle>
          <Stack spacing={2}>
            <Typography variant="subtitle">
              Your SSH public key is usually contained the file{" "}
              <strong> ~/.ssh/id_ed25519.pub</strong> or{" "}
              <strong>~/.ssh/id_rsa.pub</strong> and begins with{" "}
              <strong>ssh-ed25519</strong> or <strong>ssh-rsa</strong>.
            </Typography>
          </Stack>
        </Alert>
      </Stack>
      {/* <Divider></Divider> */}

      <Stack spacing={2}>
        <Typography>
          Choose a descriptive name for your SSH Key credential.
        </Typography>
        <Box>
          <TextField
            id="id"
            label="Name"
            placeholder="Choose a descriptive name"
            size="small"
            variant="outlined"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            onChange={onChange}
            error={hasConflict}
            helperText={
              hasConflict &&
              "A credential with this name already exists. Please choose another name."
            }
          />
        </Box>
      </Stack>

      <Stack spacing={2}>
        <Typography>
          Paste your <strong>public</strong> SSH key below.{" "}
          <strong
            style={{
              color: red[500],
            }}
          >
            Do not share your private key
          </strong>
          , as this may compromise your identity.
        </Typography>

        <Box>
          <TextField
            id="value"
            label="Public SSH Key"
            fullWidth
            multiline
            rows={4}
            placeholder="Typically starts with 'ssh-ed25519' or 'ssh-rsa'"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={onChange}
            // helperText={<>Warning: Do not share your private key</>}
          />
        </Box>
      </Stack>
    </Stack>
  );
};

const AddOpenStackApplicationForm = ({ cloudId, onChange, hasConflict }) => {
  const [clouds] = useClouds();

  return (
    <Box margin={2}>
      {!cloudId && (
        <Box>
          <TextField
            id="id"
            label="Name"
            placeholder="Choose a descriptive name"
            fullWidth
            required
            variant="outlined"
            sx={{ fontSize: "0.9em", width: "20rem" }}
            InputLabelProps={{
              shrink: true,
            }}
            onChange={onChange}
            error={hasConflict}
            helperText={
              hasConflict &&
              "A credential with this name already exists. Please choose another name."
            }
          />
        </Box>
      )}
      {!cloudId && (
        <Box mb={1}>
          <TextField
            id="cloudId"
            name="cloudId"
            required
            select
            margin="normal"
            fullWidth
            variant="outlined"
            sx={{ fontSize: "0.9em", width: "20rem" }}
            InputLabelProps={{
              shrink: true,
            }}
            label="Cloud"
            defaultValue={clouds[0]?.id}
            onChange={onChange}
          >
            {clouds
              .filter((c) => c.type === "openstack")
              .map((c, index) => (
                <MenuItem key={index} value={c.id}>
                  {c.name}
                </MenuItem>
              ))}
          </TextField>
        </Box>
      )}
      <Box mb={2}>
        <TextField
          id="applicationId"
          label="Application ID"
          fullWidth
          required
          variant="outlined"
          sx={{ fontSize: "0.9em", width: "20rem" }}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={onChange}
        />
      </Box>
      <Box mb={2}>
        <TextField
          id="applicationSecret"
          label="Application Secret"
          type="password"
          fullWidth
          required
          variant="outlined"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={onChange}
        />
      </Box>
    </Box>
  );
};

const UploadForm = ({ onChange }) => {
  return (
    <>
      <Typography>Custom upload form placeholder</Typography>
      <Box>
        <TextField
          InputLabelProps={{
            shrink: true,
          }}
          onChange={onChange}
        />
      </Box>
      <Box mt={2}>
        <Button color="primary" variant="contained">
          Upload
        </Button>
      </Box>
    </>
  );
};

export { AddSshKeyForm, AddOpenStackApplicationForm, UploadForm };
