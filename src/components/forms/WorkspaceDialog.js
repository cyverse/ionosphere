import { useState, useEffect } from "react";
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  AppBar,
  Toolbar,
  Grid,
  IconButton,
  Typography,
  TextField,
  FormControl,
  FormHelperText,
  Select,
  MenuItem,
  InputLabel,
} from "@mui/material";
import { Close as CloseIcon, Help as HelpIcon } from "@mui/icons-material";
import { useEvents, useClouds } from "../../contexts";
import { isEmpty } from "validator";

const CreateWorkspaceForm = ({ onSubmit, onClose }) => {
  const [clouds] = useClouds();

  const MIN_WORKSPACE_NAME_LENGTH = 4;
  const MAX_WORKSPACE_NAME_LENGTH = 234;
  const WORKSPACE_TYPES = ["Openstack", "CyVerse Cloud"];

  const initialValues = {
    name: "",
    description: "",
    // typeId: 0,
    default_provider_id: clouds[0].id,
  };

  const [values, setValues] = useState(initialValues);
  const [errors, setErrors] = useState({});
  const [isValid, setValid] = useState(false);

  const validateNameDesc = (value) => {
    if (isEmpty(value)) return "This field is required";
    if (value.length < MIN_WORKSPACE_NAME_LENGTH)
      return `Must be at least ${MIN_WORKSPACE_NAME_LENGTH} characters`;
    if (value.length > MAX_WORKSPACE_NAME_LENGTH)
      return `Must be less than ${MAX_WORKSPACE_NAME_LENGTH} characters`;
  };

  const validators = {
    name: validateNameDesc,
    description: () => false,
    // typeId: () => {},
    default_provider_id: (value) => (value ? null : "Must select a cloud"),
  };

  const handleChange = (e) => {
    const [id, value] = [e.target.name, e.target.value];
    setValues({ ...values, [id]: value });

    const validator = validators[id];
    const error = validator(value);
    setErrors({ ...errors, [id]: error });
  };

  useEffect(() => {
    setValid(
      Object.keys(validators).every((id) => !validators[id](values[id]))
    );
  }, [values, errors]);

  return (
    <Box mt={5} m={2}>
      <TextField
        sx={{ minWidth: "20em", margin: "2rem" }}
        fullWidth
        name="name"
        label="Workspace Name"
        multiline
        required
        error={!!errors["name"]}
        helperText={
          errors["name"] ||
          `${values["name"].length}/${MAX_WORKSPACE_NAME_LENGTH} characters`
        }
        value={values["name"]}
        variant="outlined"
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleChange}
      />
      <TextField
        sx={{ minWidth: "20em", margin: "2rem" }}
        name="description"
        label="Description"
        fullWidth
        multiline
        error={!!errors["description"]}
        helperText={
          errors["description"] ||
          `${values["description"].length}/${MAX_WORKSPACE_NAME_LENGTH} characters`
        }
        value={values["description"]}
        variant="outlined"
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleChange}
      />
      <Grid container spacing={3}>
        {/* <Grid item>
          <FormControl variant="outlined" sx={{minWidth: "20em",
        margin:'2rem'}}>
            <InputLabel id="workspace-type" shrink required>
              Workspace Type
            </InputLabel>
            <Select
              name="typeId"
              labelId="workspace-type"
              value={values['typeId']}
              label="Workspace Type"
              onChange={handleChange}
            >
              {WORKSPACE_TYPES.map((type, index) => (
                <MenuItem key={index} value={index}>
                  {type}
                </MenuItem>
              ))}
            </Select>
            <FormHelperText>{errors['typeId']}</FormHelperText>
          </FormControl>
          <IconButton color="primary" aria-label="help">
            <HelpIcon />
          </IconButton>
        </Grid> */}
        <Grid item>
          <FormControl
            variant="outlined"
            sx={{ minWidth: "20em", margin: "2rem" }}
          >
            <InputLabel id="cloud-id" shrink required>
              Cloud
            </InputLabel>
            <Select
              name="default_provider_id"
              labelId="cloud-id"
              value={values["default_provider_id"]}
              onChange={handleChange}
              label="Cloud"
            >
              {clouds &&
                clouds.map((c, index) => (
                  <MenuItem key={index} value={c.id}>
                    {c.name}
                  </MenuItem>
                ))}
            </Select>
            <FormHelperText>{errors["default_provider_id"]}</FormHelperText>
          </FormControl>
          {/* <IconButton color="primary" aria-label="help">
            <HelpIcon />
          </IconButton> */}
        </Grid>
      </Grid>
      <Box mt={5}>
        <Grid
          container
          spacing={1}
          direction="row"
          justifyContent="flex-end"
          alignItems="flex-end"
        >
          <Grid item>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              disabled={!isValid}
              onClick={() => onSubmit(values)}
              style={{ whiteSpace: "nowrap" }}
            >
              Create Workspace{/* Create and Close */}
            </Button>
          </Grid>
          {/* <Grid item>
            <Button
              variant="contained"
              fullWidth
              color="primary"
              disabled={!isValid}
              onClick={() => onSubmit(values, true)}
              style={{whiteSpace: 'nowrap'}}
            >
              Create and Continue to New Deployment
            </Button>
          </Grid> */}
        </Grid>
      </Box>
    </Box>
  );
};

export default function WorkspaceDialog({ open, handleSubmit, handleClose }) {
  // const [event] = useEvents()

  // FIXME event detection
  // useEffect(() => {
  //   if (event) {
  //     if (event.type === 'org.cyverse.events.WorkspaceCreated')
  //       router.push(`/workspaces/${event.data.id}`)
  //     else if (event.type === 'org.cyverse.events.WorkspaceCreateFailed')
  //       setError('Workspace creation failed')
  //   }
  // }, [event])

  return (
    <Dialog
      open={open}
      onClose={(_, reason) =>
        reason !== "backdropClick" && handleClose && handleClose()
      }
      maxWidth="md"
    >
      <AppBar sx={{ position: "relative" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" sx={{ marginLeft: "1rem", flex: 1 }}>
            New Workspace
          </Typography>
        </Toolbar>
      </AppBar>
      <DialogContent>
        <CreateWorkspaceForm onSubmit={handleSubmit} onClose={handleClose} />
      </DialogContent>
    </Dialog>
  );
}
