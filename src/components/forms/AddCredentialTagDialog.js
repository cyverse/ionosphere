import { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Stack,
  TextField,
} from "@mui/material";

import { Close as CloseIcon } from "@mui/icons-material";

/**
 * Dialog for adding a new tag to a credential.
 */
export default function AddTagDialog({
  open,
  title,
  handleClose,
  dispatchTagState,
}) {
  const [tagKey, setTagKey] = useState("");
  const [tagValue, setTagValue] = useState("");

  // Reset tag key/value when dialog is closed
  const closeDialog = () => {
    setTagKey("");
    setTagValue("");
    handleClose();
  };

  return (
    <Dialog open={open} aria-labelledby="edit-credential-dialog">
      <DialogTitle>
        {title}{" "}
        <IconButton
          aria-label="close"
          onClick={closeDialog}
          sx={{
            position: "absolute",
            right: 12,
            top: 12,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Stack spacing={2}>
          <DialogContentText mb={1} fontSize={14}>
            Tags are key/value pairs, but you can add a simple tag by only
            filling out <b>Key</b>.
          </DialogContentText>
          <TextField
            id="tagKey"
            name="tagKey"
            onChange={(e) => {
              setTagKey(e.target.value);
            }}
            value={tagKey}
            fullWidth
            label="Key"
            variant="outlined"
            required
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="tagValue"
            name="tagValue"
            onChange={(e) => {
              setTagValue(e.target.value);
            }}
            value={tagValue}
            fullWidth
            label="Value"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog}>Cancel</Button>
        <Button
          disabled={!tagKey}
          onClick={() => {
            dispatchTagState({
              type: "createNew",
              key: tagKey,
              value: tagValue,
            });
            closeDialog();
          }}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
}
