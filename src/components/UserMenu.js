import React, { useState } from "react";
import {
  AccountCircle as PersonIcon,
  Logout as LogoutIcon,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  Button,
  ButtonGroup,
  IconButton,
  Menu,
  Paper,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import VersionDialog from "./VersionModal";

export default function UserMenu({ user }) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [showVersionDialog, setVersionDialog] = useState(false);

  const closeVersionDialog = () => {
    setVersionDialog(false);
  };

  const openVersionDialog = () => {
    setVersionDialog(true);
  };

  return (
    <div>
      <Tooltip title="Account Info">
        <IconButton
          color="primary"
          aria-label="account"
          aria-controls="customized-menu"
          aria-haspopup="true"
          onClick={(event) => setAnchorEl(event.currentTarget)}
        >
          <Avatar sx={{ bgcolor: (theme) => theme.palette.appBar.userAvatar }}>
            {user.first_name.charAt(0).toUpperCase()}
            {user.last_name.charAt(0).toUpperCase()}
          </Avatar>
        </IconButton>
      </Tooltip>
      <Menu
        elevation={3}
        style={{ margin: ".5rem" }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        <Paper elevation={0}>
          <Stack alignItems="center" spacing={3} margin={0.5}>
            <Stack spacing={1} alignItems="center">
              <PersonIcon fontSize="large" color="primary" />
              <Typography fontWeight={500}>
                {user.first_name} {user.last_name}
              </Typography>
              <Typography variant="caption">{user.username}</Typography>
              <Typography variant="caption">{user.primary_email}</Typography>
            </Stack>

            <Button
              variant="outlined"
              startIcon={<LogoutIcon />}
              size="small"
              color="secondary"
              href="/logout"
              sx={{ textTransform: "none" }}
            >
              Sign Out
            </Button>

            <Box paddingX={1}>
              <ButtonGroup size="small">
                <Button
                  size="small"
                  variant="text"
                  href="https://cyverse.org/policies"
                  target="_blank"
                >
                  Policies
                </Button>

                <Button
                  size="small"
                  variant="text"
                  href="https://cyverse.org/about"
                  target="_blank"
                >
                  About
                </Button>

                <Button
                  size="small"
                  variant="text"
                  id="version_link"
                  target="_blank"
                  onClick={openVersionDialog}
                >
                  Version
                </Button>
              </ButtonGroup>
            </Box>

            <VersionDialog
              open={showVersionDialog}
              title="Ionosphere Version"
              handleClose={closeVersionDialog}
            />
          </Stack>
        </Paper>
      </Menu>
    </div>
  );
}
