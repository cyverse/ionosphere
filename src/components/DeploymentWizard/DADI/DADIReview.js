import {
  Box,
  Grid,
  ListItemText,
  List,
  ListItem,
  Typography,
} from "@mui/material";
import Alert from "@mui/material/Alert";

const DADIReview = (props) => {
  // const workspace = props.workspaces.find(w => w.id === props.workspaceId)
  return (
    <Box sx={{ margin: "2rem" }}>
      {props.changedParams?.length > 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid xs={12} md={9}>
            <Alert severity="info" variant="outlined">
              <Typography>
                Please review your changes (highlighed in{" "}
                <Box
                  component={"span"}
                  sx={{
                    color: "#ff007f",
                    fontWeight: "bold",
                    textDecoration: "underline",
                  }}
                >
                  pink and underlined
                </Box>
                ).
              </Typography>{" "}
            </Alert>
          </Grid>
        </Grid>
      )}
      {props.changedParams?.length === 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="flex-start"
        >
          <Grid xs={12}>
            <Box display="flex" justifyContent="center" mt={3}>
              <Typography style={{ color: "#ff007f" }}>
                No changes have been made.
              </Typography>
            </Box>
          </Grid>
        </Grid>
      )}
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignContent="center"
        alignItems="flex-start"
      >
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("regionId")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("regionId")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Region:{" "}
                  </Box>{" "}
                  {props.deploymentValues["regionId"]}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("instance_name")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "instance_name"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instance Name:{" "}
                  </Box>{" "}
                  {props.values.instance_name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Template:{" "}
                  </Box>{" "}
                  {props.wizard.template.name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {props.isEditing && (
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography
                    color="inherit"
                    style={{
                      color: props.changedParams?.includes("power_state")
                        ? "#ff007f"
                        : null,
                      textDecoration: props.changedParams?.includes(
                        "power_state"
                      )
                        ? "underline"
                        : null,
                    }}
                  >
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Power State:{" "}
                    </Box>
                    {props.values.power_state}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        )}
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Run dadi-cli?
                  </Box>{" "}
                  {props.values.run_dadi_cli ? "Yes" : "No"}
                </Typography>
              }
            />
          </ListItem>
        </Grid>

        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Run workqueue_factory?
                  </Box>{" "}
                  {props.values.run_workqueue_factory ? "Yes" : "No"}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {((props.values.run_dadi_cli && props.values.dadi_cli_use_workqueue) ||
          props.values.run_workqueue_factory) && (
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography color="inherit">
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Project Name
                    </Box>{" "}
                    {props.values.workqueue_project_name}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        )}
        {((props.values.run_dadi_cli && props.values.dadi_cli_use_workqueue) ||
          props.values.run_workqueue_factory) && (
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography color="inherit">
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Workqueue Password
                    </Box>{" "}
                    {props.values.workqueue_password}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        )}
        {props.values.run_dadi_cli && (
          <>
            <Grid xs={12} sm={12} md={6}>
              <ListItem>
                <ListItemText
                  primary={
                    <Typography color="inherit">
                      <Box component="span" sx={{ fontWeight: "bold" }}>
                        Use workqueue with dadi-cli?
                      </Box>{" "}
                      {props.values.dadi_cli_use_workqueue ? "Yes" : "No"}
                    </Typography>
                  }
                />
              </ListItem>
            </Grid>
            <Grid xs={12} sm={12} md={6}>
              <ListItem>
                <ListItemText
                  primary={
                    <Typography color="inherit">
                      <Box component="span" sx={{ fontWeight: "bold" }}>
                        Parameters
                      </Box>{" "}
                      {props.values.dadi_cli_parameters}
                    </Typography>
                  }
                />
              </ListItem>
            </Grid>
          </>
        )}
      </Grid>
    </Box>
  );
};

export default DADIReview;
