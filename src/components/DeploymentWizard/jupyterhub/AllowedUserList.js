import React from "react";
import { Box, Grid, Paper, Typography, Button, List } from "@mui/material";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";

export default function AllowedUsersList(props) {
  const [checked, setChecked] = React.useState([]);
  const [adminChecked, setAdminChecked] = React.useState([]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const handleAdminToggle = (value) => () => {
    const currentIndex = adminChecked.indexOf(value);
    const newChecked = [...adminChecked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setAdminChecked(newChecked);
  };

  const addAdmin = () => {
    props.addAdmin(checked);
    setChecked([]);
  };

  const removeAdmin = () => {
    props.removeAdmin(adminChecked);
    setAdminChecked([]);
  };

  const removeUsers = () => {
    props.removeUsers(checked);
    setChecked([]);
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      alignItems="flex-start"
      spacing={2}
    >
      <Grid item md={6}>
        <Typography
          variant="body1"
          component="h3"
          style={{ marginBottom: ".5rem" }}
        >
          Allowed Users
        </Typography>
        <Paper>
          <Box
            style={{
              height: "15rem",
              overflowY: "scroll",
              overflowX: "hidden",
            }}
          >
            <List sx={{ width: "100%", backgroundColor: "#ffff" }}>
              {props.users.map((value, index) => {
                const labelId = `checkbox-list-label-${value}`;

                return (
                  <ListItem
                    key={value}
                    role={undefined}
                    dense
                    button
                    onClick={handleToggle(value)}
                  >
                    <ListItemIcon>
                      <Checkbox
                        edge="start"
                        checked={checked.indexOf(value) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{
                          "aria-labelledby": labelId,
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText id={labelId} primary={`${value}`} />
                  </ListItem>
                );
              })}
            </List>
          </Box>
        </Paper>
        <Grid
          container
          direction="row"
          spacing={3}
          alignItems="flex-start"
          justifyContent="flex-start"
          style={{ marginTop: ".5rem" }}
        >
          <Grid item>
            <Button size="small" color="primary" onClick={removeUsers}>
              {" "}
              Delete{" "}
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              size="small"
              color="primary"
              onClick={addAdmin}
            >
              Make Admin
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item md={6}>
        <Typography
          variant="body1"
          component="h3"
          style={{ marginBottom: ".5rem" }}
        >
          Administrators
        </Typography>

        <Paper>
          <Box
            style={{
              height: "15rem",
              overflowY: "scroll",
              overflowX: "hidden",
            }}
          >
            <List sx={{ width: "100%", backgroundColor: "#ffff" }}>
              {props.admin.map((value, index) => {
                const labelId = `checkbox-list-label-${value}`;

                return (
                  <ListItem
                    key={value}
                    role={undefined}
                    dense
                    button
                    onClick={handleAdminToggle(value)}
                  >
                    <ListItemIcon>
                      <Checkbox
                        edge="start"
                        checked={adminChecked.indexOf(value) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{
                          "aria-labelledby": labelId,
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText id={labelId} primary={`${value}`} />
                  </ListItem>
                );
              })}
            </List>
          </Box>
        </Paper>
        <Grid
          container
          direction="row"
          justifyContent="flex-end"
          alignItems="center"
          spacing={3}
          style={{ marginTop: ".5rem" }}
        >
          <Grid item>
            <Button size="small" color="primary" onClick={removeAdmin}>
              Remove Admin
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
