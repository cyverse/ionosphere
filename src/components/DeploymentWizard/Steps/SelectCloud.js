import {
  Box,
  CircularProgress,
  DialogContent,
  Grid,
  Stack,
  Typography,
} from "@mui/material";
import { CloudQueue as CloudOutlineIcon } from "@mui/icons-material";

import { useCloudMenuForm } from "@/contexts/cloudMenuForm";
import {
  setCloud,
  setProject,
  setCredentialId,
} from "@/contexts/cloudMenuForm";

import LabeledDropdownMenu from "@/components/Common/Form/LabeledDropdownMenu";

/**
 * Prerequisite step in deployment wizard to select a cloud and project for the deployment.
 */
export default function SelectCloud() {
  const {
    cloudMenu,
    cloudMenuDispatch,
    cloudOptions,
    projectOptions,
    loadingProjects,
    credentialOptions,
    loadingCredentials,
  } = useCloudMenuForm();

  return (
    <DialogContent style={{ minHeight: "40vh" }}>
      <Box
        mt={2}
        mb={2}
        sx={{
          height: "95%",
          overflowX: "hidden",
          overflowY: "hidden",
        }}
      >
        <Box
          mt={4}
          display={"flex"}
          flexDirection={"column"}
          justifyContent={"center"}
          sx={{ width: "100%" }}
        >
          <Grid
            container
            spacing={2}
            paddingX={8}
            paddingBottom={4}
            justifyContent={"center"}
          >
            <Grid item xs={8}>
              <Typography style={{ fontSize: "1.25rem" }}>
                Select the cloud to deploy to:
              </Typography>
            </Grid>

            {cloudMenu.cloud && (
              <Grid item xs={8}>
                <LabeledDropdownMenu
                  label={
                    <Stack
                      direction={"row"}
                      alignItems={"flex-end"}
                      spacing={1}
                    >
                      <CloudOutlineIcon />
                      <Typography>Cloud</Typography>
                    </Stack>
                  }
                  icon={<CloudOutlineIcon />}
                  selectedId={cloudMenu.cloud?.id}
                  options={cloudOptions}
                  handleSelect={(selected) => {
                    cloudMenuDispatch(setCredentialId(null));
                    cloudMenuDispatch(setProject(null));
                    cloudMenuDispatch(setCloud(selected));
                  }}
                />
              </Grid>
            )}

            {loadingProjects || loadingCredentials ? (
              <Grid
                item
                xs={8}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <Box
                  m={2}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                >
                  <CircularProgress color="primary" />
                </Box>
              </Grid>
            ) : (
              <>
                {cloudMenu.cloud?.hasProjects && (
                  <Grid item xs={8}>
                    <LabeledDropdownMenu
                      label={"Project"}
                      selectedId={cloudMenu.project?.id}
                      options={projectOptions}
                      handleSelect={(selected) => {
                        cloudMenuDispatch(setCredentialId(null));
                        cloudMenuDispatch(setProject(selected));
                      }}
                    />
                  </Grid>
                )}
                {(!cloudMenu.cloud.hasProjects ||
                  (cloudMenu.cloud.hasProjects &&
                    credentialOptions.length > 1)) &&
                  cloudMenu.credentialId && (
                    <Grid item xs={8}>
                      <LabeledDropdownMenu
                        label={"Credential"}
                        selectedId={cloudMenu.credentialId}
                        options={credentialOptions?.map((p) => {
                          return { id: p.id, name: p.name };
                        })}
                        handleSelect={(selected) =>
                          cloudMenuDispatch(setCredentialId(selected.id))
                        }
                      />
                    </Grid>
                  )}
              </>
            )}
          </Grid>
        </Box>
      </Box>
    </DialogContent>
  );
}
