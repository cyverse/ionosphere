import {
  Box,
  Typography,
  Grid,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import Alert from "@mui/material/Alert";

const SelectRegion = ({
  regions,
  selectedId,
  setRegionId,
  loadingRegions,
  isEditing,
}) => {
  return (
    <Box m={1}>
      <Grid
        container
        justifyContent="center"
        alignItems="flex-start"
        spacing={1}
      >
        {isEditing && (
          <Grid item xs={12} sm={10} md={8}>
            <Box display="flex" justifyContent="center" mb={3}>
              <Alert severity="info">
                Some values are not editable and have been disabled. If you need
                to change these values, please create a new deployment.
              </Alert>
            </Box>
          </Grid>
        )}
      </Grid>

      <Grid
        container
        justifyContent="center"
        alignItems="flex-start"
        spacing={1}
      >
        {loadingRegions && (
          <Grid item xs={12}>
            <Box display="flex" justifyContent="center">
              <Box sx={{ display: "flex", justifyContent: "center" }}>
                <CircularProgress />
              </Box>
            </Box>
          </Grid>
        )}

        {!loadingRegions && (
          <>
            <Grid item xs={12} sm={7}>
              <FormControl variant="outlined" fullWidth>
                <Typography
                  style={{ fontSize: "1.25rem", marginBottom: "1em" }}
                >
                  Select a region for your deployment:
                </Typography>
              </FormControl>
            </Grid>

            <Grid item xs={12} sm={7}>
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="region" shrink>
                  Region
                </InputLabel>
                <Select
                  name="region"
                  id="region"
                  value={selectedId}
                  label="Region"
                  onChange={(e) => setRegionId(e.target.value)}
                  renderValue={(option) => option}
                  disabled={isEditing}
                >
                  {regions &&
                    regions.map((region, index) => (
                      <MenuItem key={index} value={region.id}>
                        {region.name}
                      </MenuItem>
                    ))}
                  {!regions && (
                    <MenuItem key={0} value={selectedId}>
                      {selectedId}
                    </MenuItem>
                  )}
                </Select>
              </FormControl>
            </Grid>
          </>
        )}
      </Grid>
    </Box>
  );
};

export default SelectRegion;
