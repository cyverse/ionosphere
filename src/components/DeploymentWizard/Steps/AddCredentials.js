import { Box, DialogContent } from "@mui/material";
import DeployPrerequisiteForm from "@/components/forms/DeployPrerequisiteForm";
import { useDeploymentWizard } from "../contexts/DeploymentWizardContext";

/**
 * Prerequisite step in deployment wizard to add a cloud credential and ssh key.
 */
export default function AddCredentials({ hasCloudCredential }) {
  return (
    <DialogContent>
      <Box m={4}>
        <DeployPrerequisiteForm hasCloudCredential={hasCloudCredential} />
      </Box>
    </DialogContent>
  );
}
