import AddCredentials from "./AddCredentials";
import SelectCloud from "./SelectCloud";
import SelectRegion from "./SelectRegion";
import SelectTemplate from "./SelectTemplate";
import PrerequisiteStepControls from "./PrerequisiteStepControls";
import DeploymentSuccess from "./DeploymentSuccess";

export {
  AddCredentials,
  SelectCloud,
  SelectRegion,
  SelectTemplate,
  PrerequisiteStepControls,
  DeploymentSuccess,
};
