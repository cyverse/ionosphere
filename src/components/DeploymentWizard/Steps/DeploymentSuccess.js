import React from "react";
import { Button, DialogContent, Link, Stack } from "@mui/material";
import DeploymentConfirmation from "@/components/Deployment/DeploymentConfirmation";
import { useRouter } from "next/router";

const DeploymentSuccess = ({ handleClose, handlePostCreate }) => {
  const router = useRouter();

  return (
    <>
      <DialogContent>
        <DeploymentConfirmation />
      </DialogContent>

      <Stack direction="row" spacing={1} my={4} justifyContent="center">
        <Button variant="outlined" onClick={handleClose}>
          Close
        </Button>
        <Link href="/deployments" passHref>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handlePostCreate();
              router.push("/deployments");
            }}
          >
            View Deployments
          </Button>
        </Link>
      </Stack>
    </>
  );
};

export default DeploymentSuccess;
