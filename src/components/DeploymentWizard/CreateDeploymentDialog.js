import React, { useEffect, useMemo } from "react";
import getConfig from "next/config";
import { useRouter } from "next/router";

import { Box, Dialog, DialogContent, Typography } from "@mui/material";

import {
  CloudQueue as CloudOutlineIcon,
  RocketLaunch as RocketLaunchIcon,
} from "@mui/icons-material";

import CreateWizardController from "./CreateWizardController";
import {
  AddCredentials,
  SelectCloud,
  SelectTemplate,
  PrerequisiteStepControls,
  DeploymentSuccess,
} from "./Steps";
import {
  useAPI,
  useClouds,
  useCredentials,
  useError,
  useUserConfig,
  useUserRecents,
} from "@/contexts";
import {
  useDeploymentWizard,
  useDeploymentWizardDispatch,
} from "./contexts/DeploymentWizardContext";
import { useCloudMenuForm } from "@/contexts/cloudMenuForm";

import { setSubmittingDeployment } from "./contexts/actions/actions";
import DialogAppBar from "../Common/Dialog/DialogAppBar";

import { filterEligibleClouds } from "@/utils/cacaoUtils";

// Constants for steps
const STEPS = {
  CREDENTIAL: "credentialStep",
  CLOUD: "cloudStep",
  TEMPLATE: "templateStep",
  SUCCESS: "successStep",
};

/**
 * Builds the list of prerequisite steps.
 * @param {Array} credentials list of credentials
 * @param {Object} deploymentWizard deployment wizard context
 * @returns {Array} - List of steps
 */
const buildPrerequisiteSteps = (
  credentials,
  clouds,
  templateCloudTypes,
  cloudId,
  credentialId
) => {
  let steps = [];

  const addStepIf = (condition, step) => {
    if (condition) steps.push(step);
  };

  // add credential step if user is missing ssh key or valid credential for selected template
  const cloudOptions = filterEligibleClouds(
    clouds,
    templateCloudTypes,
    credentials
  );

  addStepIf(
    !credentials.find((c) => c.type === "ssh") || cloudOptions.length === 0,
    STEPS.CREDENTIAL
  );

  addStepIf(!credentialId || !cloudId, STEPS.CLOUD);

  // After all prerequisite steps, proceed to template steps (starts at 0)
  steps.push(0);

  return steps;
};

export default function CreateDeploymentDialog({
  handleClose,
  handlePostCreate,
  open,
  templates,
}) {
  // Get info from deployment wizard context
  const deploymentWizard = useDeploymentWizard();
  const { template, regions, templateCloudTypes } = deploymentWizard;

  const { cloudMenu } = useCloudMenuForm();
  const { cloud, project, credentialId } = cloudMenu;

  const dispatch = useDeploymentWizardDispatch();
  const config = getConfig().publicRuntimeConfig;

  // Get credentials from context
  const [credentials] = useCredentials();
  const [_, setError] = useError();
  const [clouds] = useClouds();
  const [userRecents, updateUserRecents] = useUserRecents();

  const router = useRouter();
  const api = useAPI();
  const [userConfig] = useUserConfig();

  const hasCloudCredential = useMemo(() => {
    return (
      filterEligibleClouds(clouds, templateCloudTypes, credentials).length > 0
    );
  }, [credentials, clouds, templateCloudTypes]);

  const prereqSteps = React.useRef(
    buildPrerequisiteSteps(
      credentials,
      clouds,
      templateCloudTypes,
      cloud?.id,
      credentialId
    )
  );

  // Start at first prerequisite step
  const [activeStep, setActiveStep] = React.useState(prereqSteps.current[0]);

  // TODO: Move regionId to deploymentWizard context
  const [regionId, setRegionId] = React.useState(null);

  // Handle navigation for prerequisite steps
  const handleNext = () => {
    const steps = prereqSteps.current;
    const currentIndex = steps.indexOf(activeStep);
    if (currentIndex < steps.length - 1) {
      setActiveStep(steps[currentIndex + 1]);
    }
  };
  const handlePrev = () => {
    const steps = prereqSteps.current;
    const currentIndex = steps.indexOf(activeStep);
    if (currentIndex > 0) {
      setActiveStep(steps[currentIndex - 1]);
    }
  };

  // Once regions are loaded, set the default region to IU
  // TODO: Move to deploymentWizard context
  useEffect(() => {
    if (regions.length) {
      let defaultRegion = null;
      const cloudType = cloud?.type;
      if (cloudType === "openstack") {
        defaultRegion = regions.find((r) => r.id === "IU") || regions[0];
      } else if (cloudType === "aws") {
        defaultRegion =
          regions.find((r) => r.id === userConfig?.aws_default_region) ||
          regions[0];
      }
      setRegionId(defaultRegion?.id);
    }
  }, [regions]);

  /**
   * Calls API to creates a deployment and run using the provided params.
   *
   * Upon successful creation, performs post-create tasks.
   * @param {*} deploymentParams
   * @param {*} runParams
   */
  const handleCreateDeployment = async (deploymentParams, runParams) => {
    // display loading indicator
    dispatch(setSubmittingDeployment(true));

    console.log("Deployment params:", deploymentParams);
    console.log("Run params:", runParams);

    /**
     * If simulation mode is off, create deployment.
     *
     * Simulation mode can be used by setting env var SIMULATE_DEPLOYMENT_SUBMIT=true.
     * It is helpful during development so deployments are not actually created.
     */
    if (!(config.SIMULATE_DEPLOYMENT_SUBMIT === "true")) {
      try {
        // create deployment
        const res = await api.createDeployment(deploymentParams);

        // create run
        await api.createDeploymentRun(res.tid, {
          deployment_id: res.tid,
          parameters: runParams,
        });

        // update user recent cloud/project/credential
        updateRecents(deploymentParams, runParams);

        // if not on deployments page, show success confimation and option to redirect to deployments page
        if (router.pathname !== "/deployments") {
          setActiveStep(STEPS.SUCCESS);
        } else {
          handlePostCreate();
        }
      } catch (error) {
        // display error & remove loading indicator
        setError(api.errorMessage(error));
        dispatch(setSubmittingDeployment(false));
      }
    } else {
      if (router.pathname !== "/deployments") {
        setActiveStep(STEPS.SUCCESS);
      } else {
        handlePostCreate();
      }
    }
  };

  /**
   * Update user recent cloud/project/credential after deployment is successfully created.
   */
  const updateRecents = (deploymentParams, runParams) => {
    try {
      const cloudId = deploymentParams.primary_provider_id;

      if (userRecents.cloud !== cloudId) {
        updateUserRecents("cloud", cloudId);
      }

      const projectParam = runParams.find((p) => p.key === "project");

      if (
        projectParam &&
        userRecents[`project-${cloudId}`] !== projectParam.value
      ) {
        updateUserRecents(`project-${cloudId}`, projectParam.value);
      }

      if (
        userRecents[`credential-${cloudId}`] !==
        deploymentParams.cloud_credentials[0]
      ) {
        updateUserRecents(
          `credential-${cloudId}`,
          deploymentParams.cloud_credentials[0]
        );
      }
    } catch (error) {
      console.error("Error updating user recents:", error);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={(_, reason) =>
        reason !== "backdropClick" && handleClose && handleClose()
      }
      maxWidth="md"
      fullWidth
    >
      <DialogAppBar
        handleClose={handleClose}
        title={template ? "New Deployment: " + template.name : "New Deployment"}
        icon={<RocketLaunchIcon fontSize="small" />}
        endText={
          cloud && (
            <>
              <CloudOutlineIcon />
              <Typography style={{ marginLeft: "0.5em" }}>
                {cloud.name?.toUpperCase()}
                {cloud.hasProjects && project ? ` / ${project.name}` : ""}
              </Typography>
            </>
          )
        }
      />

      {activeStep === STEPS.CREDENTIAL && (
        <>
          <AddCredentials hasCloudCredential={hasCloudCredential} />
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={!hasCloudCredential}
          />
        </>
      )}

      {activeStep === STEPS.CLOUD && (
        <>
          <SelectCloud />
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={
              !cloud || (cloud?.hasProjects && !project) || !credentialId
            }
          />
        </>
      )}

      {activeStep === STEPS.TEMPLATE && (
        <>
          <SelectTemplate templates={templates}></SelectTemplate>
          <PrerequisiteStepControls
            {...{ handleNext, handlePrev }}
            disablePrev={activeStep === prereqSteps.current[0]}
            disableNext={!template}
          />
        </>
      )}

      {activeStep >= 0 && (
        <CreateWizardController
          activeStep={activeStep}
          setActiveStep={setActiveStep}
          handlePrev={handlePrev}
          submitHandler={handleCreateDeployment}
          regionId={regionId}
          setRegionId={setRegionId}
        ></CreateWizardController>
      )}

      {activeStep === STEPS.SUCCESS && (
        <DeploymentSuccess
          handleClose={handleClose}
          handlePostCreate={handlePostCreate}
        />
      )}
    </Dialog>
  );
}
