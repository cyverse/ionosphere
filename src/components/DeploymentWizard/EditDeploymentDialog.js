/* eslint-disable no-unused-vars */
import { useState } from "react";
import { Dialog, Typography } from "@mui/material";
import {
  CloudQueue as CloudOutlineIcon,
  RocketLaunch as RocketLaunchIcon,
} from "@mui/icons-material";

import EditWizardController from "./EditWizardController";
import { useClouds } from "@/contexts";
import DialogAppBar from "@/components/Common/Dialog/DialogAppBar";

export default function EditDeploymentDialog(props) {
  const getDeploymentParam = (paramName) => {
    return props.deployment.parameters.find((p) => p.key === paramName)?.value;
  };

  const [clouds] = useClouds();

  const [activeStep, setActiveStep] = useState(0);

  const [regions, setRegions] = useState(null);
  const [loadingRegions, setLoadingRegions] = useState(false);
  const [regionId, setRegionId] = useState(getDeploymentParam("region"));

  // get cloud name
  const cloud = clouds.find((c) => c.id === props.cloudId);

  return (
    <Dialog
      open={props.open}
      onClose={(_, reason) =>
        reason !== "backdropClick" && props.handleClose && props.handleClose()
      }
      maxWidth="md"
      fullWidth
    >
      <DialogAppBar
        handleClose={props.handleClose}
        title={`Edit Deployment: ${props.deployment.name}`}
        icon={<RocketLaunchIcon fontSize="small" />}
        endText={
          cloud && (
            <>
              <CloudOutlineIcon />
              <Typography style={{ marginLeft: "0.5em" }}>
                {cloud.name.toUpperCase()}
                {cloud.hasProjects ? ` / ${getDeploymentParam("project")}` : ""}
              </Typography>
            </>
          )
        }
      />

      {activeStep >= 0 && (
        <EditWizardController
          cloudId={props.cloudId}
          activeStep={activeStep}
          setActiveStep={setActiveStep}
          submitHandler={props.handleSubmit}
          template={props.template}
          deploymentValues={{
            workspaceId: props.deployment.workspace_id,
            templateId: props.deployment.template_id,
            credentialId: props.deployment.cloud_credentials[0],
            cloudId: props.deployment.primary_provider_id,
            projectId: getDeploymentParam("project"),
            regionId: regionId,
          }}
          deployment={props.deployment}
          setRegionId={setRegionId}
          regions={regions}
          loadingRegions={loadingRegions}
          handleClose={props.handleClose}
          getDeploymentParam={getDeploymentParam}
        ></EditWizardController>
      )}
    </Dialog>
  );
}
