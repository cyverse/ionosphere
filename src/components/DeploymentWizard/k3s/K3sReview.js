import {
  Box,
  Grid,
  Chip,
  Typography,
  ListItem,
  ListItemText,
} from "@mui/material";
import Alert from "@mui/material/Alert";
import { BootDiskReview } from "../Fields/BootDisk";

const K3sReview = (props) => {
  return (
    <Box sx={{ margin: "2rem" }}>
      {props.changedParams?.length > 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid item xs={12} md={9}>
            <Box display="flex" justifyContent="center" mb={2}>
              <Alert severity="info" variant="outlined">
                <Typography>
                  Please review your changes (highlighed in{" "}
                  <Box
                    component={"span"}
                    sx={{
                      color: "#ff007f",
                      fontWeight: "bold",
                      textDecoration: "underline",
                    }}
                  >
                    pink and underlined
                  </Box>
                  ).
                </Typography>{" "}
              </Alert>
            </Box>
          </Grid>
        </Grid>
      )}
      {props.changedParams?.length === 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Box display="flex" justifyContent="center" mb={2}>
              <Alert severity="info">No changes have been made.</Alert>
            </Box>
          </Grid>
        </Grid>
      )}

      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignContent="flex-start"
        alignItems="flex-start"
      >
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Region:{" "}
                  </Box>{" "}
                  {props.deploymentValues["regionId"]}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("instance_name")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "instance_name"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instance Name:{" "}
                  </Box>{" "}
                  {props.values.instance_name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Template:{" "}
                  </Box>{" "}
                  {props.template.name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {props.isEditing && (
          <Grid xs={12} sm={12} md={6}>
            <ListItem>
              <ListItemText
                primary={
                  <Typography
                    color="inherit"
                    style={{
                      color: props.changedParams?.includes("power_state")
                        ? "#ff007f"
                        : null,
                      textDecoration: props.changedParams?.includes(
                        "power_state"
                      )
                        ? "underline"
                        : null,
                    }}
                  >
                    <Box component="span" sx={{ fontWeight: "bold" }}>
                      Power State:{" "}
                    </Box>
                    {props.values.power_state}
                  </Typography>
                }
              />
            </ListItem>
          </Grid>
        )}
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("image_name")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("image_name")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Image Name:{" "}
                  </Box>{" "}
                  {props.values.image_name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("flavor")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes("flavor")
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Size:{" "}
                  </Box>{" "}
                  {props.values.flavor}
                  {props.values.flavor.startsWith("g3") && (
                    <Chip
                      size="small"
                      label="GPU"
                      style={{ marginLeft: ".7em" }}
                    />
                  )}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("instance_count")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "instance_count"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Instance Count:{" "}
                  </Box>
                  {props.values.instance_count}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography
                  color="inherit"
                  style={{
                    color: props.changedParams?.includes("k3s_traefik_disable")
                      ? "#ff007f"
                      : null,
                    textDecoration: props.changedParams?.includes(
                      "k3s_traefik_disable"
                    )
                      ? "underline"
                      : null,
                  }}
                >
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Traefik Disabled:{" "}
                  </Box>
                  {props.values.k3s_traefik_disable ? "Yes" : "No"}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <BootDiskReview values={props.values} />
      </Grid>
    </Box>
  );
};

export default K3sReview;
