import {
  FormControl,
  FormHelperText,
  Grid,
  InputAdornment,
  TextField,
} from "@mui/material";

export default function SizeGB({ disabled, half, label, name, wizard }) {
  let value = wizard.values[name];
  if (value === -1) value = 0;
  return (
    <Grid item xs={12} sm={half ? 6 : 12}>
      <FormControl variant="outlined" fullWidth>
        <TextField
          id={name}
          name={name}
          value={value}
          disabled={disabled}
          onChange={(e) => wizard.onChange(name, e.target.value)}
          label={label}
          type="number"
          placeholder="100"
          step="1"
          fullWidth
          InputProps={{
            endAdornment: <InputAdornment position="end">GB</InputAdornment>,
          }}
          variant="outlined"
          helperText={
            !disabled &&
            value < 1 &&
            "Enter value greater than 0 or select local storage"
          }
          required
          error={!disabled && value < 1}
        />
        <FormHelperText>1 TB = 1,000 GB</FormHelperText>
      </FormControl>
    </Grid>
  );
}
