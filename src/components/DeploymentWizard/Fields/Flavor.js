import {
  Box,
  Chip,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from "@mui/material";

export default function Flavor({
  default_value,
  item,
  helperText,
  label,
  name,
  wizard,
}) {
  label = item?.ui_label || label || "Size";
  let param = wizard.get_param?.(item);
  name = param?.name || name || "flavor";
  let value =
    wizard.flavors?.find((flavor) => flavor.name === wizard.values[name])
      ?.name ||
    param?.default ||
    default_value;
  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel>{label}</InputLabel>
      <Select
        name={name}
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        value={value}
        label={label}
        onChange={(e) => wizard.onChange(name, e.target.value)}
        renderValue={(option) => (
          <>
            {option}
            {option.startsWith("g3") && (
              <Chip
                size="small"
                label="GPU"
                style={{ marginLeft: 5, marginTop: "-0.45em" }}
              />
            )}
          </>
        )}
      >
        {wizard.flavors?.map((flavor, index) => (
          <MenuItem key={index} value={flavor.name}>
            <Box display="flex" alignItems="center">
              <Typography style={{ fontWeight: "bold", width: "8em" }}>
                {flavor.name}
              </Typography>
              <Box style={{ fontWeight: "bold", width: "3.5em" }}>
                {flavor.name.startsWith("g3") && (
                  <Chip size="small" label="GPU" />
                )}
              </Box>
              <Typography variant="body2" style={{ width: "4.375rem" }}>
                vCPU: {flavor.vcpus}
              </Typography>
              <Typography variant="body2" style={{ width: "5.9375rem" }}>
                RAM: {flavor.ram}
              </Typography>
              <Typography variant="body2" style={{ width: "3.75rem" }}>
                Disk: {flavor.disk}
              </Typography>
            </Box>
          </MenuItem>
        ))}
      </Select>
      {(item?.helper_text || helperText) && (
        <FormHelperText>{item?.helper_text || helperText}</FormHelperText>
      )}
    </FormControl>
  );
}
