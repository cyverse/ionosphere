import { FormControl, TextField } from "@mui/material";

export default function Number({
  item,
  wizard,
  name,
  label,
  minValue,
  numType,
}) {
  let param = item && wizard.get_param?.(item);
  name = name || param?.name;
  label = item?.ui_label || label || name;
  let required = param?.required;

  const isInteger = param?.type === "integer" || numType === "integer";

  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        name={name}
        disabled={wizard.isEditing && !wizard.isParamEditable?.(name)}
        label={label}
        type="number"
        inputProps={{
          step: isInteger ? "1" : "0.1",
        }}
        value={wizard.values[name]}
        onChange={(e) => {
          wizard.onChange(name, e.target.value);
        }}
        error={wizard.values[name] < minValue}
        helperText={
          wizard.values[name] < minValue
            ? `Value must be greater than or equal to ${minValue}`
            : ""
        }
        onKeyDown={(e) => {
          if (isInteger && (e.key === "." || e.key === "e")) e.preventDefault();
        }}
        variant="outlined"
        required={required}
      />
    </FormControl>
  );
}
