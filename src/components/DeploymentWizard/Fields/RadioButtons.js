import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@mui/material";

export default function RadioButtons({ item, name, label, required, wizard }) {
  let param = item && wizard.get_param?.(item);
  name = name || param?.name;
  label = item?.ui_label || label || name;
  required = item ? item.required : required;
  return (
    <FormControl variant="outlined" fullWidth>
      <FormLabel>{label}</FormLabel>
      <RadioGroup
        name={name}
        value={wizard.values[name] || param?.default || null}
        variant="outlined"
      >
        {param?.enum?.map((o) => (
          <FormControlLabel
            control={<Radio />}
            disabled={
              wizard.isEditing &&
              !(param?.editable || wizard.isParamEditable?.(name))
            }
            key={o}
            label={o}
            onChange={(e) => wizard.onChange(name, e.target.value)}
            required={item?.required || (!item && required)}
            value={o}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
}
