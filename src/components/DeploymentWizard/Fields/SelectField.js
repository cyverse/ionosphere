import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

export default function SelectField({ item, name, label, required, wizard }) {
  let param = item && wizard.get_param?.(item);
  name = name || param?.name;
  label = item?.ui_label || label || name;
  required = item ? item.required : required;
  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel id="demo-simple-select-helper-label">{label}</InputLabel>
      <Select
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        fullWidth
        id={name}
        label={label}
        name={name}
        onChange={(e) => wizard.onChange(name, e.target.value)}
        required={item?.required || (!item && required)}
        value={wizard.values[name] || param?.default}
        variant="outlined"
      >
        {param?.enum?.map((o) => (
          <MenuItem key={o} value={o}>
            {o}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
