import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const powerStateOptions = ["active", "shutoff", "shelved_offloaded"];

export default function PowerState({ item, wizard }) {
  let param = item && wizard.get_param?.(item);
  let name = param?.name || "power_state";
  let label = item?.ui_label || "Power State";
  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel>Power State</InputLabel>
      <Select
        name={name}
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        value={wizard.values[name] || param?.default}
        label={label}
        onChange={(e) => wizard.onChange(name, e.target.value)}
        renderValue={(option) => option}
      >
        {powerStateOptions.map((option, index) => (
          <MenuItem key={index} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
