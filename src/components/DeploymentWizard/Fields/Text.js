import { FormControl, TextField } from "@mui/material";

export default function Text({
  item,
  name,
  label,
  helperText,
  required,
  wizard,
}) {
  let param = item && wizard.get_param?.(item);
  name = name || param?.name;
  label = item?.ui_label || label || name;
  required = param?.required || required;
  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        fullWidth
        helperText={item?.helper_text || helperText}
        id={name}
        label={label}
        minRows={item?.field_type === "textarea" ? 2 : 0}
        multiline={item?.field_type === "textarea"}
        name={name}
        onChange={(e) => wizard.onChange(name, e.target.value)}
        required={required}
        type={item?.field_type === "password" ? "password" : "text"}
        value={wizard.values[name] || param?.default || ""}
        variant="outlined"
      />
    </FormControl>
  );
}
