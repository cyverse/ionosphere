import { FormControl, TextField } from "@mui/material";

export default function InstanceCount({ item, wizard }) {
  let param = item && wizard.get_param?.(item);
  let name = param?.name || "instance_count";
  let label = item?.ui_label || "No. of Instances";
  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        name={name}
        disabled={
          wizard.isEditing &&
          !(param?.editable || wizard.isParamEditable?.(name))
        }
        label={label}
        type="number"
        step="1"
        value={wizard.values[name] || param?.default || 1}
        onChange={(e) => {
          // prevent decrementing below 1
          if (e.target.value < 1) {
            e.preventDefault();
          } else {
            wizard.onChange(name, e.target.value);
          }
        }}
        // prevent typing
        onKeyDown={(e) => {
          e.preventDefault();
        }}
        // hide cursor
        style={{ caretColor: "transparent" }}
        variant="outlined"
        required
        readOnly
      />
    </FormControl>
  );
}
