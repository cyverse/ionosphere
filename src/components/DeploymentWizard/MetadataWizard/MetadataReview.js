import { Box, Grid, Typography, ListItem, ListItemText } from "@mui/material";
import Alert from "@mui/material/Alert";
import { BootDiskAddReviewItems } from "../Fields/BootDisk";

const MetadataReview = ({ changedParams, deploymentValues, wizard }) => {
  let components = [];
  const add_item = (name, label) => {
    label = label || name;
    const param = wizard.get_param({ name: name });
    const item = wizard.get_item(param);
    let value = wizard.values[name];
    if (item?.field_type === "password") {
      label += ": ";
      value = "*".repeat(value.length);
    } else if (typeof value == "boolean") {
      value = value ? "yes" : "no";
      label += " ";
    } else label += ": ";
    components.push(
      <Grid item key={name} xs={12} sm={12} md={6}>
        <ListItem>
          <ListItemText
            primary={
              <Typography
                color="inherit"
                style={{
                  color: changedParams?.includes(name) ? "#ff007f" : null,
                  textDecoration: changedParams?.includes(name)
                    ? "underline"
                    : null,
                }}
              >
                <Box component="span" sx={{ fontWeight: "bold" }}>
                  {label}
                </Box>
                {value}
              </Typography>
            }
          />
        </ListItem>
      </Grid>
    );
  };
  wizard.template.metadata.parameters.forEach((p) => {
    if (p.ignore) return;
    if (p.name === "root_storage_source") {
      BootDiskAddReviewItems(add_item, wizard.values);
      return;
    }
    if (p.name === "power_state" && !wizard.isEditing) return;
    const item = wizard.get_item(p);
    if (item) add_item(p.name, item.ui_label);
  });
  return (
    <Box sx={{ margin: "2rem" }}>
      {changedParams?.length > 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid item xs={12} md={9}>
            <Box display="flex" justifyContent="center" mb={2}>
              <Alert severity="info" variant="outlined">
                <Typography>
                  Please review your changes (highlighed in{" "}
                  <Box
                    component={"span"}
                    sx={{
                      color: "#ff007f",
                      fontWeight: "bold",
                      textDecoration: "underline",
                    }}
                  >
                    pink and underlined
                  </Box>
                  ).
                </Typography>{" "}
              </Alert>
            </Box>
          </Grid>
        </Grid>
      )}
      {changedParams?.length === 0 && (
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Box display="flex" justifyContent="center" mb={2}>
              <Alert severity="info">No changes have been made.</Alert>
            </Box>
          </Grid>
        </Grid>
      )}
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignContent="flex-start"
        alignItems="flex-start"
      >
        <Grid item xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography color="inherit">
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Template:{" "}
                  </Box>{" "}
                  {wizard.template.name}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <ListItem>
            <ListItemText
              primary={
                <Typography>
                  <Box component="span" sx={{ fontWeight: "bold" }}>
                    Region:{" "}
                  </Box>{" "}
                  {deploymentValues["regionId"]}
                </Typography>
              }
            />
          </ListItem>
        </Grid>
        {components}
      </Grid>
    </Box>
  );
};

export default MetadataReview;
