import Alert from "@mui/material/Alert";
import { Typography } from "@mui/material";

const VM_ASSIGNMENTS_LOCATION = "/opt/student_assignments.csv";

export default function VMAssignmentInfo() {
  return (
    <Alert severity="info">
      <Typography variant="caption">
        VM assignments can be found on all instructor VMs at{" "}
        <strong>{VM_ASSIGNMENTS_LOCATION}</strong>.
      </Typography>
    </Alert>
  );
}
