import {
  Box,
  Typography,
  Switch,
  FormHelperText,
  Grid,
  TextField,
  FormControl,
  FormControlLabel,
  NativeSelect,
  InputLabel,
} from "@mui/material";

const Storage = ({ values, onChange, isEditing, isParamEditable }) => {
  return (
    <>
      <Box sx={{ maxWidth: "90%", margin: "0 auto" }}>
        <Box>
          <Typography
            variant="h6"
            component="h2"
            style={{ fontSize: "1.25rem", marginBottom: "1em" }}
          >
            Storage Settings
          </Typography>
        </Box>
        <Grid container spacing={3} mb={3}>
          <Grid item md={12}>
            <Box mb={3}>
              <FormControl variant="outlined" fullWidth>
                <FormControlLabel
                  control={
                    <Switch
                      checked={values["enable_shared_storage"]}
                      // disabled={isEditing && !isParamEditable("share_name")}
                      onChange={(e) =>
                        onChange("enable_shared_storage", e.target.checked)
                      }
                    />
                  }
                  label="Enable Shared Storage"
                />
              </FormControl>
              <FormHelperText>
                Enable shared storage to configure storage options. This assumes
                the share has already been created.
              </FormHelperText>
            </Box>
          </Grid>
        </Grid>

        {values["enable_shared_storage"] && (
          <>
            <Grid container direction="row" spacing={3}>
              {/* <Grid item sm={6} xs={12}>
                <TextField
                  id="storage_size"
                  name="storage_size"
                  value={values["storage_size"]}
                  disabled={isEditing && !isParamEditable("storage_size")}
                  onChange={(e) => onChange("storage_size", e.target.value)}
                  label="Storage in GB"
                  type="number"
                  placeholder="100"
                  step="1"
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">GB</InputAdornment>
                    ),
                  }}
                  variant="outlined"
                  helperText={
                    values["jh_storage_size"] < 1 &&
                    "Please enter a value greater than 0 or disable shared storage."
                  }
                  required
                  error={values["storage_size"] < 1}
                />
                <FormHelperText>1 TB = 1,000 GB</FormHelperText>
              </Grid> */}
              <Grid item sm={6} xs={12}>
                <TextField
                  id="share_name"
                  name="share_name"
                  value={values["share_name"]}
                  disabled={isEditing && !isParamEditable("share_name")}
                  onChange={(e) => onChange("share_name", e.target.value)}
                  label="Share Name"
                  fullWidth
                  variant="outlined"
                  helperText={""}
                  required
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  id="share_mount_path"
                  name="share_mount_path"
                  value={values["share_mount_path"]}
                  disabled={isEditing && !isParamEditable("share_mount_path")}
                  onChange={(e) => onChange("share_mount_path", e.target.value)}
                  label="Share Mount Path"
                  fullWidth
                  variant="outlined"
                  required
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  id="share_access_key"
                  name="share_access_key"
                  value={values["share_access_key"]}
                  disabled={isEditing && !isParamEditable("share_access_key")}
                  onChange={(e) => onChange("share_access_key", e.target.value)}
                  label="Share Access Key"
                  fullWidth
                  variant="outlined"
                  required
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  id="share_access_to"
                  name="share_access_to"
                  value={values["share_access_to"]}
                  disabled={isEditing && !isParamEditable("share_access_to")}
                  onChange={(e) => onChange("share_access_to", e.target.value)}
                  label="Share Access To"
                  fullWidth
                  variant="outlined"
                  required
                />
              </Grid>
              <Grid item md={6}>
                <Box display="flex" alignItems="center">
                  <InputLabel style={{ marginRight: "1rem" }}>
                    Access Level
                  </InputLabel>
                  <NativeSelect
                    value={values["share_access_level"]}
                    onChange={(e) =>
                      onChange("share_access_level", e.target.value)
                    }
                    inputProps={{
                      name: "Access Level",
                      id: "share_access_level",
                    }}
                  >
                    <option value="rw">Read/Write</option>
                    <option value="ro">Read Only</option>
                  </NativeSelect>
                </Box>
              </Grid>
            </Grid>
          </>
        )}
      </Box>
    </>
  );
};

export default Storage;
