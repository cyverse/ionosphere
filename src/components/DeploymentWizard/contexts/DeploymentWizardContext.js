import { createContext, useContext, useEffect, useReducer } from "react";

import { useRegions } from "@/hooks";
import deploymentWizardReducer from "./deploymentWizardReducer";
import { useClouds, useWorkspaces } from "@/contexts";
import { useCloudMenuForm } from "@/contexts/cloudMenuForm";

import { setWorkspaceId } from "./actions/actions";

export const DeploymentWizardContext = createContext(null);
export const DeploymentWizardDispatchContext = createContext(null);

/**
 * DeploymentWizardContext is a React Context that provides the current
 * state for the deployment wizard.
 *
 * Deployment wizard state is managed with a React reducer.
 *
 * @param {Object} value (see below for params)
 * @param {JSX.Element} children
 * @returns Deployment wizard state
 */

export function DeploymentWizardProvider({ children, initialValues }) {
  // initialize state from initialValues prop, or use defaults
  const {
    template = null,
    regionId = "",
    workspaceId = null,
    templateCloudTypes = {},
  } = initialValues;

  const initialState = {
    template,
    regionId,
    workspaceId,
    templateCloudTypes,
  };

  const [workspaces] = useWorkspaces();
  const [clouds] = useClouds();
  const { cloudMenu } = useCloudMenuForm();

  // initialize deployment wizard reducer
  const [state, dispatch] = useReducer(deploymentWizardReducer, initialState);

  const [regions, loadingRegions] = useRegions(
    cloudMenu.cloud?.id,
    cloudMenu.credentialId
  );

  useEffect(() => {
    const cloudId = cloudMenu.cloud?.id;
    if (cloudId && clouds) {
      const workspaceId = workspaces.find(
        (w) =>
          w.default_provider_id === cloudId && w.name === "Default Workspace"
      )?.id;
      dispatch(setWorkspaceId(workspaceId));
    }
  }, [cloudMenu.cloud?.id]);

  return (
    <DeploymentWizardContext.Provider
      value={{ ...state, regions, loadingRegions }}
    >
      <DeploymentWizardDispatchContext.Provider value={dispatch}>
        {children}
      </DeploymentWizardDispatchContext.Provider>
    </DeploymentWizardContext.Provider>
  );
}

/**
 * React hook to access Quick Deploy wizard state.
 */
export function useDeploymentWizard() {
  return useContext(DeploymentWizardContext);
}

/**
 * React hook to access dispatch function for updating
 * Quick Deploy wizard state.
 */
export function useDeploymentWizardDispatch() {
  return useContext(DeploymentWizardDispatchContext);
}
