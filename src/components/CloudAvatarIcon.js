import { Avatar } from "@mui/material/";
import SvgCloudIcon from "./icons/cloudIcon";
import OpenstackIcon from "./icons/openstackIcon";
import K8Icon from "./icons/k8Icon";

/**
 * Render Avatar Icon for cloud based on provider type
 * @param {string} type provider type
 * @returns {JSX.Element}
 * @constructor
 */
export default function CloudAvatarIcon({ type }) {
  if (type === "openstack")
    return (
      <Avatar sx={{ background: "transparent" }} variant="square">
        <OpenstackIcon fill={"#f01c44"} />
      </Avatar>
    );
  else if (type === "kubernetes")
    return (
      <Avatar sx={{ background: "transparent" }}>
        <K8Icon />
      </Avatar>
    );
  else
    return (
      <Avatar>
        <SvgCloudIcon />
      </Avatar>
    );
}
