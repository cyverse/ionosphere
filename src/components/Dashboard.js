import { useEffect, useMemo, useState } from "react";
import Image from "next/image";
import {
  Alert,
  AlertTitle,
  Box,
  Container,
  CssBaseline,
  IconButton,
  Snackbar,
  Toolbar,
  Tooltip,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";

import useMediaQuery from "@mui/material/useMediaQuery";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import MenuIcon from "@mui/icons-material/Menu";

import { AppBar, Drawer, DrawerHeader } from "./Common/MiniDrawer";
import NavMenu from "./NavMenu";
import UserMenu from "./UserMenu";
import PageHeader from "./PageHeader";
import { CustomIntercom } from "./CustomIntercom";
import Footer from "./Footer";
import { QuickDeployButton } from "./QuickDeploy/components/QuickDeployAvatar";
import QuickDeployDialog from "./QuickDeploy/components/QuickDeployDialog";

import {
  useAPI,
  useClouds,
  useConfig,
  useError,
  useUser,
  useUserRecents,
} from "../contexts";

const DRAWER_WIDTH = 240;

function Dashboard(props) {
  const theme = useTheme();
  const config = useConfig();
  const api = useAPI();

  const [user] = useUser();
  const [userRecents] = useUserRecents();
  const [clouds] = useClouds();

  const [error, setError] = useError();
  const [showQuickDeploy, setShowQuickDeploy] = useState(false);
  const [quickDeployTemplates, setQuickDeployTemplates] = useState(null);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const selectedCloud = useMemo(() => {
    return clouds.find((c) => c.id === userRecents.cloud) || clouds[0];
  }, [clouds, userRecents.cloud]);

  const isSmallScreen = useMediaQuery((theme) => theme.breakpoints.down("sm"));

  const QUICK_DEPLOY_TEMPLATE_MAP = {
    aws: "aws-single-image",
    openstack: "openstack-single-image",
  };

  const handleDrawerOpen = () => {
    setDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  useEffect(() => {
    if (isSmallScreen) {
      setDrawerOpen(false);
    }
  }, [isSmallScreen]);

  // Fetch templates and map each provider type to the quick deploy template for that type (aws, openstack)
  const fetchTemplates = async () => {
    const templates = await api.templates();
    const quickDeployTemplates = {};

    Object.entries(QUICK_DEPLOY_TEMPLATE_MAP).forEach(([key, value]) => {
      quickDeployTemplates[key] = templates.find((t) => t.name === value);
    });
    setQuickDeployTemplates(quickDeployTemplates);
  };

  useEffect(() => {
    fetchTemplates();
  }, []);

  const handleQuickDeployClose = () => {
    setShowQuickDeploy(false);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        open={drawerOpen}
        sx={{
          backgroundColor: theme.palette.appBar.background,
          borderBottom: "1px solid #e0e0e0",
        }}
      >
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 4,
              ...(drawerOpen && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Box
            sx={{ width: "100%" }}
            display="flex"
            justifyContent={"space-between"}
            alignItems="center"
          >
            <Box marginTop={1}>
              <Image
                src="/images/cacaoLogo.svg"
                width="100px"
                height="50px"
                priority={true}
              />
            </Box>
            {config.NODE_ENV !== "production" && (
              <Typography color="brown">LOCAL DEVELOPMENT</Typography>
            )}
            {config.CACAO_DEV_MODE === "true" && <div>DEVELOPMENT VERSION</div>}
            {config.INTERCOM_APP_ID && (
              <CustomIntercom
                appId={config.INTERCOM_APP_ID}
                companyId={config.INTERCOM_COMPANY_ID}
              />
            )}
            <Box>
              <Box
                display={"flex"}
                alignContent={"center"}
                justifyContent={"center"}
                alignItems={"center"}
              >
                {quickDeployTemplates && (
                  <Tooltip title="Quick Deploy">
                    <Box>
                      <QuickDeployButton
                        onClick={() => setShowQuickDeploy(true)}
                      />
                    </Box>
                  </Tooltip>
                )}
                <Box>
                  <UserMenu user={user} />
                </Box>
              </Box>
            </Box>
          </Box>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        open={drawerOpen}
        PaperProps={{
          sx: {
            backgroundColor: theme.palette.sidebar.background,
          },
        }}
      >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <NavMenu showStaff={props.showStaff} open={drawerOpen} />
      </Drawer>

      {/* page contents */}
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          width: { sm: `calc(100% - ${DRAWER_WIDTH}px)` },
        }}
      >
        <Toolbar />
        <Container maxWidth="xl">
          <PageHeader
            title={props.title}
            breadcrumbs={props.breadcrumbs}
            parts={props.parts}
            back={props.back}
            actions={props.actions}
          />
          {props.children}
        </Container>
        <Box
          component="footer"
          sx={{
            padding: "4rem",
            marginTop: "auto", // Pushes the footer to the bottom
          }}
        >
          <Footer />
        </Box>
      </Box>

      {/* error display */}
      {error && (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          open={!!error}
        >
          <Alert
            elevation={6}
            variant="filled"
            severity="error"
            onClose={() => setError(null)}
          >
            <AlertTitle>Oops! An error occurred:</AlertTitle>
            {error}
          </Alert>
        </Snackbar>
      )}

      {/* quick deploy dialog */}
      {showQuickDeploy && (
        <QuickDeployDialog
          open={true}
          handleClose={handleQuickDeployClose}
          templates={quickDeployTemplates}
          cloud={selectedCloud}
        />
      )}
    </Box>
  );
}

export default Dashboard;
