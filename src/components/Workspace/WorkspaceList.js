import { useState } from "react";
import getConfig from "next/config";
import { Alert, Box } from "@mui/material";

import { useAPI, useError, useWorkspaces } from "@/contexts";

import { useCloudMenu } from "@/contexts";
import { useDeployments } from "@/contexts/deployments";

import Workspace from "@/components/Workspace/Workspace";
import EditDeploymentDialog from "@/components/DeploymentWizard/EditDeploymentDialog";

const WorkspaceList = () => {
  const api = useAPI();
  const config = getConfig().publicRuntimeConfig;

  // Get data from contexts
  const {
    selectedCloud,
    selectedProject,
    selectedCredential,
    credentialOptions,
  } = useCloudMenu();

  const { setBusy, deployments, templates, deleteDeployment } =
    useDeployments();

  const [workspaces] = useWorkspaces();
  const [_, setError] = useError();

  const [showEditDeploymentDialog, setShowEditDeploymentDialog] = useState();

  // deployment selected for editing
  const [deployment, setDeployment] = useState();

  const handleError = (error) => {
    console.error(error);
    setBusy(false);
    setError(api.errorMessage(error));
  };

  /**
   * Handles editing of deployment and run. Invoked from EditDeploymentDialog.
   *
   * @param {string} deploymentId: ID of deployment to be edited
   * @param {array of {key, value} pairs} deploymentParams: params for editing existing deployment
   * @param {array of {key, value} pairs} runParams: params for creating new deployment run
   */
  const handleEditDeployment = async (
    deploymentId,
    deploymentParams,
    runParams
  ) => {
    console.log("Deployment Params:", deploymentParams);
    console.log("Run Params:", runParams);

    if (!(config.SIMULATE_DEPLOYMENT_SUBMIT === "true")) {
      setBusy(true);

      try {
        if (deploymentParams.name) {
          const res = await api.updateDeployment(
            deploymentId,
            deploymentParams
          );
        }

        // create run
        if (runParams) {
          const res2 = await api.createDeploymentRun(deploymentId, {
            deployment_id: deploymentId,
            parameters: runParams,
          });
        }

        setTimeout(async () => {
          setShowEditDeploymentDialog(false);
          setBusy(false);
        }, 500);
      } catch (error) {
        handleError(error);
      }
    }
  };

  /**
   * Handles deletion of deployments.
   *
   * @param {string} id ID of deployment to delete.
   * */

  /**
   * Opens EditDeploymentDialog w/ selected deployment.
   *
   * @param {string} id ID of deployment to edit.
   */
  const openEditDeploymentDialog = async (id) => {
    const deployment = deployments.find((d) => d.id === id);

    setDeployment(deployment);
    setShowEditDeploymentDialog(id);
  };

  /**
   * Filters deployments by workspace ID and currently selected project/credential.
   * If no project is selected, all deployments for the workspace are returned.
   * @param {string} workspaceId
   */
  const filterDeployments = (workspaceId, credentialId) => {
    // Filter deployments by workspace id
    const workspaceDeployments = deployments.filter(
      (d) => d.workspace_id === workspaceId
    );

    const projectCredentialIds = new Set(
      credentialOptions.map((cred) => cred.id)
    );

    // Check if projects are relevant for the selected cloud
    if (selectedCloud.hasProjects) {
      // Return deployments matching the selected project, if any
      return selectedProject?.name
        ? workspaceDeployments.filter((d) => {
            return d.cloud_credentials.some((credId) =>
              projectCredentialIds.has(credId)
            );
          })
        : workspaceDeployments; // No project selected, return all for the workspace
    } else {
      // If no projects, filter by credential id if provided
      return credentialId
        ? workspaceDeployments.filter((d) =>
            d.cloud_credentials.includes(credentialId)
          )
        : workspaceDeployments; // No credential id specified, return all for the workspace
    }
  };

  return (
    <>
      {workspaces?.length > 0 ? (
        workspaces
          .filter((w) => w.default_provider_id === selectedCloud?.id)
          .map((workspace, index) => (
            <Box key={index} mb={4}>
              <Workspace
                workspace={workspace}
                selectedProjectName={selectedProject?.name}
                selectedCloud={selectedCloud}
                deployments={filterDeployments(
                  workspace.id,
                  selectedCredential?.id
                )}
                templates={templates}
                showAllDeployments={workspaces.length === 1}
                handleDeleteDeployment={deleteDeployment}
                openEditDeploymentDialog={openEditDeploymentDialog}
              />
            </Box>
          ))
      ) : (
        <Box mt={4}>
          <Alert severity="error">
            You do not have any workspaces. Select "Add Workspace" to create
            one.
          </Alert>
        </Box>
      )}

      {showEditDeploymentDialog && (
        <EditDeploymentDialog
          open={true}
          deployment={deployment}
          cloudId={selectedCloud?.id}
          template={templates?.find((t) => t.id === deployment.template_id)}
          handleSubmit={handleEditDeployment}
          handleClose={() =>
            setDeployment() || setShowEditDeploymentDialog(false)
          }
        />
      )}
    </>
  );
};

export default WorkspaceList;
