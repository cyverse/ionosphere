import { useState } from "react";
import { useRouter } from "next/router";

import {
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  Grid,
  Link,
  Paper,
  Radio,
  RadioGroup,
  Skeleton,
  Stack,
  Typography,
} from "@mui/material";

import { Add as AddIcon, Refresh as RefreshIcon } from "@mui/icons-material";

import { useDeployments } from "@/contexts/deployments";

import { sortByDateDesc, sortByName } from "@/utils";
import { ConfirmationDialog } from "@/components";
import DeploymentSummary from "@/components/Deployment/DeploymentSummary";
import ResourceAlert from "@/components/Resources/ResourceAlert";
import RefreshButton from "../Common/Button/RefreshButton";
import { useCloudMenu } from "@/contexts";

const MAX_SUMMARY_DEPLOYMENTS = 10;

const sortDeployments = (deployments, sort) => {
  const order = {
    active: 0,
    "pre-flight": 1,
    errored: 2,
    "": 3,
  };

  if (sort === "name") return deployments.sort(sortByName);
  if (sort === "status")
    return deployments.sort(
      (a, b) => order[a.current_run.status] - order[b.current_run.status]
    );

  return deployments.sort(sortByDateDesc);
};

/**
 * Currently one workspace per cloud is supported.
 *
 * In a Workspace, users can:
 * - View all deployments for the selected cloud, filtered by project.
 * - Create new deployments for the selected cloud & project.
 *
 * NOTE: Current implementation of Workspace component may seem odd
 * because it was originally designed for users to have multiple workspaces
 * per cloud, but this functionality has been removed for now.
 *
 * Workspace structure was left in place for if/when we return to allowing
 * multiple workspaces/cloud.  If we don't support multiple workspaces,
 * this should be refactored.
 */
const Workspace = ({
  workspace,
  deployments,
  templates,
  showAllDeployments,
  handleDeleteDeployment,
  openEditDeploymentDialog,
  selectedProjectName,
  selectedCloud,
}) => {
  const [sort, setSort] = useState();
  const router = useRouter();
  const { fetchDeployments } = useDeployments();
  const [deploymentIdToDelete, setDeploymentIdToDelete] = useState();
  const { loadingCredentials, loadingProjects } = useCloudMenu();

  const numDeploymentsToShow = showAllDeployments
    ? deployments.length
    : MAX_SUMMARY_DEPLOYMENTS;

  return (
    <Paper variant="outlined" sx={{ padding: "2em" }}>
      <Grid container justifyContent="space-between" alignItems={"center"}>
        <Grid item>
          <Typography component="h1" variant="h5">
            {selectedCloud.name}
            {selectedCloud.hasProjects && selectedProjectName ? (
              <> / {selectedProjectName} </>
            ) : null}
          </Typography>
        </Grid>

        <Grid item>
          <Button
            variant="contained"
            color="primary"
            style={{ marginLeft: "2em" }}
            startIcon={<AddIcon />}
            onClick={() => router.push("/templates")}
            size="small"
          >
            New Deployment
          </Button>
        </Grid>
      </Grid>
      <Box marginY={2}>
        <ResourceAlert cloudName={selectedCloud?.name}></ResourceAlert>
      </Box>

      {/* {description && (
        <Box mt={3} mb={2.5}>
          <Typography variant="body2">{description}</Typography>
        </Box>
      )} */}

      <Box pt={2}>
        <Grid
          container
          justifyContent="space-between"
          alignItems="center"
          style={{ paddingBottom: "0.5em" }}
        >
          <Grid item>
            <Stack direction="row" alignItems="center" spacing={1}>
              <Typography variant="h6">
                Deployments
                {deployments.length > 0 && (
                  <small> ({deployments.length})</small>
                )}
              </Typography>
              <Box style={{ marginTop: "-.5em" }}>
                <RefreshButton onClick={fetchDeployments} />
              </Box>
            </Stack>
          </Grid>

          {sortDeployments(deployments, sort)?.length > 1 && (
            <Grid item>
              <Box
                display="flex"
                flexWrap="wrap"
                alignSelf="flex-end"
                alignItems="center"
              >
                <Typography color="textSecondary">Sort by:</Typography>
                <FormControl
                  component="fieldset"
                  style={{ marginLeft: "1.5em" }}
                >
                  <RadioGroup
                    row
                    aria-label="sort by"
                    name="sort by"
                    defaultValue="newest"
                    onChange={(event) => setSort(event.target.value)}
                  >
                    <FormControlLabel
                      value="name"
                      control={<Radio color="primary" />}
                      label="Name"
                    />
                    <FormControlLabel
                      value="newest"
                      control={<Radio color="primary" />}
                      label="Newest"
                    />
                    <FormControlLabel
                      value="status"
                      control={<Radio color="primary" />}
                      label="Status"
                    />
                  </RadioGroup>
                </FormControl>
              </Box>
            </Grid>
          )}
        </Grid>
        <Divider />

        {loadingProjects || loadingCredentials ? (
          <LoadingDeployments />
        ) : (
          <>
            {sortDeployments(deployments, sort)?.length > 0 ? (
              <div>
                {sortDeployments(deployments, sort)
                  ?.slice(0, numDeploymentsToShow)
                  .map((deployment, index) => (
                    <Link
                      key={index}
                      href={`/deployments/${deployment.id}`}
                      underline="none"
                    >
                      <DeploymentSummary
                        deployment={deployment}
                        template={templates.find(
                          (t) => t.id === deployment.template_id
                        )}
                        handleDelete={() =>
                          setDeploymentIdToDelete(deployment.id)
                        }
                        handleEdit={() =>
                          openEditDeploymentDialog(deployment.id)
                        }
                      />
                      <Divider />
                    </Link>
                  ))}
                <br />
                {!showAllDeployments &&
                  deployments.length > MAX_SUMMARY_DEPLOYMENTS && (
                    <Box display="flex" justifyContent="center">
                      <Button
                        size="small"
                        color="primary"
                        href={`/deployments?workspace=${workspace.id}`}
                      >
                        View All
                      </Button>
                    </Box>
                  )}
              </div>
            ) : (
              <Box mt={2}>
                <Typography color="textSecondary" variant="body2">
                  This project does not have any deployments yet. Select "Add
                  Deployment" to create one.{" "}
                </Typography>
              </Box>
            )}
          </>
        )}
      </Box>

      <ConfirmationDialog
        open={!!deploymentIdToDelete}
        title="Delete deployment"
        handleClose={() => setDeploymentIdToDelete()}
        handleSubmit={() => handleDeleteDeployment(deploymentIdToDelete)}
      />
    </Paper>
  );
};

const LoadingDeployments = ({ count }) => (
  <Stack mt={2} spacing={2}>
    {Array.from({ length: count || 3 }).map((_, index) => (
      <Box key={index}>
        <Stack direction="row" spacing={2} paddingLeft={2} paddingBottom={2}>
          <Skeleton
            variant="circular"
            width={30}
            height={30}
            animation="pulse"
          />
          <Stack sx={{ width: "100%" }}>
            <Skeleton variant="text" sx={{ fontSize: "1.5rem" }} width="100%" />
            <Skeleton variant="text" sx={{ fontSize: "1.5rem" }} width="100%" />
          </Stack>
        </Stack>
        <Divider />
      </Box>
    ))}
  </Stack>
);

export default Workspace;
