import React from "react";
import { Grid, Box, Button, Typography } from "@mui/material";
import Image from "next/image";

export default function WelcomeBanner() {
  return (
    <div>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        alignContent="center"
        spacing={3}
      >
        <Grid item xs={12} sm={12} md={7} lg={6}>
          <Box display="flex" justifyContent="center">
            <Image src="/images/cacaoLogo.svg" width={350} height={150} />
          </Box>
          <Typography
            component="h2"
            variant="h5"
            color="primary"
            sx={{ textAlign: "center", marginTop: "1rem" }}
          >
            Hi there, this is a Beta release.
          </Typography>
          <br />
          <Box
            display="flex"
            justifyContent="space-evenly"
            flexDirection="row"
            flexWrap="wrap"
          >
            <Box display="flex">
              <Button
                variant="contained"
                color="primary"
                href="https://forms.gle/vV8FtCiAqN4VaZua9"
                target="_blank"
              >
                Submit Feedback
              </Button>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
