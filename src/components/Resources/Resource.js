import OpenStackResource from "./OpenStackResource";
import AWSResource from "./AWSResource";

const RESOURCE_TYPES = ["openstack_instance", "aws_instance"];

/**
 *
 * @param {Object} resource
 * @param {string} resource.type
 * @returns {boolean}
 */
export function needToRenderResource(resource) {
  return RESOURCE_TYPES.includes(resource.type);
}

/**
 *
 * @param {string} props.type
 * @returns {JSX.Element}
 * @constructor
 */
export function Resource(props) {
  if (props.type === "openstack_instance") {
    return <OpenStackResource {...props} />;
  } else if (props.type === "aws_instance") {
    return <AWSResource {...props} />;
  }
  return <></>;
}
