import { Typography } from "@mui/material";

/**
 * Resource Alert for Deployments page.
 *
 * @param {string} cloudName
 * @returns JSX.Element
 */

const ResourceAlert = () => {
  return (
    <Typography variant="caption">
      CACAO does not display all resources, only the resources managed by CACAO.
    </Typography>
  );
};

export default ResourceAlert;
