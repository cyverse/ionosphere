import React from "react";
import { CardHeader, Avatar, CardContent, Typography } from "@mui/material";
import { mdiOpenInNew } from "@mdi/js";
import { mdiSchool } from "@mdi/js";
import Icon from "@mdi/react";
import HoverCard from "../Common/HoverCard";

export default function LearningCard({ title, description }) {
  return (
    <HoverCard style={{ height: "100%" }}>
      <CardHeader
        avatar={
          <Avatar sx={{ width: "2rem", height: "2rem" }}>
            <Icon path={mdiSchool} size={"20px"} />
          </Avatar>
        }
        title={<Typography variant="body1">{title}</Typography>}
        action={<Icon path={mdiOpenInNew} size={1} />}
      />
      <CardContent sx={{ minHeight: "5rem" }}>{description}</CardContent>
    </HoverCard>
  );
}
