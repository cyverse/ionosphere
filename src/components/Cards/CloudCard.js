import React from "react";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Link,
  Typography,
} from "@mui/material/";
import CloudAvatarIcon from "../CloudAvatarIcon";

/**
 *
 * @param {string} id
 * @param {string} name
 * @param {string} description
 * @param {string} type
 * @returns {JSX.Element}
 * @constructor
 */
export default function CloudCard({ id, name, description, type }) {
  return (
    <Card>
      <Link href={`/administrative/clouds/${id}`} underline="none">
        <CardHeader
          avatar={<CloudAvatarIcon type={type} />}
          title={name}
          subheader={type}
        />
      </Link>
      <CardContent sx={{ height: "3em" }}>
        <Typography
          variant="body2"
          color="textPrimary"
          component="p"
          sx={{
            display: "-webkit-box",
            WebkitLineClamp: 2,
            WebkitBoxOrient: "vertical",
            overflow: "hidden",
            minHeight: "2.5em",
          }}
        >
          {description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing></CardActions>
    </Card>
  );
}
