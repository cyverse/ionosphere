import { Search } from "@mui/icons-material";
import { TextField, InputAdornment } from "@mui/material";
import { SET_SEARCH } from "./TemplatesContext";
import { useTemplates, useTemplatesDispatch } from "./TemplatesContext";

export default function TemplateSearch() {
  const templateContext = useTemplates();
  const templatesDispatch = useTemplatesDispatch();
  return (
    <TextField
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <Search />
          </InputAdornment>
        ),
      }}
      variant="outlined"
      fullWidth
      label="Search Templates"
      value={templateContext.search}
      onChange={(e) =>
        templatesDispatch({ type: SET_SEARCH, payload: e.target.value })
      }
      placeholder=" Try 'docker', 'vms', 'llm', or 'jupyter' or clouds like 'aws' and 'openstack'"
    />
  );
}
