import React from "react";
import {
  Grid,
  Alert,
  AlertTitle,
  Typography,
  Button,
  Box,
} from "@mui/material";
import { isPrerequisiteTemplate } from "@/utils";
import TemplateCard from "./TemplateCard";
import { useTemplates, useTemplatesDispatch } from "./TemplatesContext";
import { SET_SEARCH } from "./TemplatesContext";

export default function TemplateList({ setShowTemplateWizard }) {
  const templateContext = useTemplates();
  const { searchResults } = templateContext;
  const templatesDispatch = useTemplatesDispatch();

  console.log(templateContext);
  return (
    <>
      <Box>
        <Grid container spacing={3} justify="flex-end">
          {searchResults.length === 0 ? (
            <Grid item xs={12}>
              <Box>
                <Alert severity="info">
                  <AlertTitle>No matching templates found</AlertTitle>
                  <Typography variant="body1">
                    {" "}
                    Try different keywords or{" "}
                    <Button
                      onClick={() =>
                        templatesDispatch({ type: SET_SEARCH, payload: "" })
                      }
                      color="primary"
                      size="small"
                      variant="text"
                    >
                      view all templates
                    </Button>
                  </Typography>
                </Alert>
              </Box>
            </Grid>
          ) : (
            searchResults
              .filter((t) => isPrerequisiteTemplate(t))
              .map((template, index) => (
                <Grid
                  key={index}
                  item
                  xs={12}
                  sm={12}
                  md={6}
                  lg={4}
                  display="flex"
                >
                  <TemplateCard
                    template={template}
                    search={SET_SEARCH}
                    launchWizard={() => setShowTemplateWizard(template.id)}
                  />
                </Grid>
              ))
          )}
        </Grid>
      </Box>
    </>
  );
}
