import {
  Code as CodeIcon,
  Info,
  MoreVert as MoreVertIcon,
  RocketLaunch as RocketLaunchIcon,
  Widgets as WidgetsIcon,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  CardContent,
  Chip,
  Grid,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Stack,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Date } from "../Common/DateTime";
import { stringToColor } from "@/utils";
import HoverCard from "../Common/HoverCard";
import { useTemplates } from "./TemplatesContext";

/**
 * Splits a given string by a search term and filters out any empty strings from the result.
 * The search is case-insensitive and the search term is included in the split results.
 *
 * @param {string} str - The string to split.
 * @param {string} search - The search term used to split the string. Treated as a regular expression.
 * @returns {string[]} An array of substrings split by the search term, excluding any empty strings.
 *
 * @example
 * // returns ['Th', 'is', ' ', 'is', ' a test string']
 * splitAndFilter('This is a test string', 'is');
 */
function splitAndFilter(str, search) {
  return str.split(new RegExp(`(${search})`, "gi")).filter(Boolean);
}

/**
 * Card for displaying information about a CACAO template.
 */
export default function TemplateCard({ template, launchWizard }) {
  const templateContext = useTemplates();
  const { search } = templateContext;

  const name = splitAndFilter(template.name, search);
  const description = splitAndFilter(template.metadata.description, search);
  const author = splitAndFilter(template.metadata.author, search);
  const type = splitAndFilter(template.metadata.template_type, search);
  // const purpose = splitAndFilter(template.metadata.purpose, search);
  // const owner = splitAndFilter(template.owner, search);

  return (
    <a
      href={`/templates/${template.id}`}
      style={{
        textDecoration: "none",
        color: "inherit",
        display: "flex",
        alignItems: "center",
        height: "100%",
        width: "100%",
      }}
      aria-label={`View details for template ${template.name}`}
    >
      <HoverCard
        style={{
          display: "flex",
          flexDirection: "row",
          height: "100%",
          minWidth: "100%",
          alignContent: "center",
          justifyContent: "center",
          alignItems: "flex-start",
        }}
        tabIndex={0} // Make the card focusable
        onKeyDown={(e) => {
          if (e.key === "Enter" || e.key === " ") {
            window.location.href = `/templates/${template.id}`;
          }
        }} // Allow keyboard navigation
      >
        <CardContent style={{ flexGrow: 1 }}>
          <Stack
            direction="row"
            alignItems={"center"}
            justifyContent={"space-between"}
            spacing={1}
            marginBottom={1}
          >
            <Stack
              direction="row"
              alignItems={"center"}
              justifyContent={"flex-start"}
              spacing={1}
            >
              <Box>
                <Avatar
                  sx={{
                    bgcolor: stringToColor(template.name),
                    width: "1.4rem",
                    height: "1.4rem",
                  }}
                >
                  <WidgetsIcon style={{ width: "1rem" }} fontSize="inherit" />
                </Avatar>
              </Box>
              <Box>
                <Typography
                  variant="body1"
                  style={{ fontSize: "18px", fontWeight: 500 }}
                  component="h2"
                >
                  <HighlightedText text={name} search={search} />
                </Typography>
              </Box>
            </Stack>
          </Stack>
          <Stack direction="row" useFlexGap flexWrap="wrap">
            <Box>
              <Typography
                variant="body1"
                style={{ fontSize: "14px", fontWeight: 500 }}
                component="p"
              >
                <HighlightedText text={description} search={search} />
              </Typography>
            </Box>
          </Stack>
          <Stack
            direction="row"
            alignItems={"center"}
            justifyContent={"flex-start"}
            spacing={1}
          >
            <Box>
              <Grid
                container
                spacing={1}
                direction="row"
                alignItems={"flex-start"}
                marginY={0.5}
              >
                {/* <Grid item>
                <Chip
                  label={<HighlightedText text={purpose} search={search} />}
                  size="small"
                />
              </Grid> */}
                <Grid item>
                  <Chip
                    label={<HighlightedText text={type} search={search} />}
                    size="small"
                  />
                </Grid>
                <Grid item>
                  <Chip
                    label={template.public ? "public" : "private"}
                    size="small"
                  />
                </Grid>
              </Grid>
            </Box>
          </Stack>
          <Stack
            direction="row"
            alignItems={"center"}
            justifyContent={"space-between"}
            alignContent={"center"}
            spacing={1}
          >
            <Box>
              <Typography variant="caption">
                Author: <HighlightedText text={author} search={search} />
              </Typography>
            </Box>
            {/* <Box>
              <Typography variant="caption">
                  Imported By: <HighlightedText text={owner} search={search} />
              </Typography>
            </Box> */}
            {/* </Stack> */}
            <Box>
              <Typography variant="caption">
                Updated: <Date datetime={template.updated_at} />
              </Typography>
            </Box>
          </Stack>
        </CardContent>
      </HoverCard>
    </a>
  );
}

// Highlight and bold searched terms in the template card
function HighlightedText({ text, search }) {
  return text.map((part, index) =>
    part.toLowerCase() === search.toLowerCase() && search.length > 1 ? (
      <b
        key={index}
        style={{
          backgroundColor: "#fff38a",
          padding: "2px",
          borderRadius: "5px",
        }}
      >
        {part}
      </b>
    ) : (
      <span key={index}>{part}</span>
    )
  );
}
