import React from "react";
import {
  Radio,
  Box,
  Grid,
  Button,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Typography,
} from "@mui/material";

import {
  useTemplates,
  useTemplatesDispatch,
  SET_CLOUD_FILTER,
  TemplatesContext,
} from "./TemplatesContext";

export default function SortingOptions() {
  const templateDispatch = useTemplatesDispatch();

  return (
    <Grid
      spacing={1}
      container
      alignContent={"flex-start"}
      alignItems={"flex-start"}
      justifyContent={"flex-start"}
      direction={"column"}
    >
      <Grid item xs={6} md={12} sm={3}>
        <CloudSorting />
      </Grid>
      {/* <Grid item xs={6} md={12} sm={3}>
        <PurposeSorting />
      </Grid> */}
      <Grid item xs={6} md={12} sm={3}>
        <Box mt={1}>
          <Button
            onClick={() =>
              templateDispatch({ type: SET_CLOUD_FILTER, payload: null })
            }
            size="small"
          >
            Clear Filters
          </Button>
        </Box>
      </Grid>
    </Grid>
  );
}

// Select Sytling
function SortSelect(props) {
  return <Radio size="small" {...props} />;
}

// Label Sytling
function SortLabel({ label }) {
  return <Typography variant="body2">{label}</Typography>;
}

function SortTitle({ title }) {
  return (
    <Typography variant="body1" style={{ fontWeight: "500" }}>
      {title}
    </Typography>
  );
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const RadioButton = ({ value, label }) => (
  <FormControlLabel
    value={value}
    control={<SortSelect />}
    label={<SortLabel label={label} />}
  />
);

// const PurposeSorting = () => {
//   const templateContext = useTemplates();
//   const handleChange = (event) => {
//     setSelectedFilter(event.target.value);
//   };

//   // const radioButtons = templates
//   //   ? templates.map((template) => ({
//   //       value: template.metadata.purpose,
//   //       label: capitalizeFirstLetter(template.metadata.purpose),
//   //     }))
//   //   : [];

//   return (
//     <FormControl component="fieldset">
//       <SortTitle title="Purpose" />
//       {/* <RadioGroup
//         aria-labelledby="sort-templates-by-purpose"
//         value={selectedFilter}
//         name="templates-by-purpose"
//         onChange={handleChange}
//       >
//         {radioButtons.map((button) => (
//           <RadioButton
//             key={button.value}
//             value={button.value}
//             label={button.label}
//           />
//         ))}
//       </RadioGroup> */}
//     </FormControl>
//   );
// };

const CloudSorting = () => {
  const templateContext = useTemplates();
  const { templates, cloudFilter } = templateContext;
  const templateDispatch = useTemplatesDispatch();

  // console.log(templateContext);

  const handleChange = (event) => {
    templateDispatch({ type: SET_CLOUD_FILTER, payload: event.target.value });
  };

  const uniqueTemplateTypes = [
    ...new Set(templates.map((template) => template.metadata.template_type)),
  ];

  const radioButtons = uniqueTemplateTypes.map((templateType) => ({
    value: templateType,
    label: capitalizeFirstLetter(templateType),
  }));

  // Function to format the cloud labels to human readable
  const formatLabel = (label) => {
    return label
      .split("_")
      .map((word) => {
        if (word.toLowerCase() === "aws") {
          return "AWS"; // Special case for AWS
        }
        return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
      })
      .join(" ");
  };

  return (
    <FormControl>
      <SortTitle title="Cloud" />
      <RadioGroup
        aria-labelledby="sort-templates-by-cloud"
        value={cloudFilter}
        name="templates-by-cloud"
        onChange={handleChange}
      >
        {radioButtons.map((button) => (
          <RadioButton
            key={button.value}
            value={button.value}
            label={formatLabel(button.label)}
          />
        ))}
      </RadioGroup>
    </FormControl>
  );
};
