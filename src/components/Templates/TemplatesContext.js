import { createContext, useContext, useEffect, useReducer } from "react";

// Create context for templates page
export const TemplatesContext = createContext(null);
export const TemplatesDispatchContext = createContext(null);

// Provider for TemplatesContext
export function TemplatesProvider({ children, templates }) {
  const initialState = {
    search: "",
    cloudFilter: null,
    searchResults: templates,
    templates: templates,
  };

  const [state, dispatch] = useReducer(templatesReducer, initialState);

  useEffect(() => {
    let filteredTemplates = state.templates;

    if (state.cloudFilter) {
      filteredTemplates = filteredTemplates.filter((template) =>
        filterTemplateType(template, state.cloudFilter)
      );
    }

    const rankedTemplates = filteredTemplates
      .map((template) => ({
        ...template,
        score: scoreTemplate(template, state.search),
      }))
      .filter((template) => template.score > 0)
      .sort((a, b) => b.score - a.score);

    dispatch({ type: SET_SEARCH_RESULTS, payload: rankedTemplates });
  }, [state.templates, state.search, state.cloudFilter]);

  return (
    <TemplatesContext.Provider value={state}>
      <TemplatesDispatchContext.Provider value={dispatch}>
        {children}
      </TemplatesDispatchContext.Provider>
    </TemplatesContext.Provider>
  );
}

// React hook to access Template context state
export function useTemplates() {
  return useContext(TemplatesContext);
}

// React hook to access dispatch function for updating Template context state
export function useTemplatesDispatch() {
  return useContext(TemplatesDispatchContext);
}

export const SET_SEARCH = "SET_SEARCH";
export const SET_CLOUD_FILTER = "SET_CLOUD_FILTER";
export const SET_SEARCH_RESULTS = "SET_SEARCH_RESULTS";
export const SET_TEMPLATES = "SET_TEMPLATES";

function templatesReducer(state, action) {
  switch (action.type) {
    case SET_SEARCH:
      return { ...state, search: action.payload };
    case SET_CLOUD_FILTER:
      return { ...state, cloudFilter: action.payload };
    case SET_SEARCH_RESULTS:
      return { ...state, searchResults: action.payload };
    case SET_TEMPLATES:
      return { ...state, templates: action.payload };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

function filterTemplateType(template, type) {
  return template.metadata.template_type === type;
}

const scoreTemplate = (template, search) => {
  let score = 0;
  if (!template) return score;
  const searchLower = search.toLowerCase();
  if (searchLower.length < 0) {
    return score;
  }
  if (template.name && template.name.toLowerCase().includes(searchLower))
    score += 6;
  if (
    template.metadata.description &&
    template.metadata.description.toLowerCase().includes(searchLower)
  )
    score += 5;
  if (
    template.metadata.author &&
    template.metadata.author.toLowerCase().includes(searchLower)
  )
    score += 4;
  if (template.owner && template.owner.toLowerCase().includes(searchLower))
    score += 3;
  if (
    template.metadata.purpose &&
    template.metadata.purpose.toLowerCase().includes(searchLower)
  )
    score += 2;
  if (
    template.metadata.template_type &&
    template.metadata.template_type.toLowerCase().includes(searchLower)
  )
    score += 1;
  return score;
};
