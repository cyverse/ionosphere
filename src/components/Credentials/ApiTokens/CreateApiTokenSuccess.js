/**
 * CreateApiTokenSuccess Component
 * -------------------------------
 *
 * This component is used to display a success message after a new API token
 * has been created.
 *
 * It displays the newly created token and a copy to clipboard button.
 */

import {
  Alert,
  Box,
  Button,
  DialogActions,
  DialogContent,
  Divider,
  Stack,
  Typography,
} from "@mui/material";

import { red } from "@mui/material/colors";
import CopyToClipboardButton from "@/components/Common/Button/CopyToClipboardButton";

export default function CreateApiTokenSuccess({ token, handleClose }) {
  return (
    <>
      <DialogContent>
        <Stack spacing={4} marginX={2}>
          <Alert severity="success">API token created.</Alert>

          <Stack spacing={2}>
            <Typography color={red[600]} sx={{ fontWeight: "500" }}>
              Copy your token now and put it somewhere safe. Once you close the
              dialog you won't be able to see it again.
            </Typography>

            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                bgcolor: "grey.100",
                color: "grey.800",
                border: "1px solid",
                borderColor: "grey.300",
                borderRadius: 2,
                fontSize: "0.875rem",
                fontWeight: "700",
              }}
            >
              {/* Token Box */}
              <Box
                flexGrow={1}
                flexShrink={1}
                flexBasis="0%"
                sx={{ wordWrap: "break-word", overflow: "hidden" }}
                margin={2}
              >
                <Typography variant="body2" noWrap={false}>
                  {token}
                </Typography>
              </Box>

              {/* Divider */}
              <Divider orientation="vertical" flexItem />

              {/* Copy Button */}
              <Box margin={1}>
                <CopyToClipboardButton
                  text={token}
                  sx={{ width: "2em", height: "2em" }}
                  fontSize="1.25em"
                />
              </Box>
            </Box>
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose}>
          Close
        </Button>
      </DialogActions>
    </>
  );
}
