/**
 * CreateApiTokenForm Component
 * ------------------------------
 *
 * This component is used to display a Formik form for creating a new API token.
 * The form uses yup for validation and dayjs for date manipulation.
 */

import {
  DialogActions,
  DialogContent,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";

import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

import { useEffect, useRef, useState } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";

import { useUser, useAPI, useError } from "@/contexts";
import { capitalizeFirstLetter } from "@/utils";
import CreateApiTokenSuccess from "./CreateApiTokenSuccess";

dayjs.extend(utc);
const now = dayjs().startOf("day");
const oneYearFromNow = now.add(1, "year");

export default function CreateApiTokenForm({ tokenTypes, handleClose }) {
  const api = useAPI();
  const [user] = useUser();

  const [loading, setLoading] = useState(false);
  const [_, setError] = useError();
  const [token, setToken] = useState();

  // token type defaults to personal if available, otherwise first in list
  const defaultTokenType =
    tokenTypes.find((type) => type === "personal") || tokenTypes[0];

  // exclude `delegated` from tokenTypes in UI
  const displayTokenTypes = tokenTypes.filter((type) => type !== "delegated");

  // yup validation schema
  const validationSchema = yup.object({
    type: yup.string().oneOf(displayTokenTypes),
    name: yup.string().required("Please provide a name for your token."),
    description: yup.string(),
    start: yup
      .date()
      .required("Start date is required")
      .test(
        "is-now-or-future",
        "Start date must be today or in the future.",
        (value) => {
          const today = dayjs().startOf("day");
          const startDate = dayjs(value).startOf("day");
          return startDate.isSame(today) || startDate.isAfter(today);
        }
      ),
    expiration: yup
      .date()
      .required("Expiration date is required")
      .when("start", (start, schema) => {
        return schema.test({
          test: (expiration) => {
            // Check if start date is defined and valid
            if (!start || !expiration) return false;

            const oneYearFromStart = dayjs(start).add(1, "year");

            // Check if expiration is within one year from start
            return (
              dayjs(expiration).isBefore(oneYearFromStart) ||
              dayjs(expiration).isSame(oneYearFromStart)
            );
          },
          message:
            "Expiration date must be within one year from the start date.",
        });
      }),
    scopes: yup.string().nullable(),
    publicToken: yup.bool(false),
    delegatedContext: yup.string().nullable(),
  });

  // Formik submit handler
  const handleSubmit = (values) => {
    const tokenParams = setupParams(values);
    createAPIToken(tokenParams);
  };

  const formik = useFormik({
    initialValues: {
      type: defaultTokenType,
      name: "",
      description: "",
      start: now,
      expiration: oneYearFromNow,
      // below fields are not displayed in UI
      owner: user.username,
      scopes: "",
      delegatedContext: "",
      publicToken: false,
    },
    onSubmit: handleSubmit,
    enableReinitialize: true,
    validationSchema: validationSchema,
  });

  /**
   * Makes API call to create token.
   */
  const createAPIToken = async (tokenParams) => {
    setLoading(true);
    try {
      const res = await api.createToken(tokenParams);
      setToken(res.id);
    } catch (error) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  /**
   * Prepare token params for API call from formik values.
   * @param {Object} formikValues
   * @returns
   */
  const setupParams = (formikValues) => {
    let tokenParams = {
      owner: formikValues.owner,
      name: formikValues.name,
      scopes: formikValues.scopes,
      description: formikValues.description,
      publicToken: formikValues.publicToken,
      expiration: formikValues.expiration.endOf("day").utc().format(),
      type: formikValues.type,
      start: formikValues.start.utc().format(),
      delegatedContext: formikValues.delegatedContext,
    };
    return tokenParams;
  };

  // Focus name field on mount
  const focusFieldRef = useRef(null);
  useEffect(() => {
    if (focusFieldRef.current) {
      focusFieldRef.current.focus();
    }
  }, []);

  return (
    <>
      {!token && (
        <form onSubmit={formik.handleSubmit}>
          <DialogContent>
            <Stack spacing={2}>
              <FormControl>
                <InputLabel id="typeLabel">Token Type</InputLabel>
                <Select
                  id="type"
                  name="type"
                  label="Token Type"
                  value={formik.values.type}
                  onChange={formik.handleChange}
                >
                  {displayTokenTypes.map((type) => (
                    <MenuItem key={type} value={type}>
                      {capitalizeFirstLetter(type)}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl>
                <TextField
                  inputRef={focusFieldRef}
                  id="name"
                  name="name"
                  label="Token Name"
                  placeholder="Choose a descriptive name for your token"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.name && !!formik.errors.name}
                  helperText={formik.touched.name && formik.errors.name}
                  required={validationSchema.fields.name.tests.some(
                    (test) => test.OPTIONS.name === "required"
                  )}
                  inputProps={{
                    maxLength: 100,
                  }}
                />
              </FormControl>

              <FormControl>
                <TextField
                  id="description"
                  name="description"
                  label="Description"
                  placeholder="Add an optional description for your token"
                  multiline
                  value={formik.values.description}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.description && !!formik.errors.description
                  }
                  helperText={
                    formik.touched.description && formik.errors.description
                  }
                  required={validationSchema.fields.description.tests.some(
                    (test) => test.OPTIONS.name === "required"
                  )}
                  inputProps={{
                    maxLength: 1000,
                  }}
                />
              </FormControl>

              <Stack direction={{ sm: "row", xs: "column" }} spacing={2}>
                <FormControl sx={{ flexGrow: 1, flexBasis: 0 }}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      label="Start Date"
                      value={formik.values.start}
                      onChange={(value) => formik.setFieldValue("start", value)}
                      onBlur={() => formik.setFieldTouched("start", true)}
                      minDate={now}
                      slotProps={{
                        textField: {
                          helperText: !!formik.errors.start
                            ? formik.errors.start
                            : "Defaults to today.",
                          error: !!formik.errors.start,
                        },
                      }}
                    />
                  </LocalizationProvider>
                </FormControl>

                <FormControl sx={{ flexGrow: 1, flexBasis: 0 }}>
                  <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DatePicker
                      label="Expiration Date"
                      value={formik.values.expiration}
                      onChange={(value) =>
                        formik.setFieldValue("expiration", value)
                      }
                      onBlur={() => formik.setFieldTouched("expiration", true)}
                      minDate={formik.values.start}
                      maxDate={dayjs(formik.values.start).add(1, "year")}
                      slotProps={{
                        textField: {
                          helperText: !!formik.errors.expiration
                            ? formik.errors.expiration
                            : "Defaults to 1 year from today.",
                          error: !!formik.errors.expiration,
                        },
                      }}
                    />
                  </LocalizationProvider>
                </FormControl>
              </Stack>
            </Stack>
          </DialogContent>
          <DialogActions>
            <LoadingButton variant="contained" type="submit" loading={loading}>
              Create Token
            </LoadingButton>
          </DialogActions>
        </form>
      )}
      {token && (
        <CreateApiTokenSuccess token={token} handleClose={handleClose} />
      )}
    </>
  );
}
