import React, { createContext, useContext, useMemo, useReducer } from "react";
import { useClouds } from "@/contexts";
import { cloudCredentialReducer } from "./CloudCredentialReducer";
import SelectCloud from "./SelectCloud";

const CloudCredentialContext = createContext();

export const useCloudCredential = () => useContext(CloudCredentialContext);

/**
 * The CloudCredentialProvider provides top level context for the cloud credential wizard,
 * including selected cloud, active step, and validation state.
 *
 * Cloud-specific logic for credential creation is handled by the cloud-specific context providers
 * (AWSCredentialContext and OpenstackCredentialContext).
 *
 * @param {ReactNode} children
 * @param {string} cloudName - The name of the cloud to be selected by default. This is used after the Jetstream2 auth redirect to return the user to the correct step in the wizard.
 * @param {integer} wizardStep - The step the wizard should start on. This is used after the Jetstream2 auth redirect to return the user to the correct step in the wizard.
 * @param {Function} setAddCredentialType - Function to open the AddCredentialDialog, if
 * manually importing an existing openstack credential instead of creating one through the wizard.
 *
 * @returns {React.ReactNode} - The children components wrapped in the CloudCredentialContext provider.
 */
export const CloudCredentialProvider = ({
  children,
  cloudName,
  wizardStep,
  setAddCredentialType,
}) => {
  const [clouds] = useClouds();

  // Initial state for the cloud credential wizard
  const initialState = {
    activeStep: wizardStep || 0,
    selectedCloudId: clouds.find((c) => c.name === cloudName)?.id || null,
    isValid: false,
    error: null,
    // We initialize this dummy list of steps to render the Stepper until the user selects a cloud, at which this is updated to use the steps from the cloud-specific context.
    steps: [
      {
        title: "Select Cloud",
        validator: (_, selectedCloudId) => !!selectedCloudId,
        render: () => <SelectCloud clouds={clouds} />,
        nextText: "Next",
      },
      {
        title: "Create Credential",
        render: () => <></>,
      },
      {
        title: "Summary",
        render: () => <></>,
      },
    ],
  };

  // Use the cloudCredentialReducer to manage state
  const [state, dispatch] = useReducer(cloudCredentialReducer, initialState);

  // Get the selected cloud based on the selectedCloudId
  const selectedCloud = useMemo(() => {
    return clouds.find((c) => c.id === state.selectedCloudId);
  }, [clouds, state.selectedCloudId]);

  return (
    <CloudCredentialContext.Provider
      value={{ state, dispatch, setAddCredentialType, selectedCloud }}
    >
      {children}
    </CloudCredentialContext.Provider>
  );
};
