export const ActionTypes = {
  SET_SELECTED_CLOUD_ID: "SET_SELECTED_CLOUD_ID",
  SET_ACTIVE_STEP: "SET_ACTIVE_STEP",
  SET_IS_VALID: "SET_IS_VALID",
  SET_ERROR: "SET_ERROR",
  SET_STEPS: `SET_STEPS`,
};

export function cloudCredentialReducer(state, action) {
  switch (action.type) {
    case ActionTypes.SET_SELECTED_CLOUD_ID:
      return { ...state, selectedCloudId: action.payload };
    case ActionTypes.SET_ACTIVE_STEP:
      return { ...state, activeStep: action.payload };
    case ActionTypes.SET_IS_VALID:
      return { ...state, isValid: action.payload };
    case ActionTypes.SET_ERROR:
      return { ...state, error: action.payload };
    case ActionTypes.SET_STEPS:
      return {
        ...state,
        steps: [state.steps[0], ...action.payload],
      };
    default:
      throw new Error();
  }
}
