import {
  Alert,
  Box,
  Button,
  Grid,
  List,
  ListItemButton,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material";
import { useCloudCredential } from "./CloudCredentialContext";

const CloudButton = ({ cloud, disabled }) => {
  const { state, dispatch } = useCloudCredential();
  const { selectedCloudId } = state;
  return (
    <ListItemButton
      onClick={() =>
        dispatch({ type: "SET_SELECTED_CLOUD_ID", payload: cloud.id })
      }
      selected={selectedCloudId === cloud.id}
      style={{
        border: "1px solid #dfdfdf",
        minHeight: "1rem",
        borderRadius: "5px",
      }}
      disabled={disabled}
    >
      <Box p={1} ml={1} sx={{ margin: "auto", paddingX: "1rem" }}>
        <Typography align="center">{cloud.name}</Typography>
      </Box>
    </ListItemButton>
  );
};

/**
 * Step 1 of Credential Creation Wizard. Allows user to select cloud
 * to create credentials for.
 */
const SelectCloud = ({ clouds }) => {
  const { setAddCredentialType } = useCloudCredential();
  const theme = useTheme();
  return (
    <Stack
      spacing={2}
      justifyContent="center"
      paddingTop={4}
      sx={{
        width: "100%",
        margin: "auto",
        [theme.breakpoints.up("md")]: {
          width: "80%",
        },
      }}
    >
      <Box display="flex" justifyContent="center">
        <Typography>Select a cloud to create a credential for.</Typography>
      </Box>
      <Box display="flex" justifyContent="center">
        <List>
          <Grid container dispay="flex" direction="row" spacing={1}>
            {clouds &&
              clouds.map((cloud, index) => (
                <Grid key={index} item md={6} sm={12} xs={12}>
                  <CloudButton cloud={cloud} />
                </Grid>
              ))}

            <Tooltip title="Not Available">
              <Grid item md={6} sm={12} xs={12}>
                <CloudButton
                  cloud={{ id: "kube", name: "Kubernetes" }}
                  disabled={true}
                />
              </Grid>
            </Tooltip>

            <Tooltip title="Not Available">
              <Grid item md={6} sm={12} xs={12}>
                <CloudButton
                  cloud={{ id: "google", name: "Google" }}
                  disabled={true}
                />
              </Grid>
            </Tooltip>
          </Grid>
        </List>
      </Box>

      <Box display="flex" justifyContent="center">
        <Button
          color="secondary"
          onClick={() => setAddCredentialType("application")}
        >
          <Typography variant="caption">
            Existing Credentials? Click here
          </Typography>
        </Button>
      </Box>
    </Stack>
  );
};

export default SelectCloud;
