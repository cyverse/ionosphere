import { Dialog, Typography } from "@mui/material";
import {
  CloudQueue as CloudOutlineIcon,
  VpnKey as CredentialIcon,
} from "@mui/icons-material";

import { useCloudCredential } from "./CloudCredentialContext";
import { OpenstackProvider } from "./openstack/OpenstackCredentialContext";
import { AWSProvider } from "./aws/AWSCredentialContext";

import CloudCredentialDialogContent from "./CloudCredentialDialogContent";
import DialogAppBar from "@/components/Common/Dialog/DialogAppBar";

/**
 * Dynamic context provider for the Cloud Credential wizard.
 *
 * This component wraps the children components with the appropriate context provider
 * based on the selected cloud.
 * @param {string} props.cloudType - The type of cloud selected by the user (e.g., 'aws', 'openstack').
 * @param {React.ReactNode} props.children - The child components to be wrapped by the cloud-specific provider.
 * @returns {React.ReactNode} - The children components wrapped in the appropriate cloud provider context, or the children as is if no specific provider is matched.
 */
const CloudContextWrapper = ({ cloudType, children }) => {
  switch (cloudType) {
    case "aws":
      return <AWSProvider>{children}</AWSProvider>;
    case "openstack":
      return <OpenstackProvider>{children}</OpenstackProvider>;
    default:
      return children;
  }
};

/**
 * Dialog for creating a cloud credential. Depending on the selected cloud,
 * the dialog renders the appropriate credential form wrapped in a context provider specific to
 * the selected cloud.
 */
export default function CloudCredentialDialog({
  open,
  handleClose,
  handleSubmit,
}) {
  const { state, selectedCloud } = useCloudCredential();

  return (
    <Dialog
      open={open}
      onClose={(_, reason) => {
        reason !== "backdropClick" && handleClose && handleClose();
      }}
      maxWidth="md"
      fullWidth
    >
      <DialogAppBar
        handleClose={handleClose}
        title={"New Cloud Credential"}
        icon={<CredentialIcon fontSize="small" />}
        endText={
          state.selectedCloudId && (
            <>
              <CloudOutlineIcon />
              <Typography style={{ marginLeft: "0.5em" }}>
                {selectedCloud?.name}
              </Typography>
            </>
          )
        }
      />

      <CloudContextWrapper cloudType={selectedCloud?.type}>
        <CloudCredentialDialogContent
          handleSubmit={handleSubmit}
          handleClose={handleClose}
        />
      </CloudContextWrapper>
    </Dialog>
  );
}
