import { Alert, Box, Stack, TextField, Typography } from "@mui/material";
import { useTheme } from "@mui/material";
import { useAWS } from "./AWSCredentialContext";

export default function AWSCredentialForm() {
  const theme = useTheme();
  const { credentialFormik, credentialError } = useAWS();

  return (
    <Box
      sx={{
        width: "100%",
        margin: "auto",
        [theme.breakpoints.up("sm")]: {
          width: "80%",
        },
        [theme.breakpoints.up("md")]: {
          width: "60%",
        },
      }}
    >
      <form>
        <Stack spacing={2} mt={4}>
          <Typography>Add your AWS access key and secret key below.</Typography>
          <TextField
            fullWidth
            id="credName"
            name="credName"
            label="Credential Name"
            value={credentialFormik.values.credName}
            onChange={credentialFormik.handleChange}
            onBlur={credentialFormik.handleBlur}
            error={Boolean(credentialFormik.errors.credName)}
            helperText={credentialFormik.errors.credName}
            autoComplete="off"
          />
          <TextField
            fullWidth
            id="accessKey"
            name="accessKey"
            label="Access Key ID"
            value={credentialFormik.values.accessKey}
            onChange={credentialFormik.handleChange}
            onBlur={credentialFormik.handleBlur}
            error={
              credentialFormik.touched.accessKey &&
              Boolean(credentialFormik.errors.accessKey)
            }
            helperText={
              credentialFormik.touched.accessKey &&
              credentialFormik.errors.accessKey
            }
            autoComplete="off"
          />
          <TextField
            fullWidth
            id="secretKey"
            name="secretKey"
            label="Secret Access Key"
            value={credentialFormik.values.secretKey}
            onChange={credentialFormik.handleChange}
            onBlur={credentialFormik.handleBlur}
            error={
              credentialFormik.touched.secretKey &&
              Boolean(credentialFormik.errors.secretKey)
            }
            helperText={
              credentialFormik.touched.secretKey &&
              credentialFormik.errors.secretKey
            }
            autocomplete="new-password"
            type="password"
          />
          {credentialError && <Alert severity="error">{credentialError}</Alert>}
        </Stack>
      </form>
    </Box>
  );
}
