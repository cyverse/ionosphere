import { useEffect } from "react";
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { LoadingButton } from "@mui/lab";

import { useAWS } from "./AWSCredentialContext";
import { useUserConfig } from "@/contexts";

export default function AWSConfirmation() {
  const theme = useTheme();
  const { regionFormik, regions, createdCredential } = useAWS();
  const [userConfig] = useUserConfig();

  // if user has no default region, select the first region in the list
  useEffect(() => {
    if (regions.length > 0 && !regionFormik.values.region)
      regionFormik.setFieldValue("region", regions[0].id);
  }, [regions]);

  return (
    <Box
      sx={{
        width: "100%",
        margin: "auto",
        [theme.breakpoints.up("sm")]: {
          width: "80%",
        },
        [theme.breakpoints.up("md")]: {
          width: "60%",
        },
      }}
    >
      <Box mt={4}>
        <Stack direction={"row"}>
          <CheckCircleIcon
            sx={{
              marginRight: 1,
              color: "green",
            }}
          />
          <Typography>Credential created.</Typography>
        </Stack>
        <form onSubmit={regionFormik.handleSubmit}>
          <Stack spacing={3} mt={5}>
            <Typography>
              If you'd like, set a default region for your AWS deployments.
            </Typography>

            <Stack spacing={2} justifyContent="flex-start">
              <FormControl variant="outlined" fullWidth>
                <InputLabel id="region" shrink>
                  AWS Region
                </InputLabel>
                <Select
                  name="region"
                  id="region"
                  value={regionFormik.values.region}
                  label="AWS Region"
                  onChange={regionFormik.handleChange}
                >
                  {regions &&
                    regions.map((region, index) => (
                      <MenuItem key={index} value={region.id}>
                        {region.name}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
              <LoadingButton
                color="primary"
                variant="contained"
                type="submit"
                // disable the button if the form is invalid or if the current region is already saved
                disabled={
                  !regionFormik.isValid ||
                  (userConfig.aws_default_region &&
                    regionFormik.values.region ===
                      userConfig.aws_default_region)
                }
                sx={{ width: "auto", alignSelf: "flex-start" }}
                loading={regionFormik.isSubmitting}
              >
                {userConfig.aws_default_region &&
                regionFormik.values.region === userConfig.aws_default_region
                  ? "Saved"
                  : "Save"}
              </LoadingButton>
            </Stack>
          </Stack>
        </form>
      </Box>
    </Box>
  );
}
