import React, { createContext, useContext, useEffect } from "react";
import { useFormik } from "formik";
import * as yup from "yup";

import {
  useAPI,
  useCredentials,
  useError,
  useUser,
  useUserConfig,
} from "@/contexts";
import { useCloudCredential } from "../CloudCredentialContext";
import { ActionTypes as CloudCredentialActionTypes } from "../CloudCredentialReducer";

import AWSCredentialForm from "./AWSCredentialForm";
import AWSConfirmation from "./AWSConfirmation";

// Create context for AWS Credential Wizard
const AWSContext = createContext();

// Custom hook for using the AWS context
export const useAWS = () => useContext(AWSContext);

// Validation schema for select default AWS region form
const regionValidationSchema = yup.object({
  region: yup.string().required("Please select a region"),
});

// Steps for AWS Credential Wizard
const steps = [
  {
    title: "Create Credential",
    validator: (formik) => formik.dirty && formik.isValid,
    render: () => <AWSCredentialForm />,
  },
  {
    title: "Select Region",
    render: () => <AWSConfirmation />,
    submitText: "Close",
    disablePrev: true,
    submitVariant: "outlined",
  },
];

/**
 * Context provider for creating an AWS credential in the Cloud Credential Wizard.
 * Must be used within CredentialProvider and UserConfigProvider.
 *
 * Includes formik form for creating a new AWS credential, and logic for validating
 * the credential with AWS. After credential creation, the user can select a default AWS region
 * which is saved to their user config.
 *
 * @param {*} children - Child components to be wrapped by the AWS context provider.
 * @returns {React.ReactNode} - The children components wrapped in the AWS context provider.
 */

export const AWSProvider = ({ children }) => {
  const api = useAPI();
  const [user] = useUser();
  const [userConfig, setUserConfig] = useUserConfig();
  const [credentials, setCredentials] = useCredentials();
  const [_, setError] = useError();

  // Get the main cloud credential context
  const { state, dispatch } = useCloudCredential();

  // State for creating credential
  const [creatingCredential, setCreatingCredential] = React.useState(false);
  const [createdCredential, setCreatedCredential] = React.useState(null);
  const [credentialError, setCredentialError] = React.useState(null);

  // AWS regions
  const [regions, setRegions] = React.useState(null);

  // Validation schema for Create AWS credential form
  // In the provider to allow for dynamic validation
  const credentialValidationSchema = yup.object({
    accessKey: yup
      .string("Enter your Access key ID.")
      .required("Access key ID is required"),
    secretKey: yup
      .string("Enter your Secret access key.")
      .required("Secret is required"),
    credName: yup
      .string()
      .required("Please choose a name for your credential")
      .test(
        "is-unique",
        "There is already a credential with this name",
        (value) => !credentials.some((credential) => credential.name === value)
      ),
  });

  // Formik for Create AWS credential form
  const credentialFormik = useFormik({
    initialValues: {
      accessKey: "",
      secretKey: "",
      credName: "",
    },
    validationSchema: credentialValidationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  /**
   * Saves `aws_default_region` to user config.
   * @param {Object} values regionFormik.values
   */
  const saveDefaultRegion = async (values) => {
    // Clear any errors
    dispatch({
      type: "SET_ERROR",
      payload: null,
    });
    try {
      // Save default region to user config
      await api.setUserConfig(
        user.username,
        "aws_default_region",
        values.region
      );
      // Update user config context
      const config = await api.userConfigs(user.username);
      setUserConfig(config);
    } catch (error) {
      dispatch({
        type: "SET_ERROR",
        payload: "Error saving region: " + error.response.data.message,
      });
    }
  };

  // Formik for Select default AWS region form
  const regionFormik = useFormik({
    initialValues: {
      // If user already has a default aws region set, use that
      region: userConfig.aws_default_region || "",
    },
    validationSchema: regionValidationSchema,
    onSubmit: saveDefaultRegion,
  });

  // Refresh credentials context
  const fetchCredentials = async () => {
    try {
      const res = await api.credentials();
      setCredentials(res);
    } catch (error) {
      setError("Could not refresh credentials.");
    } finally {
      setCreatingCredential(false);
    }
  };

  /**
   * Creates a new credential of type 'aws'.
   *
   * After creating the credential, we fetch regions as a mechanism to
   * to validate the credentials with AWS. If the credential is invalid,
   * it is deleted immediately and the user is shown an error message.
   * @param {Object} values credentialFormik.values
   */
  const createCredential = async (values) => {
    // Clear any errors and show loading state
    setCredentialError(null);
    setCreatingCredential(true);

    // Trim whitespace from access and secret keys
    const value = {
      AWS_ACCESS_KEY_ID: values.accessKey.trim(),
      AWS_SECRET_ACCESS_KEY: values.secretKey.trim(),
    };

    // Build the request object
    const req = {
      name: values.credName,
      type: "aws",
      // tags_kv: { provider_placeholder: state.selectedCloudId },
      value: JSON.stringify(value),
    };

    try {
      // Create the credential
      const response = await api.createCredential(req);
      const newCredentialId = response.credential;

      // Fetch regions to validate the credential
      try {
        const regions = await api.providerRegions(state.selectedCloudId, {
          credential: newCredentialId,
        });
        setRegions(regions);
        setCreatedCredential(response.credential);
        // Move to next step after credential is validated
        dispatch({
          type: CloudCredentialActionTypes.SET_ACTIVE_STEP,
          payload: state.activeStep + 1,
        });
      } catch (error) {
        // If we can't fetch regions, assume the credential is invalid and delete it
        // TODO: Improve error handling
        setCredentialError(
          "AWS was not able to validate the provided access credentials. Please check your access key and secret key and try again."
        );
        // Delete invalid credential
        try {
          await api.deleteCredential(newCredentialId);
        } catch (error) {
          console.log(error.response);
        }
      }
      fetchCredentials();
    } catch (error) {
      setCredentialError(error.response.data.message);
      setCreatingCredential(false);
    }
  };

  /**
   * Handles the next button click.
   */
  const handleNext = async () => {
    if (state.activeStep === 1) {
      createCredential(credentialFormik.values, state.selectedCloudId);
    } else {
      dispatch({
        type: CloudCredentialActionTypes.SET_ACTIVE_STEP,
        payload: state.activeStep + 1,
      });
    }
  };

  // Validate current step to enable/disable next button
  useEffect(() => {
    const validator = state.steps[state.activeStep].validator;
    dispatch({
      type: CloudCredentialActionTypes.SET_IS_VALID,
      payload: !validator || validator(credentialFormik, state.selectedCloudId),
    });
  }, [state.activeStep, credentialFormik.isValid, state.selectedCloudId]);

  // Update main context to use AWS step configuration
  useEffect(() => {
    dispatch({
      type: CloudCredentialActionTypes.SET_STEPS,
      payload: steps,
    });
  }, []);

  return (
    <AWSContext.Provider
      value={{
        credentialFormik,
        createdCredential,
        credentialError,
        busy: creatingCredential,
        busyMessage: creatingCredential ? "Creating credential" : "",
        handleNext,
        regionFormik,
        regions,
      }}
    >
      {children}
    </AWSContext.Provider>
  );
};
