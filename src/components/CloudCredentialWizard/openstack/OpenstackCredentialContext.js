import React, {
  createContext,
  useContext,
  useEffect,
  useReducer,
  useState,
} from "react";

import { useConfig } from "@/contexts";
import { useCloudCredential } from "../CloudCredentialContext";
import { ActionTypes as CloudCredentialActionTypes } from "../CloudCredentialReducer";

import {
  openstackReducer,
  ActionTypes as OpenstackActionTypes,
} from "./OpenstackCredentialReducer";
import { useOpenstackProjects } from "./hooks/useOpenstackProjects";
import { useCreateCredentials } from "./hooks/createOpenstackCredentials";

import SelectOpenstackProjects from "./steps/SelectOpenstackProjects";
import OpenstackConfirmation from "./steps/OpenstackConfirmation";

// Create the openstack context
const OpenstackContext = createContext();

// Custom hook for using the openstack context
export const useOpenstack = () => useContext(OpenstackContext);

// Initial state for the openstack context
const initialState = {
  values: { selectedProjects: [] },
  canCreateCredentials: null,
};

// Get the steps for the openstack credential creation wizard
const getSteps = (canCreateCredentials) => {
  const validateSelectMultiple = (selected) => {
    if (!selected.length) return "Please make a selection";
  };

  const steps = [
    {
      title: "Select Allocations",
      validator: (values) =>
        !validateSelectMultiple(values["selectedProjects"]),
      render: () => <SelectOpenstackProjects />,
      nextText: canCreateCredentials === false ? "Close" : "Create",
      prevText: "Back",
      disableNext: canCreateCredentials === false,
    },
    {
      title: "Summary",
      render: () => <OpenstackConfirmation />,
      submitText: "OK",
      disablePrev: true,
    },
  ];

  return steps;
};

/**
 * Provider for creating a Jetstream2 credential in the Cloud Credential Wizard.
 *
 * After selecting Jetstream2, the user is redirected to authenticate with Jetstream2 and
 * then select their Jetstream2 projects to create credentials for.
 *
 * @param {*} param0
 * @returns
 */
export const OpenstackProvider = ({ children }) => {
  const config = useConfig();

  // Get the main cloud credential context
  const { state, dispatch, selectedCloud } = useCloudCredential();

  // Custom hook for getting openstack projects
  const {
    getOpenstackProjects,
    openstackProjects,
    loadingProjects,
    projectError,
  } = useOpenstackProjects();

  // Custom hook for creating openstack credentials
  const {
    createCredentials,
    createdCredentials,
    failedCredentials,
    creatingCredentials,
    credentialError,
  } = useCreateCredentials();

  // Manage the state of the openstack context using a reducer
  const [openstackState, openstackDispatch] = useReducer(
    openstackReducer,
    initialState
  );

  const [busy, setBusy] = useState(false);

  const handleNext = async (values) => {
    // If production, redirect to Jetstream 2 login. If development, skip to next step.
    if (state.activeStep === 0 && selectedCloud.name === "Jetstream 2") {
      if (!config.isDevelopment) {
        setBusy(true);
        window.location.replace(
          `https://js2.jetstream-cloud.org:5000/identity/v3/auth/OS-FEDERATION/websso/openid?origin=${window.location.origin}`
        );
      } else {
        dispatch({
          type: CloudCredentialActionTypes.SET_ACTIVE_STEP,
          payload: state.activeStep + 1,
        });
      }
    } else if (
      state.activeStep === 1 &&
      openstackState.values["selectedProjects"]
    ) {
      createCredentials(
        openstackState.values,
        openstackProjects,
        state.selectedCloudId
      );
      dispatch({
        type: CloudCredentialActionTypes.SET_ACTIVE_STEP,
        payload: state.activeStep + 1,
      });
    } else {
      dispatch({
        type: CloudCredentialActionTypes.SET_ACTIVE_STEP,
        payload: state.activeStep + 1,
      });
    }
  };

  // On step 1, fetch openstack projects
  useEffect(() => {
    if (selectedCloud?.name === "Jetstream 2" && state.activeStep === 1) {
      getOpenstackProjects();
    }
  }, [state.activeStep, selectedCloud]);

  // Once openstack projects have been fetched, determine if user can create credentials
  useEffect(() => {
    if (openstackProjects === null) return;
    const isEligible = (element) => !element.hasCredential;
    openstackDispatch({
      type: OpenstackActionTypes.SET_CAN_CREATE_CREDENTIALS,
      payload: openstackProjects.some(isEligible),
    });
  }, [openstackProjects]);

  // Set error message in main context if there is an error fetching openstack projects
  useEffect(() => {
    if (projectError) {
      dispatch({
        type: CloudCredentialActionTypes.SET_ERROR,
        payload: projectError,
      });
    }
  }, [projectError]);

  // Update steps once we know if user can create openstack credentials
  useEffect(() => {
    dispatch({
      type: CloudCredentialActionTypes.SET_STEPS,
      payload: getSteps(openstackState.canCreateCredentials),
    });
  }, [openstackState.canCreateCredentials]);

  // Set error message in main context if there is an error creating credentials
  useEffect(() => {
    if (credentialError) {
      dispatch({
        type: CloudCredentialActionTypes.SET_ERROR,
        payload: credentialError,
      });
    }
  }, [credentialError]);

  // Validate current step to enable/disable next button
  useEffect(() => {
    const validator = state.steps[state.activeStep].validator;
    dispatch({
      type: CloudCredentialActionTypes.SET_IS_VALID,
      payload:
        !validator || validator(openstackState.values, state.selectedCloudId),
    });
  }, [state.activeStep, openstackState.values, state.selectedCloudId]);

  const getBusyMessage = () => {
    if (loadingProjects) return "Loading allocations";
    else if (creatingCredentials) return "Creating credentials";
    else if (busy) return "Authenticating";
    else return null;
  };

  return (
    <OpenstackContext.Provider
      value={{
        openstackState,
        openstackDispatch,
        handleNext,
        openstackProjects,
        busy: loadingProjects || creatingCredentials || busy,
        busyMessage: getBusyMessage(),
        createdCredentials,
        failedCredentials,
      }}
    >
      {children}
    </OpenstackContext.Provider>
  );
};
