import { useState } from "react";
import axios from "axios";
import { useAPI, useJs2Allocations } from "@/contexts";
import { useCredentials, useError } from "@/contexts";

/**
 * Custom hook for creating openstack credentials.
 * @returns {Object} - Object containing the createCredentials function, busy state, error state, and createdCredentials state.
 */
export const useCreateCredentials = () => {
  const api = useAPI();
  const [, setJs2Allocations] = useJs2Allocations();
  const [, setCredentials] = useCredentials();

  const [busy, setBusy] = useState(false);
  const [error, setError] = useState(null);
  const [createdCredentials, setCreatedCredentials] = useState([]);
  const [failedCredentials, setFailedCredentials] = useState([]);

  /**
   * Refresh the credential context and js2Allocation context after creating
   * new credentials.
   */
  const refreshCredentialContext = async () => {
    try {
      const res = await api.credentials();
      setCredentials(res);
    } catch (error) {
      setError("Error refreshing credentials.");
    }

    try {
      const js2Allocations = await api.js2allocations({ nocache: true });
      setJs2Allocations(js2Allocations);
    } catch (error) {
      setError("Error refreshing js2 allocations.");
    }
  };

  const createCredentials = async (
    values,
    openstackProjects,
    selectedCloudId
  ) => {
    setBusy(true);
    setError(null); // Reset previous errors

    let credentialsToCreate = [];
    let projectNames = [];

    // Based on the selected projects, create a list of credentials to create
    openstackProjects.forEach((project) => {
      if (values.selectedProjects.includes(project.id)) {
        projectNames.push(project.name);
        credentialsToCreate.push({
          scope: {
            project: {
              id: project.id,
            },
            domain: {
              id: project.domain_id,
            },
          },
        });
      }
    });

    try {
      // Call the API to create the credentials
      const res = await axios.get("/openstack/create-credentials", {
        params: {
          selectedCloudId: selectedCloudId,
          credentialsToCreate,
        },
        withCredentials: true,
      });

      let promiseResults = res.data.promiseResults;
      let successes = [];
      let failures = [];

      // Split the results into successes and failures
      promiseResults.forEach((result, index) => {
        if (result.status === "fulfilled") {
          successes.push(projectNames[index]);
        } else {
          failures.push(projectNames[index]);
        }
      });

      if (successes.length) {
        refreshCredentialContext();
      }

      setCreatedCredentials(successes);
      setFailedCredentials(failures);
    } catch (error) {
      console.error(error);
      setError(
        error.response?.data?.message ||
          "There was an error creating your credentials."
      );
    } finally {
      setBusy(false);
    }
  };

  return {
    createCredentials,
    creatingCredentials: busy,
    createdCredentials,
    failedCredentials,
    credentialError: error,
  };
};
