import { useState } from "react";
import axios from "axios";
import { useCredentials } from "@/contexts";
import { useCloudCredential } from "../../CloudCredentialContext";

export const useOpenstackProjects = () => {
  const [credentials] = useCredentials();
  const { state } = useCloudCredential();

  const [busy, setBusy] = useState(false);
  const [openstackProjects, setProjects] = useState(null);
  const [error, setError] = useState(null);

  const getOpenstackProjects = async () => {
    setError(null);
    setBusy(true);

    try {
      const res = await axios.get("/openstack/get-projects", {
        params: { selectedCloudId: state.selectedCloudId },
        withCredentials: true,
      });

      let projectList = res.data?.projectList;

      if (projectList && projectList.length) {
        // get CACAO-managed credentials
        const cacaoCredentials = credentials.filter(
          (cred) =>
            cred.tags_kv?.cacao_managed === "true" ||
            cred.tags?.includes("cacao_managed")
        );

        // Check each openstack project for a CACAO-managed credential.
        if (cacaoCredentials.length) {
          projectList.forEach((project, index, projectList) => {
            const projectCred = cacaoCredentials.find(
              (cred) => cred.tags_kv?.cacao_openstack_project_id === project.id
            );

            if (projectCred) {
              projectList[index].hasCredential = true;
            } else {
              projectList[index].hasCredential = false;
            }
          });
          // Otherwise, all projects are eligible for credential creation
        } else {
          projectList.forEach((project, index, projectList) => {
            projectList[index].hasCredential = false;
          });
        }
        setProjects(projectList);
      } else {
        setProjects([]);
      }
      setProjects(projectList);
    } catch (e) {
      setError(e.message);
    } finally {
      setBusy(false);
    }
  };

  return {
    getOpenstackProjects,
    openstackProjects,
    loadingProjects: busy,
    projectError: error,
  };
};
