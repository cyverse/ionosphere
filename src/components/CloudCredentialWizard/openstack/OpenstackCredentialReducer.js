export const ActionTypes = {
  SET_SELECTED_PROJECTS: "SET_SELECTED_PROJECTS",
  SET_CAN_CREATE_CREDENTIALS: "SET_CAN_CREATE_CREDENTIALS",
  ADD_OPENSTACK_PROJECT: "ADD_OPENSTACK_PROJECT",
  REMOVE_OPENSTACK_PROJECT: "REMOVE_OPENSTACK_PROJECT",
};

export function openstackReducer(state, action) {
  switch (action.type) {
    case ActionTypes.SET_SELECTED_PROJECTS:
      return {
        ...state,
        values: { ...state.values, selectedProjects: action.payload },
      };
    case ActionTypes.SET_CAN_CREATE_CREDENTIALS:
      return { ...state, canCreateCredentials: action.payload };
    case ActionTypes.SET_CREATED_CREDENTIALS:
      return { ...state, createdCredentials: action.payload };
    case ActionTypes.SET_FAILED_CREDENTIALS:
      return { ...state, failedCredentials: action.payload };
    case ActionTypes.ADD_OPENSTACK_PROJECT:
      return {
        ...state,
        values: {
          ...state.values,
          selectedProjects: [...state.values.selectedProjects, action.payload],
        },
      };
    case ActionTypes.REMOVE_OPENSTACK_PROJECT:
      return {
        ...state,
        values: {
          ...state.values,
          selectedProjects: state.values.selectedProjects.filter(
            (projectId) => projectId !== action.payload
          ),
        },
      };
    case ActionTypes.SET_CAN_CREATE_CREDENTIALS:
      return { ...state, canCreateOpenstackCredentials: action.payload };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}
