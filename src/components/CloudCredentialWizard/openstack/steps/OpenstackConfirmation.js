import { Box, Typography } from "@mui/material";
import {
  CheckCircle as CheckCircleIcon,
  Warning as WarningIcon,
} from "@mui/icons-material";

import { useOpenstack } from "../OpenstackCredentialContext";
/**
 * Step 3 of Credential Creation Wizard. Tells user whether credentials
 * were created successfully.
 */
export default function OpenstackConfirmation() {
  const { createdCredentials, failedCredentials } = useOpenstack();

  return (
    <Box display="flex" justifyContent="center" marginY={4}>
      {failedCredentials?.length > 0 && (
        <Box display="flex" alignItems="start" direction="row" pb={3}>
          <Box>
            <WarningIcon
              style={{
                width: "3rem",
                color: "orange",
                marginLeft: "1rem",
                marginRight: ".3rem",
                marginTop: "-.1rem",
              }}
            />
          </Box>
          <Box>
            <Typography variant="body1">
              Error creating credentials for the following projects:
            </Typography>
            {failedCredentials.map((projectName, index) => (
              <Typography
                key={index}
                variant="subtitle2"
                style={{ marginTop: "1rem" }}
              >
                {projectName}
              </Typography>
            ))}
          </Box>
        </Box>
      )}

      {createdCredentials?.length > 0 && (
        <Box display="flex" alignItems="start" direction="row">
          <Box>
            <CheckCircleIcon
              style={{
                width: "3rem",
                color: "green",
                marginLeft: "1rem",
                marginRight: ".3rem",
                marginTop: "-.1rem",
              }}
            />
          </Box>
          <Box>
            <Typography variant="body1">
              Success! Credentials created for the following projects:
            </Typography>
            {createdCredentials.map((projectName, index) => (
              <Typography
                key={index}
                variant="subtitle2"
                style={{ marginTop: "1rem" }}
              >
                {projectName}
              </Typography>
            ))}
          </Box>
        </Box>
      )}
    </Box>
  );
}
