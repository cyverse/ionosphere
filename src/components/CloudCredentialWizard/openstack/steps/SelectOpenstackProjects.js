import {
  Box,
  Grid,
  List,
  ListItemButton,
  Stack,
  Typography,
} from "@mui/material";
import { useOpenstack } from "../OpenstackCredentialContext";
import { useCloudCredential } from "../../CloudCredentialContext";
import { ActionTypes } from "../OpenstackCredentialReducer";

const ProjectButton = ({ project, selected, onClick, disabled }) => {
  return (
    <ListItemButton
      style={{
        border: "1px solid #dfdfdf",
        height: "100%",
        borderRadius: "5px",
      }}
      selected={selected}
      disabled={disabled}
      onClick={onClick}
    >
      <Stack sx={{ minWidth: "100%" }} spacing={0.25} paddingY={1}>
        <Typography variant="h6" align="center">
          {project.name}
        </Typography>
        <Typography variant="body2" align="center">
          {project.description}
        </Typography>
      </Stack>
    </ListItemButton>
  );
};

/**
 * Step 2 of Credential Creation Wizard. Allows user to select js2
 * allocations/projects to create credentials for.
 */
const SelectOpenstackProjects = () => {
  const { openstackState, openstackDispatch, openstackProjects } =
    useOpenstack();
  const { selectedProjects } = openstackState.values;
  const { state } = useCloudCredential();

  return (
    <Box sx={{ maxWidth: "90%", margin: "auto" }}>
      {openstackProjects?.length === 0 && (
        <Box
          display="flex"
          justifyContent="center"
          alignContent="center"
          mt={3}
        >
          <Typography>No Jetstream2 allocations found.</Typography>
        </Box>
      )}

      {openstackProjects?.length > 0 && (
        <Stack spacing={2} marginTop={2}>
          <Box display="flex" justifyContent="center" mt={3}>
            {openstackState.canCreateCredentials ? (
              <Typography variant="body1">
                Select Jetstream2 allocations to create credentials for.
              </Typography>
            ) : (
              <Typography align="center">
                Nothing to do here! All allocations already have credentials.
              </Typography>
            )}
          </Box>
          <List>
            <Grid container spacing={1} direction="row" alignItems="stretch">
              {openstackProjects.map((project, index) => (
                <Grid item md={6} sm={12} xs={12} key={index}>
                  <ProjectButton
                    project={project}
                    selected={selectedProjects.includes(project.id)}
                    disabled={project.hasCredential}
                    onClick={() => {
                      if (selectedProjects.includes(project.id)) {
                        openstackDispatch({
                          type: ActionTypes.REMOVE_OPENSTACK_PROJECT,
                          payload: project.id,
                        });
                      } else {
                        openstackDispatch({
                          type: ActionTypes.ADD_OPENSTACK_PROJECT,
                          payload: project.id,
                        });
                      }
                    }}
                  />
                </Grid>
              ))}
            </Grid>
          </List>

          <Box display="flex" justifyContent="center">
            <Typography variant="caption">
              Projects with existing credentials cannot be selected.
            </Typography>
          </Box>
        </Stack>
      )}
    </Box>
  );
};

export default SelectOpenstackProjects;
