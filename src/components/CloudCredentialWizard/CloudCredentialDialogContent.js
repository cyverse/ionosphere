import {
  Alert,
  Box,
  CircularProgress,
  DialogContent,
  Typography,
  useTheme,
} from "@mui/material";

import { FormStepper, FormControls } from "../forms/FormStepper";
import { useCloudCredential } from "./CloudCredentialContext";
import { useOpenstack } from "./openstack/OpenstackCredentialContext";
import { ActionTypes } from "./CloudCredentialReducer";
import { useAWS } from "./aws/AWSCredentialContext";

/**
 * Credential Creation Wizard.
 */
export default function CloudCredentialDialogContent({
  handleSubmit,
  handleClose,
}) {
  const theme = useTheme();
  const { state, dispatch, selectedCloud } = useCloudCredential();
  const { activeStep, isValid, error, steps } = state;

  const openstackContext = useOpenstack() || {};
  const awsContext = useAWS() || {};

  let handleNext, busy, busyMessage;

  switch (selectedCloud?.type) {
    case "openstack":
      ({ handleNext, busy, busyMessage } = openstackContext);
      break;
    case "aws":
      ({ handleNext, busy, busyMessage } = awsContext);
      break;
    default:
      handleNext = () => {};
      break;
  }

  const handleBack = () => {
    dispatch({ type: ActionTypes.SET_ERROR, payload: null });
    dispatch({
      type: ActionTypes.SET_ACTIVE_STEP,
      payload: state.activeStep - 1,
    });
  };

  return (
    <>
      <DialogContent style={{ minHeight: "20vh" }}>
        <FormStepper
          activeStep={activeStep}
          steps={steps.map((s) => s.title)}
        />
        <Box mt={2} mb={2} sx={{ overflowX: "hidden", overflowY: "auto" }}>
          {!busy ? (
            <>
              {steps[activeStep].render()}
              {error && (
                <Box
                  sx={{
                    width: "100%",
                    mt: 2,
                    mr: "auto",
                    mb: "auto",
                    ml: "auto",
                    [theme.breakpoints.up("sm")]: {
                      width: "80%",
                    },
                    [theme.breakpoints.up("md")]: {
                      width: "60%",
                    },
                  }}
                  display={"flex"}
                  justifyContent={"center"}
                >
                  <Alert severity="error">{error}</Alert>
                </Box>
              )}
            </>
          ) : (
            <>
              <Box sx={{ display: "flex", justifyContent: "center" }} p={2}>
                <div>
                  <CircularProgress size="3rem" />
                </div>
              </Box>
              <Box sx={{ display: "flex", justifyContent: "center" }} pb={3}>
                <div>
                  <Typography variant="body2">{busyMessage}</Typography>
                </div>
              </Box>
            </>
          )}
        </Box>
      </DialogContent>
      <Box display="flex" justifyContent="center" mt={2} mb={3}>
        <FormControls
          step={steps[activeStep]}
          disabled={!isValid || busy}
          activeStep={activeStep}
          numSteps={steps.length}
          nextHandler={handleNext}
          backHandler={handleBack}
          submitHandler={handleSubmit}
          closeHandler={handleClose}
        />
      </Box>
    </>
  );
}
