import React from "react";
import {
  Notifications as NotificationsIcon,
  InfoOutlined as InfoIcon,
  WarningOutlined as ErrorIcon,
} from "@mui/icons-material";
import {
  Menu,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemButton,
  IconButton,
  Paper,
  Tooltip,
  Button,
  Box,
} from "@mui/material";

export default function NotificationsPanel({ notifications }) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  return (
    <div>
      <Tooltip title="Alerts and Notifications">
        <IconButton
          // color="secondary"
          aria-label="add"
          aria-controls="customized-menu"
          aria-haspopup="true"
          onClick={(event) => setAnchorEl(event.currentTarget)}
        >
          <NotificationsIcon />
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
        elevation={0}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Paper sx={{ minWidth: "20rem" }}>
          <List component="nav" aria-label="main mailbox folders">
            <ListItemButton>
              <ListItemIcon>
                <InfoIcon />
              </ListItemIcon>
              <ListItemText
                primary="Deployment <id> running"
                secondary="3 minutes ago"
              />
            </ListItemButton>
            <ListItemButton>
              <ListItemIcon>
                <ErrorIcon />
              </ListItemIcon>
              <ListItemText
                primary="Deployment <id> failed"
                secondary="1 hour ago"
              />
            </ListItemButton>
          </List>
          <Box display="flex" justifyContent="flex-end">
            <Button size="small">Clear All</Button>
          </Box>
        </Paper>
      </Menu>
    </div>
  );
}
