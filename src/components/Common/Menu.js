import React from "react";
import { Menu } from "@mui/material";
import { styled } from "@mui/material/styles";

// Menu with subtle box shadow
const StyledMenu = styled((props) => <Menu elevation={0} {...props} />)(
  ({ theme }) => ({
    "& .MuiPaper-root": {
      borderRadius: 6,
      marginTop: theme.spacing(1),
      minWidth: 150,
      boxShadow: "rgba(0, 0, 0, 0.05) 0px 1px 2px 0px;",
      "& .MuiMenu-list": {
        padding: "4px 0",
      },
    },
  })
);

export default StyledMenu;
