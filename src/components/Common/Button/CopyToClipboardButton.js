import { useState } from "react";
import { IconButton, Tooltip } from "@mui/material";
import FileCopyOutlinedIcon from "@mui/icons-material/FileCopyOutlined";

export default function CopyToClipboardButton({
  text,
  fontSize = "0.85em",
  color = "primary",
  ...rest
}) {
  const [copied, setCopied] = useState(false);

  return (
    <Tooltip title={copied ? "Copied" : "Copy to clipboard"}>
      <IconButton
        size="small"
        onClick={(e) => {
          e.preventDefault();
          navigator.clipboard.writeText(text);
          setCopied(true);
          setTimeout(() => setCopied(false), 2000);
        }}
        {...rest}
      >
        <FileCopyOutlinedIcon style={{ fontSize: fontSize }} color={color} />
      </IconButton>
    </Tooltip>
  );
}
