import { Refresh as RefreshIcon } from "@mui/icons-material";
import ActionButton from "./ActionButton";

const RefreshButton = ({ onClick }) => (
  <ActionButton onClick={onClick} tooltipText="Refresh">
    <RefreshIcon />{" "}
  </ActionButton>
);

export default RefreshButton;
