import { IconButton, Tooltip } from "@mui/material";

import { styled } from "@mui/material/styles";

const StyledButton = styled(IconButton)(({ theme }) => ({
  color: theme.palette.primary.main,
  width: "2em",
  height: "2em",
  top: "0.2em",
  "&.Mui-disabled": {
    pointerEvents: "auto",
  },
}));

const ActionButton = ({ tooltipText, disabled, onClick, ...other }) => {
  const adjustedButtonProps = {
    disabled: disabled,
    component: disabled ? "div" : undefined,
    onClick: disabled ? undefined : onClick,
  };
  return (
    <Tooltip title={tooltipText}>
      <StyledButton {...other} {...adjustedButtonProps} />
    </Tooltip>
  );
};

export default ActionButton;
