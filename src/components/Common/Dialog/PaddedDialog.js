import { Dialog } from "@mui/material";
import { styled } from "@mui/material/styles";

// Custom dialog style
const PaddedDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(4),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(2),
  },
}));

export default PaddedDialog;
