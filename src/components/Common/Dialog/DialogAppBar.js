import {
  AppBar,
  Avatar,
  Box,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import { useTheme, styled } from "@mui/material/styles";

const CustomToolbar = styled(Toolbar)(({ theme }) => ({
  "& .MuiToolbar-root": {
    marginLeft: 2,
    paddingRight: 2,
  },
}));

const CustomAppBar = styled(AppBar)(({ theme }) => ({
  "& .MuiAppBar-root": {
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
}));

export default function DialogAppBar({ handleClose, title, icon, endText }) {
  const theme = useTheme();
  return (
    <AppBar
      sx={{
        position: "relative",
        backgroundColor: theme.palette.sidebar.background,
        color: "#402727",
        borderBottom: "1px solid #cfbba5",
      }}
    >
      <Toolbar disableGutters>
        <Avatar sx={{ width: "2rem", height: "2rem", marginLeft: 2 }}>
          {icon}
        </Avatar>
        <Typography variant={"h6"} sx={{ flex: 1, marginLeft: 2 }}>
          {title}
        </Typography>

        {endText && (
          <Box display="flex" mr={2}>
            {endText}
          </Box>
        )}

        <IconButton
          edge="end"
          color="inherit"
          onClick={handleClose}
          aria-label="close"
          sx={{ mr: 1 }}
        >
          <CloseIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
