import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

/**
 * The ProgressDialog is diplayed while the user is waiting for an action to complete.
 * It displays a dialog with a progress indicator and a message when the action is complete.
 */
const ProgressDialog = ({
  open,
  busy,
  title,
  busyText,
  doneText,
  handleClose,
}) => {
  return (
    <Dialog open={open} onClose={handleClose} fullWidth>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{busy ? busyText : doneText}</DialogContentText>
      </DialogContent>
      <DialogActions>
        {!busy && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              handleClose();
            }}
          >
            OK
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default ProgressDialog;
