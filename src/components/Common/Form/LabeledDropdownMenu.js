import { Box, FormControl, FormLabel, MenuItem, Select } from "@mui/material";

/**
 * The WorkspaceDropdownMenu component is a generic, reusable dropdown
 * that accepts options and a callback for when an option is selected.
 */
const LabeledDropdownMenu = ({
  label,
  selectedId,
  options,
  handleSelect,
  ...rest
}) => {
  const handleOptionClick = (event) => {
    const selectedOption = options.find(
      (option) => option.id === event.target.value
    );
    handleSelect(selectedOption);
  };

  return (
    <FormControl fullWidth>
      <Box pb={1}>
        <FormLabel color="secondary">{label}</FormLabel>
      </Box>
      <Select
        value={selectedId}
        onChange={handleOptionClick}
        size="small"
        {...rest}
      >
        {options?.map((option) => {
          return (
            <MenuItem key={option.id} value={option.id}>
              {option.name}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

export default LabeledDropdownMenu;
