import { Stack, Skeleton } from "@mui/material";

// Loading skeleton for form fields
export default function FormLoadingSkeleton({ rows }) {
  return (
    <Stack spacing={2}>
      {Array.from({ length: rows }).map((_, index) => (
        <Skeleton key={index} variant="rounded" height={55} animation="wave" />
      ))}
    </Stack>
  );
}
