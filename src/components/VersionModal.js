import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
} from "@mui/material";
import React from "react";
import { useAPI } from "../contexts";
import { useState, useEffect } from "react";
import axios from "axios";

export default function VersionDialog({ open, title, handleClose }) {
  const api = useAPI();
  const [cacaoInfo, setCacaoInfo] = useState({});
  const [gitInfo, setGitInfo] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get("/gitinfo").then((response) => {
      setGitInfo(response.data);
      setLoading(false);
    });

    api.version().then((response) => {
      if (response !== null) {
        setCacaoInfo(response);
      }
    });
  }, []);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      aria-labelledby="version-dialog"
    >
      <DialogTitle id="version-dialog">{title}</DialogTitle>
      <DialogContent>
        {!loading && (
          <>
            <Typography>Git Branch: {gitInfo.branch}</Typography>
            <Typography>Git Commit Hash: {gitInfo.hash}</Typography>
            <Typography>Git Commit Date: {gitInfo.date}</Typography>
            <Typography>
              Cacao Deployment Date: {cacaoInfo.execution_time}
            </Typography>
          </>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
}
