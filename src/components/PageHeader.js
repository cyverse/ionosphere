import React from "react";
import { Grid, Box, Breadcrumbs, Link, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { menuItems } from "../menuItems.js";

const BreadcrumbsMenu = ({ parts, title }) => {
  if (parts.length <= 1) return <></>;

  return (
    <Breadcrumbs sx={{ paddingTop: "1em" }}>
      {parts.slice(0, -1).map((part, index) => (
        <Link
          key={index}
          color="inherit"
          href={"/" + parts.slice(0, index + 1).join("/")}
          underline="hover"
        >
          {capitalize(part)}
        </Link>
      ))}
      <Typography color="textPrimary">
        {title ? title : capitalize(parts.slice(-1)[0])}
      </Typography>
    </Breadcrumbs>
  );
};

const PageHeader = (props) => {
  const router = useRouter();
  const menuItem = menuItems.find((item) => item.label === props.title);

  const parts = props.parts || router.asPath.split("/").filter((s) => s);
  const backUrl = "/" + parts.slice(0, -1).join("/");

  return (
    <Box mb={3} mt={1}>
      <Grid container justifyContent="space-between">
        <Grid item>
          {props.back ? (
            <Link color="inherit" href={backUrl}>
              Back
            </Link>
          ) : props.breadcrumbs ? (
            <BreadcrumbsMenu parts={parts} title={props.title} />
          ) : (
            <div
              style={{
                display: "flex",
                alignItems: "center",
                color: "#303030",
              }}
            >
              {/* {menuItem && menuItem.icon} */}
              <Typography nowrap="true" variant="h6">
                {props.title}
              </Typography>
            </div>
          )}
        </Grid>
        <Grid item>{props.actions}</Grid>
      </Grid>
    </Box>
  );
};

function capitalize(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
}

export default PageHeader;
