import { Grid, Skeleton } from "@mui/material";
import {
  CloudQueue as CloudOutlineIcon,
  DataUsage as ProjectIcon,
  VpnKey as CredentialIcon,
} from "@mui/icons-material";

import DropdownMenu from "@/components/DropdownMenu";
import { useCloudMenu, useUserRecents } from "@/contexts";

const CloudMenu = () => {
  const [, updateUserRecents] = useUserRecents();
  const {
    cloudOptions,
    projectOptions,
    credentialOptions,
    selectedCloud,
    selectedProject,
    selectedCredential,
    setSelectedCloud,
    setSelectedProject,
    setSelectedCredential,
    loadingCredentials,
    loadingProjects,
  } = useCloudMenu();

  return (
    <Grid container spacing={1} display="flex">
      <Grid item>
        <DropdownMenu
          label={selectedCloud ? selectedCloud.name : "Select Cloud"}
          icon={<CloudOutlineIcon />}
          header="Select Cloud"
          options={cloudOptions}
          onClick={(option) => {
            setSelectedCloud(option);
            updateUserRecents("cloud", option.id);
          }}
        />
      </Grid>
      {(loadingCredentials || loadingProjects) &&
        selectedCloud?.hasProjects && <LoadingMenu />}
      {!loadingCredentials &&
        !loadingProjects &&
        selectedCloud?.hasProjects && (
          <Grid item>
            <DropdownMenu
              label={selectedProject ? selectedProject.name : "Select Project"}
              header="Select Project"
              icon={<ProjectIcon />}
              options={projectOptions}
              onClick={(option) => {
                setSelectedProject(option);
                updateUserRecents(`project-${selectedCloud.id}`, option.name);
              }}
              enabled={selectedCloud.hasProjects}
            />
          </Grid>
        )}
      {(loadingCredentials || loadingProjects) &&
        !selectedCloud?.hasProjects && <LoadingMenu />}
      {!loadingCredentials &&
        !loadingProjects &&
        !selectedCloud?.hasProjects && (
          <Grid item>
            <DropdownMenu
              label={
                selectedCredential
                  ? selectedCredential.name
                  : "Select Credential"
              }
              header="Select Credential"
              icon={<CredentialIcon />}
              options={credentialOptions}
              onClick={(option) => {
                setSelectedCredential(option);
                updateUserRecents(`credential-${selectedCloud?.id}`, option.id);
              }}
            />
          </Grid>
        )}
    </Grid>
  );
};

export default CloudMenu;

const LoadingMenu = () => {
  return (
    <Grid item>
      <Skeleton variant="rounded" width={200} height={36} />
    </Grid>
  );
};
