import React from "react";
import { Box, Stack, Typography } from "@mui/material";
import { CheckCircleOutline as SuccessIcon } from "@mui/icons-material";
import { styled } from "@mui/material/styles";

const StyledBox = styled(Box)({
  padding: "2rem",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
});

const StyledSuccessIcon = styled(SuccessIcon)({
  fontSize: "4rem",
  color: "#4CAF50", // A green shade
  marginBottom: "1rem",
});

const Message = styled(Typography)({
  textAlign: "center",
});

const DeploymentConfirmation = () => {
  return (
    <div>
      <StyledBox elevation={3}>
        <Stack spacing={4} alignItems={"center"}>
          <Stack spacing={1} alignItems={"center"}>
            <StyledSuccessIcon />
            <Message variant="h6">Deployment submitted.</Message>
            <Typography>
              It may take several minutes for your deployment to be ready.
            </Typography>
          </Stack>
          <Typography>
            View the deployment status by clicking <b>View Deployments</b>. Or,
            close this dialog to return to where you were.
          </Typography>
        </Stack>
      </StyledBox>
    </div>
  );
};

export default DeploymentConfirmation;
