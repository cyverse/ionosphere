import { useRouter } from "next/router";
import { TextSnippet as LogsIcon } from "@mui/icons-material";
import ActionButton from "@/components/Common/Button/ActionButton";

const LogsButton = ({ deploymentId, runId }) => {
  const router = useRouter();
  const navigateToLogs = ({ deploymentId, runId }) => {
    router.push({
      pathname: `/deployments/${deploymentId}/logs`,
    });
  };
  return (
    <ActionButton
      onClick={() => navigateToLogs({ deploymentId, runId })}
      tooltipText={"View logs"}
    >
      <LogsIcon />
    </ActionButton>
  );
};

export default LogsButton;
