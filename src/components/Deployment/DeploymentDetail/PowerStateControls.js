import { useState } from "react";

import { Box } from "@mui/material";

import {
  Stop as StopIcon,
  PowerSettingsNew as PowerIcon,
  PlayArrow as PlayIcon,
} from "@mui/icons-material";

import { useAPI } from "@/contexts";
import ShelveConfirmationDialog from "@/components/ShelveConfirmationDialog";
import ActionButton from "@/components/Common/Button/ActionButton";

export default function PowerStateControls({
  deployment,
  run,
  setBusy,
  handleError,
}) {
  const api = useAPI();
  const [shelveDialog, setShelveDialog] = useState(false);

  const powerState = run.parameters.find((p) => p.key === "power_state")?.value;

  const handleUpdatedDeploymentState = async (newState) => {
    setBusy(true);
    const parameters = JSON.parse(JSON.stringify(run.parameters)); // deep copy
    const powerStateParam = parameters.find((p) => p.key === "power_state");
    powerStateParam.value = newState;

    try {
      const res = await api.createDeploymentRun(deployment.id, {
        deployment_id: deployment.id,
        parameters,
      });
      setBusy(false);
    } catch (error) {
      handleError(error);
    }
  };

  // shelving/unshelving confirmation dialog
  const handleShelve = () => {
    handleUpdatedDeploymentState("shelved_offloaded");
  };

  const handleActive = () => {
    handleUpdatedDeploymentState("active");
  };

  const isActive = (deployment) => {
    let powerState = deployment.parameters.find(
      (p) => p.key === "power_state"
    )?.value;

    if (
      deployment.current_status === "creation_errored" ||
      (deployment.pending_status === "none" &&
        (powerState === "shelved_offloaded" || powerState === "shutoff"))
    )
      return false;
    return true;
  };

  const disableControls = () => {
    return (
      deployment.current_status === "deleted" ||
      deployment.pending_status === "creating" ||
      deployment.pending_status === "deleting"
    );
  };

  return (
    <>
      {isActive(deployment) ? (
        <Box display="flex">
          <ActionButton
            onClick={() => setShelveDialog(true)}
            disabled={disableControls()}
            tooltipText={"Shelve"}
          >
            <StopIcon />
          </ActionButton>
          <ShelveConfirmationDialog
            open={shelveDialog}
            title="Shelve"
            handleClose={() => setShelveDialog(false)}
            handleSubmit={handleShelve}
            infoText={
              "Shelving your virtual machine(s) will turn them off and preserve their data. A shelved deployment continues to use storage allocation but doesn't consume any compute allocation."
            }
            confirmText={"Shelve"}
          />
          <ActionButton
            onClick={() => handleUpdatedDeploymentState("shutoff")}
            disabled={disableControls()}
            tooltipText={"Shutdown"}
          >
            <PowerIcon />
          </ActionButton>
        </Box>
      ) : (
        <Box>
          <ActionButton
            onClick={() => setShelveDialog(true)}
            disabled={disableControls()}
            tooltipText={
              powerState === "shelved_offloaded" ? "Unshelve" : "Start"
            }
          >
            <PlayIcon />
          </ActionButton>
          <ShelveConfirmationDialog
            open={shelveDialog}
            title="Unshelve Deployment"
            handleClose={() => setShelveDialog(false)}
            handleSubmit={handleActive}
            infoText={
              "Unshelving will restart the virtual machine(s), which will resume the consumption of compute resources."
            }
            confirmText={"Unshelve"}
          />
        </Box>
      )}
    </>
  );
}
