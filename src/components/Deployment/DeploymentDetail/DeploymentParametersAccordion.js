import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { AccordionTable } from "@/components/Common";

// Component to display deployment parameters in an accordion
const DeploymentParametersAccordion = ({ parameters }) => {
  return (
    <AccordionTable title={"View Deployment Parameters"}>
      <TableContainer>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Parameter Name</TableCell>
              <TableCell>Value</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {parameters
              .filter((p) => p.key !== "user_data")
              .sort((a, b) => a.key.localeCompare(b.key))
              .map((p) => (
                <TableRow key={p.key}>
                  <TableCell>{p.key}</TableCell>
                  <TableCell>{p.value}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </AccordionTable>
  );
};

export default DeploymentParametersAccordion;
