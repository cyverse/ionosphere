import LinearProgress from "@mui/material/LinearProgress";
import { styled } from "@mui/material/styles";

export const ErrorLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 5,
  borderRadius: 5,
  "& .MuiLinearProgress-bar": {
    backgroundColor: theme.palette.error.main,
  },
}));
export const StandardLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 5,
  borderRadius: 5,
}));
