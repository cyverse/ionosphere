import React from "react";
import { Dialog, DialogContent, Button, Typography, Box } from "@mui/material";
import CloudCredentialNotice from "./Workspace/CloudCredentialNotice";

export default function CredentialModal(props) {
  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby="credential-modal-title"
      aria-describedby="credential-modal-description"
    >
      <DialogContent dividers>
        <CloudCredentialNotice />
      </DialogContent>
    </Dialog>
  );
}
