import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";

export default function ConfirmationDialog({
  open,
  title,
  handleClose,
  handleSubmit,
}) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>Are you sure?</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button
          color="error"
          variant="contained"
          onClick={() => {
            handleClose();
            handleSubmit();
          }}
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
