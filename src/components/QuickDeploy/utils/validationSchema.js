import * as yup from "yup";

/**
 * Generates a Yup validation schema for a given field.
 *
 * This function takes a field object with properties derived from CACAO
 * template.metadata & template.ui_metadata.
 *
 * @param {Object} field - An object representing a form field.
 * @returns {Object} - A Yup validation schema object for the provided field.
 */
const createYupSchema = (field) => {
  const { name, type, required } = field;

  // Switch case to handle different field types
  switch (type) {
    case "string":
      let stringSchema = yup.string();
      if (required) {
        stringSchema = stringSchema.required("Required");
      }
      // Add additional validation for instance_name
      if (name === "instance_name") {
        stringSchema = stringSchema
          .max(32, "Instance name must be 32 characters or less")
          .test(
            "is-first-char-alpha",
            "The first character must be a lowercase letter",
            (value) => {
              if (typeof value !== "string" || value.length === 0) {
                return false;
              }
              const firstChar = value.charAt(0);
              // Check if the first character is NOT a letter
              return /^[a-zA-Z]$/.test(firstChar);
            }
          )
          .matches(
            /^[a-z][a-z0-9-]*$/,
            "Can only contain lowercase letters, numbers, and hyphens"
          );
      }
      return stringSchema;

    case "integer":
      let numberSchema = yup.number();
      if (required) {
        numberSchema = numberSchema.required("Required");
      }
      if (field.min) {
        numberSchema = numberSchema.min(
          field.min,
          `Must be greater than ${field.min}`
        );
      }
      if (field.max) {
        numberSchema = numberSchema.max(
          field.max,
          `Must be less than ${field.max}`
        );
      }
      return numberSchema;

    case "cacao_provider_image_name":
      let imageSchema = yup.string();
      if (required) {
        imageSchema = imageSchema.required("Required");
      }
      return imageSchema;

    case "cacao_provider_flavor":
      let flavorSchema = yup.string();
      if (required) {
        flavorSchema = flavorSchema.required("Required");
      }
      return flavorSchema;

    default:
      return null;
  }
};

/**
 * Generates a complete Yup object schema based on an array of field objects.
 * This function iterates over an array of field objects, and for each object,
 * it uses the createYupSchema function to generate a Yup schema. These individual
 * field schemas are then combined into a single Yup object schema.
 *
 * @param {Array} fields - An array of objects, each representing a field with properties like name, type, and required.
 * @returns {Object} - A Yup object schema made up of the generated field schemas.
 */
const generateYupSchema = (fields) => {
  return fields.reduce((yupSchema, field) => {
    const { name } = field;

    // This creates a Yup schema entry based on the properties of the field
    yupSchema[name] = createYupSchema(field);
    return yupSchema;
  }, {});
};

export default generateYupSchema;
