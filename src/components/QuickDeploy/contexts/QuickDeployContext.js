import { createContext, useContext, useEffect, useReducer } from "react";
import { object } from "yup";

import quickDeployReducer from "../reducers/quickDeployReducer";
import {
  setFormFields,
  setTemplate,
  setValidationSchema,
} from "../actions/actions";
import { getFormFieldsFromTemplate } from "../utils/initializeForm";
import generateYupSchema from "../utils/validationSchema";
import { useCloudMenuForm } from "@/contexts/cloudMenuForm";

export const QuickDeployContext = createContext(null);
export const QuickDeployDispatchContext = createContext(null);

/**
 * QuickDeployProvider is a React Context that provides the current
 * state for Quick Deploy.
 *
 * Quick Deploy wizard state is managed with a React reducer.
 *
 * @param {Object} value (see below for params)
 * @param {JSX.Element} children
 * @returns Deployment wizard state
 */

export function QuickDeployProvider({ children, initialValues }) {
  // initialize state from initialValues prop, or use defaults
  const {
    template = null,
    regionId = "",
    formValues = {},
    templates = {},
  } = initialValues;

  const initialState = {
    template,
    templates,
    regionId,
    formValues,
  };

  // initialize QuickDeploy reducer
  const [state, dispatch] = useReducer(quickDeployReducer, initialState);
  const { cloudMenu } = useCloudMenuForm();

  /**
   * When a template is selected, generate the list of form fields &
   * yup validation schema & update Quick Deploy state.
   */
  useEffect(() => {
    if (state.template) {
      const fields = getFormFieldsFromTemplate(state.template);
      const schema = generateYupSchema(fields);

      dispatch(setValidationSchema(object().shape(schema)));
      dispatch(setFormFields(fields));
    }
  }, [state.template]);

  useEffect(() => {
    if (cloudMenu.cloud) {
      dispatch(setTemplate(state.templates[cloudMenu.cloud.type]));
    }
  }, [cloudMenu.cloud]);

  return (
    <QuickDeployContext.Provider value={{ ...state }}>
      <QuickDeployDispatchContext.Provider value={dispatch}>
        {children}
      </QuickDeployDispatchContext.Provider>
    </QuickDeployContext.Provider>
  );
}

/**
 * React hook to access Quick Deploy wizard state.
 */
export function useQuickDeploy() {
  return useContext(QuickDeployContext);
}

/**
 * React hook to access dispatch function for updating
 * Quick Deploy wizard state.
 */
export function useQuickDeployDispatch() {
  return useContext(QuickDeployDispatchContext);
}
