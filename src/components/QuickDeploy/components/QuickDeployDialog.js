/**
 * QuickDeployDialog Component
 * ---------------------------
 *
 * This Dialog component encapsulates the QuickDeploy process, which allows
 * users to deploy the openstack-single-image template in as few as two
 * clicks using default param values. The form fields and validation schema are
 * generated from `template.metadata` & `template.ui_metadata`, so this can
 * be extended to support more templates in the future.
 *
 * The dialog uses the `QuickDeployProvider` context to manage
 * deployment-related state. The pre-selected default values can be configured
 * in the utils/initializeForm.js file but can eventually be pulled from user
 * preferences.
 *
 * The QuickDeploy context is initialized using the provided `cloudId` and
 * `projectId` props or defaults to the first available options.
 *
 * Props:
 * - `open`: Boolean indicating if the dialog is open.
 * - `handleClose`: Function to call when the dialog should be closed.
 * - `template`: CACAO template (currently always openstack-single-image).
 * - `cloudId`: The ID of the cloud provider where deployment will be launched.
 * - `projectId`: The ID of the JS2 project where deployment will be launched.
 *
 * The `QuickDeploySteps` component, nested within the provider, orchestrates
 * the step-by-step deployment process.
 */

import IconDialogTitle from "@/components/Common/Dialog/IconDialogTitle";
import PaddedDialog from "@/components/Common/Dialog/PaddedDialog";

import { QuickDeployProvider } from "../contexts/QuickDeployContext";
import { CloudMenuFormProvider } from "@/contexts/cloudMenuForm";
import { QuickDeployIcon } from "./QuickDeployAvatar";
import QuickDeploySteps from "./QuickDeploySteps";

export default function QuickDeployDialog({ open, handleClose, templates }) {
  return (
    <PaddedDialog
      open={open}
      maxWidth="sm"
      fullWidth
      aria-labelledby="quick-deploy-dialog"
      PaperProps={{
        sx: {
          minHeight: "500px",
        },
      }}
    >
      <IconDialogTitle
        title="Quick Deploy"
        handleClose={handleClose}
        icon={<QuickDeployIcon />}
      ></IconDialogTitle>
      <CloudMenuFormProvider>
        <QuickDeployProvider
          initialValues={{
            templates: templates,
          }}
        >
          <QuickDeploySteps handleClose={handleClose} />
        </QuickDeployProvider>
      </CloudMenuFormProvider>
    </PaddedDialog>
  );
}
