import { Box, Stack } from "@mui/material";

import {
  FlavorField,
  ImageField,
  IntegerField,
  ShortTextField,
  SelectField,
} from "../fields";
import {
  useQuickDeploy,
  useQuickDeployDispatch,
} from "../contexts/QuickDeployContext";
import { setRegionId } from "../actions/actions";

/**
 * Renders a set of dynamic fields for the Quick Deploy form, based on the provided field list.
 * It maps each field type to a specific component, providing the necessary data for each field.
 *
 * @param {Array} props.fieldList - Array of field objects to render, each object includes field type and other properties.
 * @param {Array} props.images - Array of image options for the 'cacao_provider_image_name' field type.
 * @param {Array} props.flavors - Array of flavor options for the 'cacao_provider_flavor' field type.
 * @returns {JSX.Element} - A stack of fields rendered according to their types.
 */
export default function QuickDeployFields({
  fieldList,
  images,
  flavors,
  regions,
}) {
  const deployment = useQuickDeploy();
  const dispatch = useQuickDeployDispatch();

  const renderField = (field) => {
    switch (field.type) {
      case "cacao_provider_image_name":
        return (
          <ImageField field={field} images={images} submitValueType={"name"} />
        );
      case "cacao_provider_image":
        return (
          <ImageField field={field} images={images} submitValueType={"id"} />
        );
      case "cacao_provider_flavor":
        return <FlavorField field={field} flavors={flavors} />;
      case "cacao_provider_region":
        return (
          <SelectField
            field={field}
            options={regions}
            value={deployment.regionId}
            handleChange={(e) => dispatch(setRegionId(e.target.value))}
          />
        );
      case "integer":
        return <IntegerField field={field} />;
      case "string":
        return <ShortTextField field={field} />;
      default:
        return <>{field.name}</>;
    }
  };

  return (
    <Stack spacing={2}>
      {fieldList.map((field, index) => {
        // power_state is only rendered when editing an existing deployment
        if (field.name !== "power_state") {
          return <Box key={index}>{renderField(field)}</Box>;
        }
      })}
    </Stack>
  );
}
