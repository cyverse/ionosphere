/**
 * CloudMenu Component
 * -----------------------
 *
 * This component provides a user interface for selecting a cloud, project,
 * and region from dropdown menus. Dropdown state is managed by the QuickDeploy
 * context.
 *
 * Region defaults to IU.
 *
 * Skeleton screens are displayed while the data is loading, providing a smoother
 * user experience.
 */

import { Box, CircularProgress, Grid, Skeleton } from "@mui/material";

import {
  setCloud,
  setProject,
  setCredentialId,
} from "@/contexts/cloudMenuForm";
import LabeledDropdownMenu from "@/components/Common/Form/LabeledDropdownMenu";
import { useCloudMenuForm } from "@/contexts/cloudMenuForm";
import { useQuickDeployDispatch } from "../contexts/QuickDeployContext";
import { setRegionId } from "../actions/actions";

export default function CloudMenu({ setInitialFormValues }) {
  const {
    cloudMenu,
    cloudMenuDispatch,
    cloudOptions,
    projectOptions,
    loadingProjects,
    credentialOptions,
    loadingCredentials,
  } = useCloudMenuForm();

  const quickDeployDispatch = useQuickDeployDispatch();
  const showProjects = cloudMenu.cloud?.hasProjects;
  const showCredentials =
    (!cloudMenu.cloud?.hasProjects ||
      (cloudMenu.cloud?.hasProjects && credentialOptions.length > 1)) &&
    cloudMenu.credentialId;

  const projectsWidth = showProjects && !showCredentials ? 8 : 4;
  const credentialsWidth = showProjects && showCredentials ? 4 : 8;

  return (
    <>
      {!!credentialOptions.length ? (
        <Grid container spacing={2}>
          <Grid item sm={4} xs={12}>
            <LabeledDropdownMenu
              label={"Cloud"}
              selectedId={cloudMenu.cloud?.id}
              options={cloudOptions}
              handleSelect={(selected) => {
                setInitialFormValues({});
                cloudMenuDispatch(setCredentialId(null));
                cloudMenuDispatch(setProject(null));
                quickDeployDispatch(setRegionId(""));
                cloudMenuDispatch(setCloud(selected));
              }}
            />
          </Grid>
          {loadingProjects || loadingCredentials ? (
            <Grid
              item
              xs={8}
              style={{ display: "flex", justifyContent: "flex-start" }}
            >
              <Box
                m={2}
                display="flex"
                justifyContent="flex-start"
                alignItems="left"
              >
                <CircularProgress color="primary" />
              </Box>
            </Grid>
          ) : (
            <>
              {showProjects && (
                <Grid item sm={projectsWidth} xs={12}>
                  <LabeledDropdownMenu
                    label={"Project"}
                    selectedId={cloudMenu.project?.id}
                    options={projectOptions.map((p) => {
                      return { id: p.id, name: p.name };
                    })}
                    handleSelect={(selected) => {
                      cloudMenuDispatch(setCredentialId(null));
                      cloudMenuDispatch(setProject(selected));
                    }}
                  />
                </Grid>
              )}
              {showCredentials && (
                <Grid item sm={credentialsWidth} xs={12}>
                  <LabeledDropdownMenu
                    label={"Credential"}
                    selectedId={cloudMenu.credentialId}
                    options={credentialOptions?.map((c) => {
                      return { id: c.id, name: c.name };
                    })}
                    handleSelect={(selected) =>
                      cloudMenuDispatch(setCredentialId(selected.id))
                    }
                  />
                </Grid>
              )}
            </>
          )}
        </Grid>
      ) : (
        <Box spacing={2}>
          <Skeleton variant="rounded" height={75} animation="wave" />
        </Box>
      )}
    </>
  );
}
