import styled from "@emotion/styled";
import { Avatar, IconButton } from "@mui/material";
import { Bolt } from "@mui/icons-material";

// Set icon for QuickDeploy
const QUICK_DEPLOY_ICON = <Bolt />;

// Custom avatar for QuickDeploy
const GradientAvatar = styled(Avatar)({
  // orange gradient background
  background: "linear-gradient(45deg, #FFA726, #FF5722)",
  // white text
  color: "#ffffff",
});

// ----- QuickDeploy Avatar Components ----- //

const QuickDeployIcon = () => {
  return <GradientAvatar>{QUICK_DEPLOY_ICON}</GradientAvatar>;
};

const QuickDeployButton = ({ onClick }) => {
  return (
    <IconButton aria-label="Quick Deploy" onClick={onClick}>
      <QuickDeployIcon />
    </IconButton>
  );
};

export { QuickDeployIcon, QuickDeployButton };
