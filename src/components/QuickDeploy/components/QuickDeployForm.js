import {
  Box,
  Button,
  DialogActions,
  DialogContent,
  Stack,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";

import { useState, useEffect, useRef } from "react";
import { useFormik } from "formik";
import getConfig from "next/config";

import {
  useAPI,
  useError,
  useUser,
  useUserConfig,
  useUserRecents,
  useWorkspaces,
} from "@/contexts";
import { useImages, useFlavors, useRegions } from "@/hooks";
import { filterFlavorsByMinValues } from "@/utils";

import {
  useQuickDeploy,
  useQuickDeployDispatch,
} from "../contexts/QuickDeployContext";
import { FormikContextProvider } from "../contexts/FormikContext";
import { initializeFormValuesFromTemplate } from "../utils/initializeForm";
import CloudMenu from "./CloudMenu";
import QuickDeployFields from "./QuickDeployFields";
import FormLoadingSkeleton from "@/components/Common/Form/FormLoadingSkeleton";
import { setRegionId } from "../actions/actions";
import { useCloudMenuForm } from "@/contexts/cloudMenuForm";

/**
 * The QuickDeployForm component renders the <form> for a CACAO template.
 *
 * It employs Formik for form state management and validation, using a custom Formik Context
 * to share formik state across the DialogContent and DialogActions. More information can be found
 * in the FormikContext.js file.
 *
 * The form and validation schema are dynamically built from the template.
 *
 * Default form values are initialized once images and flavors have been fetched from the API using the
 * useImages and useFlavors hooks.
 *
 * @param {Function} setActiveStep - Function to control the active step in a multi-step process.
 * @param {number} startStep - Initial step for the form in a multi-step process.
 */
const QuickDeployForm = ({ setActiveStep, startStep }) => {
  // Track mounted state to avoid memory leaks when component is unmounted
  const isMounted = useRef(null);

  const api = useAPI();
  const [user] = useUser();
  const config = getConfig().publicRuntimeConfig;
  const [workspaces] = useWorkspaces();
  const deployment = useQuickDeploy();
  const { cloudMenu } = useCloudMenuForm();
  const dispatch = useQuickDeployDispatch();
  const [userConfig] = useUserConfig();
  const [userRecents, updateUserRecents] = useUserRecents();

  const [loading, setLoading] = useState(false);
  const [initialFormValues, setInitialFormValues] = useState({});
  const [imageFlavors, setImageFlavors] = useState([]);
  const [, setError] = useError();

  const [regions, loadingRegions] = useRegions(
    cloudMenu.cloud?.id,
    cloudMenu.credentialId
  );

  // Once regions are loaded, set the default region to default
  useEffect(() => {
    if (regions.length) {
      let defaultRegion = null;
      const cloudType = cloudMenu.cloud?.type;
      if (cloudType === "openstack") {
        defaultRegion = regions.find((r) => r.id === "IU") || regions[0];
      } else if (cloudType === "aws") {
        defaultRegion =
          regions.find((r) => r.id === userConfig?.aws_default_region) ||
          regions[0];
      }
      dispatch(setRegionId(defaultRegion?.id));
    }
  }, [regions]);

  // Fetch images and flavors using custom hooks
  const { images, loadingImages, imageError } = useImages(
    cloudMenu.cloud?.id,
    cloudMenu.credentialId,
    deployment.regionId
  );
  const { flavors, loadingFlavors, flavorError } = useFlavors(
    cloudMenu.cloud?.id,
    cloudMenu.credentialId,
    deployment.regionId
  );

  // Initialize form values once images and flavors have been fetched
  useEffect(() => {
    if (images.length && flavors.length) {
      const initialFormValues = initializeFormValuesFromTemplate(
        deployment.template,
        images,
        flavors
      );
      if (isMounted.current) {
        setInitialFormValues(initialFormValues);
      }
    }
  }, [images, flavors]);

  /**
   * Submit handler for Formik form. Builds params for deployment and calls launchDeployment,
   * which performs the API calls to create the deployment and run.
   * @param {Object} values Formik values
   */
  const handleSubmit = (values) => {
    const [deploymentParams, runParams] = setupParams(values);
    if (!(config.SIMULATE_DEPLOYMENT_SUBMIT === "true")) {
      launchDeployment(deploymentParams, runParams);
    } else {
      console.log("Deployment Params: ", deploymentParams);
      console.log("Run Params: ", runParams);
    }
  };

  // Initialize Formik state
  const formik = useFormik({
    initialValues: initialFormValues,
    onSubmit: handleSubmit,
    enableReinitialize: true,
    validationSchema: deployment.validationSchema,
  });

  // if image_name changes, filter flavors by min_disk and min_ram and set flavor to first in list
  // TODO: update to get name of image field by type (cacao_provider_image_name or cacao_provider_image)
  useEffect(() => {
    if (
      (formik.values.image_name || formik.values.image) &&
      images.length &&
      flavors.length
    ) {
      const image = images.find(
        (image) =>
          image.name === formik.values.image_name ||
          image.id === formik.values.image
      );
      let filteredFlavors = filterFlavorsByMinValues(
        flavors,
        image?.min_disk,
        image?.min_ram
      );
      // update state only if component is still mounted
      if (isMounted.current) {
        setImageFlavors(filteredFlavors);
        formik.setFieldValue("flavor", filteredFlavors[0].name);
      }
    }
  }, [formik.values.image_name, formik.values.image, flavors]);

  // avoids memory leak when component is unmounted
  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);

  // ****************** submit functions ****************** //

  /**
   * Build params for deployment and run from QuickDeploy context and formik values.
   * @param {Object} formikValues
   * @returns {Array} [deploymentParams, runParams]
   */
  const setupParams = (formikValues) => {
    const workspaceId = workspaces.find(
      (w) =>
        w.default_provider_id === cloudMenu.cloud?.id &&
        w.name === "Default Workspace"
    ).id;

    // setup deployment params
    let deploymentParams = {
      owner: user.username,
      name: formikValues.instance_name,
      workspace_id: workspaceId,
      template_id: deployment.template.id,
      primary_provider_id: cloudMenu.cloud?.id,
      cloud_credentials: [cloudMenu.credentialId],
    };

    // setup run params
    let runParams = [{ key: "username", value: user.username }];

    const projectParam = deployment.template.metadata.parameters.find(
      (p) => p.type === "cacao_provider_project"
    );

    if (projectParam) {
      runParams.push({
        key: projectParam.name,
        value: cloudMenu.project?.name,
      });
    }

    deployment.fieldList.forEach((f) => {
      if (f.type === "cacao_provider_region") {
        runParams.push({
          key: f.name,
          value: deployment.regionId,
        });
        return;
      }
      runParams.push({
        key: f.name,
        value: formikValues[f.name].toString(),
      });
    });

    return [deploymentParams, runParams];
  };

  /**
   * Makes API calls to create deployment and run and navigates to success
   * page upon success.
   * @param {Object} deploymentParams
   * @param {Array} runParams
   */
  const launchDeployment = async (deploymentParams, runParams) => {
    setLoading(true);

    try {
      // create deployment
      const res = await api.createDeployment(deploymentParams);

      // create run
      const res2 = await api.createDeploymentRun(res.tid, {
        deployment_id: res.tid,
        parameters: runParams,
      });

      setActiveStep(2);
      updateRecents(deploymentParams, runParams);
    } catch (error) {
      console.log(error);
      setError(api.errorMessage(error));
    } finally {
      setLoading(false);
    }
  };

  /**
   * Update user recent cloud/project/credential after deployment is successfully created.
   */
  const updateRecents = (deploymentParams, runParams) => {
    try {
      const cloudId = deploymentParams.primary_provider_id;

      if (userRecents.cloud !== cloudId) {
        updateUserRecents("cloud", cloudId);
      }

      const projectParam = runParams.find((p) => p.key === "project");

      if (
        projectParam &&
        userRecents[`project-${cloudId}`] !== projectParam.value
      ) {
        updateUserRecents(`project-${cloudId}`, projectParam.value);
      }

      if (
        userRecents[`credential-${cloudId}`] !==
        deploymentParams.cloud_credentials[0]
      ) {
        updateUserRecents(
          `credential-${cloudId}`,
          deploymentParams.cloud_credentials[0]
        );
      }
    } catch (error) {
      console.error("Error updating user recents:", error);
    }
  };

  // Check if we have everything needed to render form
  const formValuesInitialized =
    !loadingFlavors &&
    !loadingImages &&
    !loadingRegions &&
    Object.keys(initialFormValues).length > 0 &&
    deployment.fieldList &&
    deployment.validationSchema;

  return (
    <FormikContextProvider value={formik}>
      <DialogContent>
        <Stack spacing={2}>
          <Box marginBottom={2}>
            <CloudMenu setInitialFormValues={setInitialFormValues}></CloudMenu>
          </Box>
          {formValuesInitialized ? (
            <form onSubmit={formik.handleSubmit}>
              <QuickDeployFields
                fieldList={deployment.fieldList}
                images={images}
                flavors={imageFlavors}
                regions={regions}
              />
            </form>
          ) : (
            <FormLoadingSkeleton rows={5} />
          )}
        </Stack>
      </DialogContent>
      <DialogActions>
        {startStep.current === 0 && (
          <Button onClick={() => setActiveStep(0)}>Back</Button>
        )}
        <LoadingButton
          variant="contained"
          onClick={formik.submitForm}
          loading={loading}
        >
          Launch
        </LoadingButton>
      </DialogActions>
    </FormikContextProvider>
  );
};

export default QuickDeployForm;
