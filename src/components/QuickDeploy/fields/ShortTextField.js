import { FormControl, TextField } from "@mui/material";
import { useContext } from "react";
import { FormikContext } from "../contexts/FormikContext";

export default function ShortTextField({ field }) {
  const formik = useContext(FormikContext);

  return (
    <FormControl variant="outlined" fullWidth>
      <TextField
        id={field.name}
        name={field.name}
        label={field.ui_label}
        required={field.required}
        value={formik.values[field.name] || ""}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        error={!!formik.errors[field.name]}
        helperText={formik.errors[field.name]}
        variant="outlined"
        inputProps={{
          maxLength: field.max_length ? field.max_length : undefined,
        }}
      />
    </FormControl>
  );
}
