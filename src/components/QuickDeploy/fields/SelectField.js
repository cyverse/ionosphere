import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { useContext } from "react";
import { FormikContext } from "../contexts/FormikContext";

const SelectField = ({ field, options, value, handleChange }) => {
  const formik = useContext(FormikContext);

  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel>{field.ui_label}</InputLabel>
      <Select
        id={field.name}
        name={field.name}
        label={field.ui_label}
        value={value ? value : formik.values[field.name]}
        onChange={handleChange ? handleChange : formik.handleChange}
        renderValue={(option) => option}
      >
        {options?.map((option, index) => (
          <MenuItem key={index} value={option.id}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default SelectField;
