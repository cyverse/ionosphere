/**
 * Reducer File
 * ------------
 * This file contains the reducer for the Quick Deploy dialog. A reducer is a function
 * that takes the current state and an action, and returns a new state based on the action type.
 *
 * Reducers specify how the application's state changes in response to actions received.
 * Actions only describe what happened, but don't describe how the application's state changes.
 *
 * Reducers should should not modify the input state. Instead, they should return a new state
 * object if changes are necessary.
 */

import {
  SET_REGION_ID,
  SET_IMAGES,
  SET_FLAVORS,
  SET_FIELD_LIST,
  SET_INITIAL_FORM_VALUES,
  SET_VALIDATION_SCHEMA,
  SET_TEMPLATE,
} from "../actions/actionTypes";

function deploymentReducer(state, action) {
  switch (action.type) {
    case SET_REGION_ID:
      return { ...state, regionId: action.payload };
    case SET_IMAGES:
      return { ...state, images: action.payload };
    case SET_FLAVORS:
      return { ...state, flavors: action.payload };
    case SET_FIELD_LIST:
      return { ...state, fieldList: action.payload };
    case SET_VALIDATION_SCHEMA:
      return { ...state, validationSchema: action.payload };
    case SET_INITIAL_FORM_VALUES:
      return {
        ...state,
        formValues: action.payload,
      };
    case SET_TEMPLATE:
      return {
        ...state,
        template: action.payload,
      };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

export default deploymentReducer;
