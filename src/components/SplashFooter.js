import React from "react";
import Image from "next/image";
import { Box, Grid, Button, Typography, Link } from "@mui/material";
import SvgJetstreamLogo from "./icons/JetstreamLogo";
import CyverseLogo from "./icons/cyverseLogo";
import AiiraLogo from "./icons/aiiraLogo";

export default function SplashFooter() {
  return (
    <>
      {/* FOOTER LINKS FOR POLICIES, FAQ, ETC */}
      <Grid
        container
        spacing={3}
        flexDirection={"row"}
        justifyContent={"center"}
        alignItems={"center"}
        alignContent={"center"}
        sx={{ marginBottom: "4rem" }}
      >
        <Grid item>
          <Button
            size="small"
            variant="text"
            href="https://docs.jetstream-cloud.org/general/policies/"
            target="_blank"
          >
            Policies
          </Button>
        </Grid>
        <Grid item>
          <Button
            href="https://docs.jetstream-cloud.org/"
            target="_blank"
            size="small"
            variant="text"
          >
            Documentation
          </Button>
        </Grid>
        <Grid item>
          <Button
            href="https://docs.jetstream-cloud.org/faq/general-faq/#how-do-i-cite-jetstream2"
            target="_blank"
            size="small"
            variant="text"
          >
            Cite Us
          </Button>
        </Grid>
      </Grid>
      {/* LOGO ROW */}
      <Grid
        container
        justifyContent={"center"}
        alignContent={"center"}
        alignItems={"center"}
        spacing={3}
      >
        <Grid item xs={12} sm={12} md={3} lg={2} xl={2}>
          <Box
            display="flex"
            alignItems="center"
            alignContent={"center"}
            justifyContent="center"
          >
            <Link href="https://www.cyverse.org/" target="_blank">
              <CyverseLogo width="150px" />
            </Link>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={3} lg={2} xl={2}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Link href="https://jetstream-cloud.org/" target="_blank">
              <SvgJetstreamLogo width="120px" />
            </Link>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={3} lg={2} xl={2}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Box
              component={"div"}
              sx={{ minHeight: "50px", minWidth: "200px" }}
            >
              <Link href="https://aiira.iastate.edu/" target="_blank">
                <AiiraLogo width="200px"></AiiraLogo>
              </Link>
            </Box>
          </Box>
        </Grid>

        <Grid item xs={12} sm={12} md={3} lg={2} xl={2}>
          <Box display="flex" alignItems="center" justifyContent="center">
            <Box
              component={"div"}
              sx={{ minHeight: "50px", minWidth: "100px", marginLeft: "10px" }}
            >
              <Link href="https://coalesce.io/" target="_blank">
                <Image
                  src="/images/coalesceLogo.png"
                  width="250px"
                  height="50px"
                />
              </Link>
            </Box>
          </Box>
        </Grid>
      </Grid>
      {/* GRANT TEXT */}
      <Grid
        container
        flexDirection="column"
        justifyContent={"center"}
        alignItems={"center"}
        alignContent={"center"}
        mt="2rem"
      >
        <Grid item>
          <Typography sx={{ fontSize: ".6em" }} textAlign={"center"}>
            This material is based upon work supported by the National Science
            Foundation <br /> under Grant Nos. OCI-2005506, DBI-1743442,
            USDA/NSF 2021-67021-35329, and CNS-1954556
          </Typography>
        </Grid>
        <Grid item>
          <Typography
            variant="body2"
            textAlign="center"
            color="textSecondary"
            sx={{ marginTop: "1rem" }}
          >
            {"Copyright © "}
            CACAO {new Date().getFullYear()}
            {"."}
          </Typography>
        </Grid>
      </Grid>
    </>
  );
}
