import { useEffect, useMemo } from "react";
import * as Sentry from "@sentry/nextjs";
import { Box, Grid } from "@mui/material";

import { Layout } from "../components";
import { useCredentials, useEvents } from "@/contexts";

import { CloudMenuProvider } from "@/contexts/cloudMenu";
import { DeploymentsProvider, useDeployments } from "@/contexts/deployments";

import CloudMenu from "@/components/Deployment/CloudMenu";
import WorkspaceList from "@/components/Workspace/WorkspaceList";
import CloudCredentialNotice from "@/components/Workspace/CloudCredentialNotice";

const Deployments = (props) => {
  return (
    <CloudMenuProvider>
      <DeploymentsProvider
        deployments={props.deployments}
        templates={props.templates}
      >
        <DeploymentView />
      </DeploymentsProvider>
    </CloudMenuProvider>
  );
};

const DeploymentView = () => {
  const [credentials] = useCredentials();
  const [event] = useEvents();
  const { deployments, fetchDeployment, busy } = useDeployments();

  const hasCloudCredential = useMemo(() => {
    return (
      credentials?.filter((c) => c.type === "aws" || c.type === "openstack")
        .length > 0
    );
  }, [credentials]);

  useEffect(() => {
    if (!event) return;

    console.log(event);

    if (event.type === "DeploymentRunCreated") {
      fetchDeployment(event.data.run.deployment);
    }

    if (event.type === "DeploymentRunStatusUpdated") {
      const deployment = deployments.find(
        (d) => d.id === event.data.deployment
      );
      if (!deployment)
        console.error(
          "Event received for unknown deployment",
          event.data.deployment
        );
      else fetchDeployment(event.data.deployment);
    }
  }, [event]);

  return (
    <Layout title={"Deployments"} parts={["deployments", ""]} busy={busy}>
      <Box style={{ minWidth: "20rem", maxWidth: "80rem" }}>
        <Grid container justifyContent="space-between">
          <Grid item>
            <CloudMenu />
          </Grid>
        </Grid>
        <br />
        {!hasCloudCredential && <CloudCredentialNotice />}
        <WorkspaceList />
      </Box>
    </Layout>
  );
};

export async function getServerSideProps({ req, res }) {
  let deployments, templates;

  try {
    [deployments, templates] = await Promise.all([
      req.api.deployments({ "full-run": true }),
      req.api.templates(),
    ]);
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/welcome");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }

  return {
    props: {
      deployments,
      templates,
    },
  };
}

export default Deployments;
