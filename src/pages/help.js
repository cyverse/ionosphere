import { Box, Divider, Grid, Link, Typography } from "@mui/material";
import Layout from "../components/Layout";
import HelpCard from "../components/Cards/HelpCard";
import { getMenuItem } from "../menuItems.js";

const Help = () => {
  const help = getMenuItem("Help");
  const supportItems = help.items.filter((item) => item.category === "support");
  const learnItems = help.items.filter((item) => item.category === "learn");

  return (
    <Layout title="Help">
      <Box>
        <Typography variant="h6">Learn</Typography>
        <Divider />
        <br />
        <Grid container spacing={4}>
          {learnItems.map((item) => (
            <Grid item md={6} s={12} xs={12} lg={3} xl={3} key={item.path}>
              <Link underline="none" target="_blank" href={item.path}>
                <HelpCard
                  title={item.label}
                  icon={item.icon}
                  description={item.description}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Box>
      <Box mt={4}>
        <Typography variant="h6" mt={4}>
          Support
        </Typography>
        <Divider />
        <br />
        <Grid container spacing={4}>
          {supportItems.map((item) => (
            <Grid item md={6} s={12} xs={12} lg={3} xl={3} key={item.path}>
              <Link underline="none" target="_blank" href={item.path}>
                <HelpCard
                  title={item.label}
                  description={item.description}
                  icon={item.icon}
                />
              </Link>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Layout>
  );
};

//FIXME this is required to prevent "useUser must be used within a UserProvider" error, not sure why
export async function getServerSideProps() {
  return { props: {} };
}

export default Help;
