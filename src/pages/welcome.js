import {
  Grid,
  Button,
  Card,
  Link,
  CardHeader,
  CardContent,
  Typography,
  Container,
  Box,
} from "@mui/material";
import {
  GitHub,
  HelpRounded,
  LibraryBooks,
  OpenInNew,
} from "@mui/icons-material";
import SplashFooter from "../components/SplashFooter";
import Image from "next/image";

const Welcome = (props) => {
  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
        padding: "4rem", // Ensures the container takes up the full viewport height
      }}
    >
      <Grid
        container
        spacing={3}
        direction="column"
        alignItems="center"
        alignContent={"center"}
        justifyContent="center"
      >
        <Grid item>
          <Box mr="30px">
            <Image src="/images/cacaoLogo.svg" width="400" height="140" />
          </Box>
        </Grid>
        <Grid item sx={{ marginBottom: "4rem" }}>
          <Button
            variant="contained"
            size="large"
            color="secondary"
            sx={{
              width: "10em",
              textTransform: "none",
            }}
            href="/login"
          >
            Sign In
          </Button>
        </Grid>
        <Grid item>
          <Grid container spacing={3} justifyContent="center" direction={"row"}>
            <Grid item xs={12} sm={12} md={4} xl={3}>
              <Link
                underline="none"
                href="https://docs.jetstream-cloud.org/"
                target="_blank"
              >
                <Card sx={{ width: "99%", height: "99%" }}>
                  <CardHeader
                    title={
                      <Typography color="primary" sx={{ fontWeight: "500" }}>
                        View Documentation
                      </Typography>
                    }
                    action={<OpenInNew color="primary" />}
                    avatar={<LibraryBooks color="primary" />}
                  />
                  <CardContent>
                    <Typography textAlign={"center"}>
                      View documentation for CACAO on Jetstream2.
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
            <Grid item xs={12} sm={12} md={4} xl={3}>
              <Link
                underline="none"
                href="https://docs.google.com/forms/d/e/1FAIpQLScfu1NKL3ofVRiCJ-YP-rhfTIxFjZ6Td8bQpLMi2oqqploAZA/viewform"
                target="_blank"
              >
                <Card sx={{ width: "99%", height: "99%" }}>
                  <CardHeader
                    title={
                      <Typography color="primary" sx={{ fontWeight: "500" }}>
                        Contact Support
                      </Typography>
                    }
                    action={<OpenInNew color="primary" />}
                    avatar={<HelpRounded color="primary" />}
                  />
                  <CardContent>
                    <Typography textAlign={"center"}>
                      Having trouble logging in? Contact CACAO Support.
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
            <Grid item xs={12} sm={12} md={4} xl={3}>
              <Link
                underline="none"
                href="https://gitlab.com/cyverse/cacao"
                target="_blank"
              >
                <Card sx={{ width: "99%", height: "99%" }}>
                  <CardHeader
                    title={
                      <Typography color="primary" sx={{ fontWeight: "500" }}>
                        View Source Code
                      </Typography>
                    }
                    action={<OpenInNew color="primary" />}
                    avatar={<GitHub color="primary" />}
                  />
                  <CardContent>
                    <Typography textAlign={"center"}>
                      CACAO source code is hosted on GitLab
                    </Typography>
                  </CardContent>
                </Card>
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Box
        component="footer"
        sx={{
          paddingY: "20px",
          marginTop: "auto", // Pushes the footer to the bottom
        }}
      >
        <SplashFooter />
      </Box>
    </Container>
  );
};

export default Welcome;
