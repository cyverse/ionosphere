import * as Sentry from "@sentry/nextjs";
import { useState, useEffect, useMemo } from "react";
import {
  Box,
  Divider,
  Alert,
  Typography,
  AlertTitle,
  Button,
  Grid,
} from "@mui/material";
import { isPrerequisiteTemplate } from "@/utils";
import { templateTypeProviderTypesMap } from "@/utils/cacaoUtils";
import Layout from "../components/Layout";
import TemplateCard from "../components/Templates/TemplateCard";
import CreateDeploymentDialog from "@/components/DeploymentWizard/CreateDeploymentDialog";
import { DeploymentWizardProvider } from "@/components/DeploymentWizard/contexts/DeploymentWizardContext";
import { CloudMenuFormProvider } from "@/contexts/cloudMenuForm";
import TemplateSearch from "@/components/Templates/TemplateSearch";
import SortingOptions from "@/components/Templates/SortingOptions";
import TemplateList from "@/components/Templates/TemplateList";
import { TemplatesProvider } from "@/components/Templates/TemplatesContext";
import {
  useTemplates,
  useTemplatesDispatch,
} from "@/components/Templates/TemplatesContext";

const Templates = ({ templates, templateTypeCloudTypesMap }) => {
  const [showTemplateWizard, setShowTemplateWizard] = useState(null); // templateId

  const selectedTemplate = useMemo(
    () => templates.find((t) => t.id === showTemplateWizard),
    [showTemplateWizard, templates]
  );
  const templateCloudTypes = useMemo(
    () => templateTypeCloudTypesMap[selectedTemplate?.metadata.template_type],
    [selectedTemplate, templateTypeCloudTypesMap]
  );

  const handleClose = async () => {
    setShowTemplateWizard(false);
  };

  return (
    <Layout title="Templates">
      <TemplatesProvider templates={templates}>
        <Grid container spacing={3} justify="flex-end">
          <Grid item xs={12} sm={12} md={3} lg={2}>
            <SortingOptions />
          </Grid>
          <Grid item xs={12} sm={12} md={9} lg={10}>
            <Box>
              <Box my={2}>
                <TemplateSearch />
              </Box>
              <TemplateList setShowTemplateWizard={setShowTemplateWizard} />
            </Box>
          </Grid>
        </Grid>
      </TemplatesProvider>
      {showTemplateWizard && (
        <CloudMenuFormProvider templateCloudTypes={templateCloudTypes}>
          <DeploymentWizardProvider
            initialValues={{
              template: selectedTemplate,
              templateCloudTypes: templateCloudTypes,
            }}
          >
            <CreateDeploymentDialog
              open={!!showTemplateWizard}
              handleClose={handleClose}
              templates={templates}
              handlePostCreate={handleClose}
            />
          </DeploymentWizardProvider>
        </CloudMenuFormProvider>
      )}
    </Layout>
  );
};

export async function getServerSideProps({ req, res }) {
  try {
    const templates = await req.api.templates();
    const templateTypes = await req.api.templateTypes();

    return {
      props: {
        templates: templates && templates.length > 0 && templates.sort(),
        templateTypeCloudTypesMap: templateTypeProviderTypesMap(templateTypes),
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);
    if (e?.response?.status === 401) {
      res.redirect("/401");
    } else {
      res.redirect("/500");
    }
    return { props: {} };
  }
}

export default Templates;
