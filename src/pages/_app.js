import React from "react";
import Head from "next/head";
import getConfig from "next/config";
import * as Sentry from "@sentry/nextjs";
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import {
  ConfigProvider,
  APIProvider,
  ErrorProvider,
  UserProvider,
  UserConfigProvider,
  UserRecentsProvider,
  EventsProvider,
  CloudsProvider,
  Js2AllocationsProvider,
  CredentialsProvider,
  WorkspacesProvider,
} from "../contexts";
import theme from "../theme";
import "../styles/global.css";

export default function MyApp(props) {
  const {
    sentryDSN,
    sentryRelease,
    sentryEnv,
    Component,
    pageProps,
    isDevelopment,
    user,
    userConfig,
    userRecents,
    clouds,
    js2Allocations,
    credentials,
    workspaces,
    baseUrl,
    token,
  } = props;
  const config = getConfig().publicRuntimeConfig;
  config.isDevelopment = isDevelopment;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  // const theme =
  //   config.THEME && themes[config.THEME]
  //     ? themes[config.THEME]
  //     : themes.default;

  if (sentryDSN) {
    // we need to init Sentry separately for frontend.
    Sentry.init({
      dsn: sentryDSN,
      tracesSampleRate: 1.0,
      environment: sentryEnv,
      release: sentryRelease,
    });
  }

  return (
    <>
      <Head>
        <title>CyVerse Cacao | Jetstream2</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <ConfigProvider config={config}>
            <APIProvider baseUrl={baseUrl} token={token}>
              <UserProvider user={user}>
                <EventsProvider username={user && user.username}>
                  <CloudsProvider clouds={clouds}>
                    <WorkspacesProvider workspaces={workspaces}>
                      <UserConfigProvider userConfig={userConfig || {}}>
                        <Js2AllocationsProvider js2Allocations={js2Allocations}>
                          <CredentialsProvider credentials={credentials}>
                            <UserRecentsProvider
                              userRecents={userRecents || {}}
                            >
                              <ErrorProvider>
                                <Component {...pageProps} />
                              </ErrorProvider>
                            </UserRecentsProvider>
                          </CredentialsProvider>
                        </Js2AllocationsProvider>
                      </UserConfigProvider>
                    </WorkspacesProvider>
                  </CloudsProvider>
                </EventsProvider>
              </UserProvider>
            </APIProvider>
          </ConfigProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const req = ctx.req;
  const api = req && req.api;

  let user;
  let userConfig;
  let userRecents;
  let clouds;
  let js2Allocations;
  let credentials;
  let workspaces;
  let pageProps;

  try {
    user = api && api.token && (await api.user());
    userConfig =
      api && api.token && user && (await api.userConfigs(user.username));
    userRecents =
      api && api.token && user && (await api.userRecents(user.username));
    clouds = api && api.token && (await api.providers());
    js2Allocations = api && api.token && (await api.js2allocations());
    credentials = api && api.token && (await api.credentials());
    workspaces = api && api.token && (await api.workspaces());
    pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};

    if (clouds && clouds.length) {
      // ensure user has workspace for each cloud
      let workspacesUpdated = false;

      for (const cloud of clouds) {
        if (
          !workspaces?.find(
            (w) =>
              w.default_provider_id === cloud.id &&
              w.name === "Default Workspace"
          )
        ) {
          await req.api.createWorkspace({
            name: "Default Workspace",
            default_provider_id: cloud.id,
          });
          workspacesUpdated = true;
        }
      }

      if (workspacesUpdated) workspaces = await req.api.workspaces();
    }
  } catch (e) {
    console.error(e);
  }

  return {
    sentryDSN: req && req.sentryDSN,
    sentryRelease: req && req.sentryRelease,
    sentryEnv: req && req.sentryEnv,
    isDevelopment: req && req.isDevelopment,
    baseUrl: api && api.baseUrl,
    token: api && api.token, //FIXME still necessary?
    user: user,
    userConfig: userConfig,
    userRecents: userRecents,
    clouds: clouds,
    js2Allocations: js2Allocations,
    credentials: credentials,
    workspaces: workspaces,
    pageProps: pageProps,
  };
};
