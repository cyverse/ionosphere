import { useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import getConfig from "next/config";
import * as Sentry from "@sentry/nextjs";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,
  Grid,
  List,
  Typography,
} from "@mui/material";

import { Delete as DeleteIcon } from "@mui/icons-material";
import { mdiRocketLaunch } from "@mdi/js";
import Icon from "@mdi/react";

import { useAPI, useCredentials, useError, useEvents } from "@/contexts";
import { timeDiffString, datetimeString } from "@/utils";

import { Layout, StatusIndicator, ConfirmationDialog } from "@/components";
import {
  Resource,
  needToRenderResource,
} from "../../components/Resources/Resource";
import { matchAWSInstanceIP } from "../../components/Resources/AWSResource";

import DeploymentErrorAlert from "../../components/Deployment/DeploymentErrorAlert";
import ActionButton from "@/components/Common/Button/ActionButton";
import ProgressDialog from "@/components/Common/Dialog/ProgressDialog";
import RefreshButton from "@/components/Common/Button/RefreshButton";

import {
  DeploymentParametersAccordion,
  ErrorLinearProgress,
  LogsButton,
  PowerStateControls,
  StandardLinearProgress,
} from "@/components/Deployment/DeploymentDetail";

const DeploymentDetail = (props) => {
  const config = getConfig().publicRuntimeConfig;
  const router = useRouter();
  const api = useAPI();
  const [_, setError] = useError();
  const [event] = useEvents();
  const [credentials] = useCredentials();

  const [deployment, setDeployment] = useState(props.deployment);
  const [run, setRun] = useState(props.run);
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const [busy, setBusy] = useState(false);

  const [showProgress, setShowProgress] = useState(false);
  const [sessionUrl, setSessionUrl] = useState();

  const credentialName = useMemo(() => {
    return credentials.find((c) => c.id === deployment.cloud_credentials[0])
      ?.name;
  }, [credentials, deployment]);

  const handleError = (error) => {
    console.error(error);
    setBusy(false);
    setError(api.errorMessage(error));
  };

  const refreshDeployment = async () => {
    setBusy(true);
    await fetchDeployment(deployment.id);
    setBusy(false);
  };

  async function fetchDeployment(id) {
    try {
      const deployment = await api.deployment(id);

      // Run is refetched separately due to some inconsistencies in deployment API response
      const run = await api.deploymentRun(
        deployment.id,
        deployment.current_run.id
      );
      setDeployment(deployment);

      if (
        props.template.metadata.template_type === "aws_terraform" &&
        run.last_state &&
        run.last_state.resources
      ) {
        // transform the resources for AWS
        matchAWSInstanceIP(run.last_state.resources);
      }

      setRun(run);
    } catch (error) {
      setError("Error refreshing deployment.");
    }
  }

  useEffect(() => {
    if (!event) return;
    console.log(event);
    if (
      event.type === "DeploymentRunStatusUpdated" &&
      event.data.deployment === deployment.id
    ) {
      if (event.data.status === "active" || event.data.status === "errored") {
        fetchDeployment(deployment.id);
      } else {
        const updatedRun = { ...run, status: event.data.status };
        setRun(updatedRun);
      }
    } else if (
      event.type === "DeploymentRunCreated" &&
      event.data.run.deployment === deployment.id
    ) {
      fetchDeployment(deployment.id);
    }
  }, [event]);

  const handleDeleteDeployment = async () => {
    setBusy(true);
    try {
      await api.deleteDeployment(deployment.id);
      setTimeout(() => router.push("/deployments"), 500); //FIXME replace with event detection
    } catch (error) {
      handleError(error);
    }
  };

  const createISessionRequest = (props) => {
    const instanceType = props.type;

    let instanceAddress;
    switch (instanceType) {
      case "aws_instance":
        instanceAddress = props.attributes.public_ip;
        break;
      case "openstack_instance":
        instanceAddress = props.attributes.floating_ip;
        break;
      default:
        instanceAddress = null;
    }

    const req = {
      instance_id: props.id,
      instance_address: instanceAddress,
      instance_admin_username: deployment.owner.split("@")?.[0],
      cloud_id: "default",
      protocol: props.protocol,
    };

    return req;
  };

  // create isession
  const createISession = async (props) => {
    try {
      setShowProgress(true);
      setBusy(true);
      setSessionUrl(null);

      const req = createISessionRequest(props);

      // Try to reuse existing session
      const isessions = await api.isessions();
      let isession = isessions?.find(
        (s) =>
          s.state === "active" &&
          s.cloud_id === req.cloud_id &&
          s.instance_id === req.instance_id &&
          s.instance_address === req.instance_address &&
          s.protocol === req.protocol
      );

      // Create new session
      if (!isession) {
        if (props.type === "vnc")
          req.password = config.ISESSION_VNC_DEFAULT_PASSWORD || "";

        isession = await api.createISession(req);
        if (!isession?.tid)
          throw new ("Could not create session" +
            (props.type === "vnc"
              ? ". Check that VNC is installed in the image."
              : ""))();
      }

      // Open session in new tab
      isession = await api.isession(isession.tid || isession.id);
      if (isession?.state === "failed") throw "Session creation failed";
      if (!isession?.redirect_url) throw "Could not retrieve session url";
      setSessionUrl(isession.redirect_url);
      window.open(isession.redirect_url, "_blank");
    } catch (error) {
      handleError(error);
      setShowProgress(false);
    } finally {
      setBusy(false);
    }
  };

  const getStatus = (run) => {
    let status = run.status;
    if (status === "running" || status === "pre-flight") {
      let ps = run.parameters?.find((p) => p.key === "power_state")?.value;
      if (ps === "shelved_offloaded") return "Shelving";
      if (ps === "shutoff") return "Shutting Off";
    }
    return "Deploying (" + (status || "pending") + ")";
  };

  return (
    <Layout title={deployment.id} breadcrumbs busy={busy && !showProgress}>
      <Box mt={4}>
        <Card>
          <CardHeader
            avatar={
              <Avatar>
                <Icon path={mdiRocketLaunch} size={1} />
              </Avatar>
            }
            action={
              <Grid container alignItems="center">
                <Box display="flex" alignItems="center">
                  <Box mt={1} mr={1}>
                    <StatusIndicator deployment={deployment} />
                  </Box>
                  <Divider
                    orientation="vertical"
                    flexItem
                    style={{ margin: "0 0.5em 0 0.5em" }}
                  />

                  <RefreshButton
                    onClick={() => refreshDeployment(deployment.id)}
                  />
                  <LogsButton deploymentId={deployment.id} runId={run.id} />

                  {deployment.parameters?.find(
                    (p) => p.key === "power_state"
                  ) && (
                    <>
                      <Divider
                        orientation="vertical"
                        flexItem
                        style={{ margin: "0 0.5em 0 0.5em" }}
                      />

                      <PowerStateControls
                        deployment={deployment}
                        run={run}
                        setBusy={setBusy}
                        handleError={handleError}
                      ></PowerStateControls>
                    </>
                  )}

                  <Divider
                    orientation="vertical"
                    flexItem
                    style={{ margin: "0 0.5em 0 0.5em" }}
                  />

                  <ActionButton
                    tooltipText={"Delete"}
                    onClick={() => setShowDeleteConfirmation(true)}
                    disabled={
                      deployment.current_status === "deleted" ||
                      deployment.pending_status !== "none"
                    }
                  >
                    <DeleteIcon />
                  </ActionButton>
                </Box>
              </Grid>
            }
            title={
              <Typography>
                <b>{deployment.name || "<Unnamed>"}</b>{" "}
                <span>
                  {run.parameters?.find((e) => e.key === "region")?.value}
                </span>
              </Typography>
            }
            subheader={
              <Typography variant="body2">
                <b>Started:</b> {datetimeString(deployment.created_at)}
                {run.start && run.end && run.end > run.start && (
                  <>
                    , <b>Build time:</b> {timeDiffString(run.start, run.end)}
                  </>
                )}
                , <b>Updated:</b> {datetimeString(deployment.updated_at)}
                <br />
                <b>Template</b>: {props.template.name}&nbsp;&nbsp;
                <b>Template ID</b>: {deployment.template_id}
                <br />
                {deployment.cloud_credentials.length > 0 && (
                  <>
                    <b>Credential</b>: {credentialName}
                  </>
                )}
              </Typography>
            }
          />
          <CardContent>
            {deployment.current_status === "creation_errored" && (
              <Box mb={2}>
                <DeploymentErrorAlert deployment={deployment} />
              </Box>
            )}

            {run && run.status === "active" ? (
              <>
                {run.parameters && (
                  <Box m={2}>
                    <DeploymentParametersAccordion
                      parameters={run.parameters}
                    />
                  </Box>
                )}
                <List>
                  {run.last_state.resources &&
                    run.last_state.resources
                      .filter((r) => needToRenderResource(r))
                      .map((resource, index) => (
                        <Box key={index}>
                          <Divider />
                          <Resource
                            state={
                              run.parameters.find(
                                (p) => p.key === "power_state"
                              )?.value
                            }
                            {...resource}
                            sessionHandler={createISession}
                          />
                        </Box>
                      ))}
                </List>
              </>
            ) : (
              <Box mx={2} mt={4}>
                {run.status === "errored" ? (
                  <Box>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      gutterBottom
                    >
                      Error:{" "}
                      {run.status_msg ? run.status_msg : "An error occurred"}
                    </Typography>
                    <Box display="flex" alignItems="center">
                      <Box width="100%">
                        <ErrorLinearProgress
                          variant="determinate"
                          value={100}
                        />
                      </Box>
                    </Box>
                  </Box>
                ) : (
                  <Box>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      gutterBottom
                    >
                      {getStatus(run)} ...
                    </Typography>
                    <Box display="flex" alignItems="center">
                      <Box width="100%">
                        <StandardLinearProgress variant="indeterminate" />
                      </Box>
                    </Box>
                  </Box>
                )}
                {run.parameters && (
                  <Box my={4}>
                    <DeploymentParametersAccordion
                      parameters={run.parameters}
                    />
                  </Box>
                )}
              </Box>
            )}
          </CardContent>
        </Card>
      </Box>
      <ConfirmationDialog
        open={showDeleteConfirmation}
        title="Delete deployment"
        handleClose={() => setShowDeleteConfirmation(false)}
        handleSubmit={() => handleDeleteDeployment()}
      />
      <ProgressDialog
        open={showProgress}
        busy={busy}
        title="Web Shell/Desktop"
        busyText={
          <Box display="flex">
            <CircularProgress
              color="inherit"
              size={20}
              style={{ marginRight: "1em" }}
            />
            <Typography>Creating session ...</Typography>
          </Box>
        }
        doneText={
          <>
            Session opened in new tab or click{" "}
            <a href={sessionUrl} target="_blank" rel="noreferrer">
              this link
            </a>
            .
          </>
        }
        handleClose={() => setShowProgress(false)}
      />
    </Layout>
  );
};

export async function getServerSideProps({ req, res, query }) {
  try {
    const deployment = await req.api.deployment(query.id);
    const template = await req.api.template(deployment.template_id);
    const run = await req.api.deploymentRun(
      deployment.id,
      deployment.current_run.id
    );

    if (
      template.metadata.template_type === "aws_terraform" &&
      run.last_state &&
      run.last_state.resources
    ) {
      // transform the resources for AWS before send to client, so that it is easier to render.
      matchAWSInstanceIP(run.last_state.resources);
    }

    return {
      props: {
        deployment,
        template,
        run,
      },
    };
  } catch (e) {
    Sentry.captureException(e);
    console.error(e);

    let redirectDestination;
    if (e?.response?.status === 401) {
      redirectDestination = "/401";
    } else if (e?.response?.status === 404) {
      redirectDestination = "/404";
    } else {
      redirectDestination = "/500";
    }
    return {
      redirect: {
        destination: redirectDestination,
        permanent: false,
      },
    };
  }
}

export default DeploymentDetail;
