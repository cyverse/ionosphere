import React from "react";
import { Grid, Box, Typography } from "@mui/material";

export default function Custom401() {
  return (
    <div>
      <Box>
        <Grid
          container
          alignItems="center"
          direction="row"
          justifyContent="center"
          wrap="nowrap"
        >
          <Grid item xs={12}>
            <Typography variant="h4" align="center" style={{ marginTop: "3%" }}>
              Login failed
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export async function getServerSideProps({ req, res }) {
  res.setHeader("Set-Cookie", ["session=", "session.sig="]);
  return { props: {} };
}
