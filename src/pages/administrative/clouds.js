import { useState } from "react";
import { useRouter } from "next/router";
import { Grid, Box, Button } from "@mui/material";
import { Layout, FormDialog } from "../../components";
import CloudCard from "../../components/Cards/CloudCard";
import { useAPI } from "../../contexts/api";

const Clouds = ({ providers }) => {
  const router = useRouter();
  const api = useAPI();
  const [dialogOpen, setDialogOpen] = useState(false);

  const sortOSFirst = (a, b) => {
    if (a.type === "openstack" && b.type !== "openstack") return -1;
    if (b.type === "openstack") return 1;

    return a.name.localeCompare(b.name);
  };

  const createCloud = async (values) => {
    const response = await api.createProvider(values);
    //TODO handle errors
    if (response) {
      router.push(`/administrative/clouds`);
    }
  };

  const createButton = (
    <Button
      variant="contained"
      color="primary"
      onClick={() => setDialogOpen(true)}
    >
      Create Cloud
    </Button>
  );

  return (
    <Layout breadcrumbs actions={createButton}>
      <Box mt={4}>
        <Grid container spacing={3}>
          {providers &&
            providers
              //.sort((a, b) => (a.name > b.name ? 1 : -1))
              .sort(sortOSFirst)
              .map((provider, index) => (
                <Grid item key={index} s={12} xs={12} md={6} lg={4} xl={3}>
                  <CloudCard {...provider} />
                </Grid>
              ))}
        </Grid>
      </Box>
      <FormDialog
        title="Create Cloud"
        open={dialogOpen}
        fields={[
          {
            id: "name",
            label: "Name",
            type: "text",
            required: true,
          },
          {
            id: "type",
            label: "Type",
            type: "text",
            required: true,
          },
          {
            id: "url",
            label: "URL",
            type: "text",
            required: true,
          },
        ]}
        handleClose={() => setDialogOpen(false)}
        handleSubmit={createCloud}
      />
    </Layout>
  );
};

export async function getServerSideProps({ req }) {
  const providers = await req.api.providers();
  return {
    props: {
      providers,
    },
  };
}

export default Clouds;
