import React from "react";
import { Layout } from "../../../components";
import {
  Container,
  Box,
  Tooltip,
  CardHeader,
  CardContent,
  Typography,
  Card,
  ListItem,
  List,
  ListItemText,
} from "@mui/material";
import { Public as PublicIcon, Lock as LockIcon } from "@mui/icons-material";
import CloudAvatarIcon from "../../../components/CloudAvatarIcon";

/**
 * Render the page
 * @param {Object} provider
 * @param {string} provider.id
 * @param {string} provider.name
 * @param {string} provider.type
 * @param {string} provider.description
 * @param {string} provider.url
 * @param {boolean} provider.public
 * @param {Object} provider.metadata
 * @returns {JSX.Element}
 * @constructor
 */
const Clouds = ({ provider }) => {
  return (
    <Layout breadcrumbs>
      <Container maxWidth="lg">
        <Box mt={4}>
          <Card>
            <CardHeader
              avatar={<CloudAvatarIcon type={provider.type} />}
              title={provider.name}
              subheader={provider.type}
            />
            <CardContent>
              <List dense={true}>
                <ListItem>
                  <ListItemText primary={provider.description} />
                </ListItem>
                <ListItem>
                  <ListItemText primary="Public" />
                  <Typography>
                    {provider.public ? (
                      <Tooltip title={"public"}>
                        <PublicIcon />
                      </Tooltip>
                    ) : (
                      <Tooltip title={"private"}>
                        <LockIcon />
                      </Tooltip>
                    )}
                  </Typography>
                </ListItem>
                <ListItem>
                  <ListItemText primary="URL" />
                  <Typography>{provider.url}</Typography>
                </ListItem>
                <ListItem>
                  <Typography>Metadata:</Typography>
                  <pre>{JSON.stringify(provider.metadata, null, "    ")}</pre>
                </ListItem>
              </List>
            </CardContent>
          </Card>
        </Box>
      </Container>
    </Layout>
  );
};

export async function getServerSideProps({ req, res, query }) {
  try {
    const provider = await req.api.provider(query.id);
    return {
      props: {
        provider,
      },
    };
  } catch (e) {
    console.error(e);
    return {
      redirect: {
        destination: "/404",
        permanent: false,
      },
    };
  }
}

export default Clouds;
