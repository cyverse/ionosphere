import React from "react";
import { Grid, Box, Typography } from "@mui/material";

export default function Custom404() {
  return (
    <div>
      <Box>
        {/*<ErrorAnimation />*/}
        <Grid
          container
          alignItems="center"
          direction="row"
          justifyContent="center"
          wrap="nowrap"
        >
          <Grid item xs={12}>
            <Typography variant="h4" align="center" style={{ marginTop: "3%" }}>
              <strong> Whoops!</strong> Page Not Found
            </Typography>
          </Grid>
        </Grid>
        <Grid
          container
          alignItems="center"
          direction="row"
          justifyContent="center"
          wrap="nowrap"
        >
          <Typography variant="h6" style={{ marginTop: "10px" }}>
            Looks like we got a little lost in space.
          </Typography>
        </Grid>
        <Grid
          container
          alignItems="center"
          direction="row"
          justifyContent="center"
          wrap="nowrap"
        >
          <Typography variant="h6" style={{ marginTop: "10px" }}>
            Let's get you back{" "}
            <a href="/home" style={{ textDecoration: "none" }}>
              home
            </a>
            .
          </Typography>
        </Grid>
      </Box>
    </div>
  );
}
