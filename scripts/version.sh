#!/bin/sh
# for generating version.txt. this should be run before building docker image.
# version.txt will be read by ion for version information
git rev-parse --abbrev-ref HEAD | tee version.txt
git rev-parse --short=8 HEAD | tee -a version.txt
git log -1 --format=%cd | tee -a version.txt
