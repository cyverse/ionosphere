### Ionosphere local development setup

#### Prerequisites

First install [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) (node version manager).

Using nvm, install node.js and npm (node package manager):

```
nvm install node
```

#### Project Setup
First clone the repo:
```
git clone git@gitlab.com:cyverse/ionosphere.git
```

Create a new file in the ionosphere directory called `.env.local` with the following:

```
THEME=jetstream
API_BASE_URL=https://js2.cyverse.org/api

DEV_AUTH_TOKEN=<auth_token>

# Openstack token can be obtained at: https://js2.jetstream-cloud.org:5000/identity/v3/auth/OS-FEDERATION/websso/openid?origin=https://cacao.jetstream-cloud.org
UNSCOPED_TOKEN=<unscoped_token>
```

Auth in local development is currently handled by setting `DEV_AUTH_TOKEN` in `.env.local`. 

To obtain an auth token from the js2 logs (you'll want to log into js2.cyverse.org first to use your own account):
```
ssh -p 1657 root@js2.cyverse.org
k3s kubectl logs -f $(k get pods | cut -f 1 -d ' ' | grep -P '^ion-')
```

Copy your auth token from the logs (without the “Bearer” part), and set `DEV_AUTH_TOKEN` to this value.

Optionally, if you’d like to be able to create js2 credentials in local development using the credential wizard, you can also set `UNSCOPED_TOKEN`, which can be obtained [here](https://js2.jetstream-cloud.org:5000/identity/v3/auth/OS-FEDERATION/websso/openid?origin=https://cacao.jetstream-cloud.org).

Finally, we can install the required node packages (make sure you are in ionosphere dir):
```
npm install
```
Now, run the server using:
```
npm run dev
```

You can access your development site at: http://localhost:3010.

Note: If you update your `DEV_AUTH_TOKEN` or `UNSCOPED_TOKEN` you'll need to re-run `npm run dev` to register those changes.

